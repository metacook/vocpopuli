{ pkgs ? import <nixpkgs> { }, ... }:

pkgs.mkShell {
  buildInputs = [
    pkgs.python310
    pkgs.python310Packages.virtualenv
  ];

  shellHook = ''
    if [ -d "./venv" ]; then
      echo "venv already created"
    else
      python -m venv venv
    fi

    source ./venv/bin/activate

    pip install -r requirements.txt
  '';
}
