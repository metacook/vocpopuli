from . import read, write
from ._queries import File as Q
from ..models.file import File
from uuid import uuid4


@write
def create(
    tx, term_version_id: str, url: str, order_id: int, id: str | None = None
) -> File:
    result = tx.run(
        Q.create,
        file_id=id or uuid4().hex,
        term_version_id=term_version_id,
        url=url,
        order_id=order_id,
    ).single()
    return dict(result["file"])


@write
def import_(
    tx, term_version_id: str, url: str, order_id: int, id: str | None = None
) -> File:
    result = tx.run(
        Q.create,
        file_id=id or uuid4().hex,
        term_version_id=term_version_id,
        url=url,
        order_id=order_id,
    ).single()
    return dict(result["file"])


@read
def get(tx, id: str) -> File:
    result = tx.run(Q.get, id=id).single()
    return dict(result["file"])


@write
def set_url(tx, id: str, new_url: str, local: bool = False) -> File:
    result = tx.run(Q.set_url, id=id, new_url=new_url, local=local).single()
    return dict(result["file"])
