from flask import current_app, g, session
from functools import wraps
from neo4j import GraphDatabase
from ._queries import Vocabulary as QVocabulary


def get_db():
    if "db" not in g:
        g.db = GraphDatabase.driver(
            current_app.config.get("NEO4J_URI"),
            auth=(
                current_app.config.get("NEO4J_USERNAME"),
                current_app.config.get("NEO4J_PASSWORD"),
            ),
        )
        g.db.verify_connectivity()
    return g.db


def get_terms_db():
    if "terms_db" not in g:
        g.terms_db = GraphDatabase.driver(
            current_app.config.get("NEO4J_URI_TERM_DB"),
            auth=(
                current_app.config.get("NEO4J_USERNAME_TERM_DB"),
                current_app.config.get("NEO4J_PASSWORD_TERM_DB"),
            ),
        )
        g.terms_db.verify_connectivity()
    return g.terms_db


def teardown_db(e=None):
    if db := g.pop("db", None):
        db.close()


def teardown_terms_db(e=None):
    if terms_db := g.pop("terms_db", None):
        terms_db.close()


def read(f):
    """
    Wraps the query in a session, executes it, and returns the results
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        db = get_db()
        with db.session() as session:
            return session.execute_read(f, *args, **kwargs)

    return decorated_function


def read_terms_db(f):
    """
    Wraps the query in a session, executes it, and returns the results
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        terms_db = get_terms_db()
        with terms_db.session() as session:
            return session.execute_read(f, *args, **kwargs)

    return decorated_function


def write(f):
    """
    Wraps the query in a session, executes it, and returns the results
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        db = get_db()
        with db.session() as session:
            return session.execute_write(
                update_vocabulary_timestamp(f), *args, **kwargs
            )

    return decorated_function


def write_terms_db(f):
    """
    Wraps the query in a session, executes it, and returns the results
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        terms_db = get_terms_db()
        with terms_db.session() as session:
            return session.execute_write(
                update_vocabulary_timestamp(f), *args, **kwargs
            )

    return decorated_function


def update_vocabulary_timestamp(f):
    """
    Attaches an update of the `updated_at` timestamp on the vocabulary node
    after the query function is executed (in the same transaction)

    The vocabulary id can be specified by passing a `_vocabulary_id` parameter
    when calling the db function, otherwise the value stored in
    session["PROJECT_ID"] (if accessible and not None) is used.
    """

    @wraps(f)
    def decorated_function(tx, *args, **kwargs):
        return_value = f(tx, *args, **kwargs)
        try:
            # assumes _vocabulary_id is truthy
            vocabulary_id = kwargs.get("_vocabulary_id") or session.get("PROJECT_ID")
        except RuntimeError:
            vocabulary_id = None

        if vocabulary_id:
            timestamp = tx.run(QVocabulary.set_updated_at, id=vocabulary_id).single()
            session["LAST_ACTIVITY"] = timestamp.get("timestamp").isoformat()

        return return_value

    return decorated_function
