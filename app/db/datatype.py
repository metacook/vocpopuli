from . import read, write, read_terms_db, write_terms_db
from ._queries import DataType as Q
from ..models.datatype import DataType
from uuid import uuid4


@read
def get_all(tx, vocabulary_id: str) -> list[DataType]:
    return tx.run(Q.get_all, vocabulary_id=vocabulary_id).value("type")


@read_terms_db
def get_all_bank(tx, vocabulary_id: str) -> list[DataType]:
    return tx.run(Q.get_all_bank, vocabulary_id=vocabulary_id).value("type")


@read
def get_all_with_order_and_instances(tx, vocabulary_id: str) -> list[dict]:
    return tx.run(
        Q.get_all_with_order_and_instances, vocabulary_id=vocabulary_id
    ).value("type")


def get_all_dict(vocabulary_id: str) -> dict[str, DataType]:
    return {data_type["name"]: data_type for data_type in get_all(vocabulary_id)}


def get_all_dict_bank(vocabulary_id: str) -> dict[str, DataType]:
    return {data_type["name"]: data_type for data_type in get_all_bank(vocabulary_id)}


def icon_dict(vocabulary_id: str) -> dict[str, str]:
    return {
        data_type["name"]: data_type.get("icon") for data_type in get_all(vocabulary_id)
    }


@read
def names(tx, vocabulary_id: str) -> list[DataType]:
    return tx.run(Q.names, vocabulary_id=vocabulary_id).value("type")


@write
def create(
    tx,
    vocabulary_id: str,
    name: str,
    color: str = "gray",
    icon: str = "bi-question-diamond",
    numeric: bool = False,
    toplevel: bool = True,
    parents: list = [],
    help_text: str = "",
) -> DataType:
    return tx.run(
        Q.create,
        vocabulary_id=vocabulary_id,
        id=uuid4().hex,
        name=name,
        color=color,
        icon=icon,
        numeric=numeric,
        toplevel=toplevel,
        parents=parents,
        help_text=help_text,
    ).value("type")


@write_terms_db
def import_terms_db_(
    tx,
    name: str,
    color: str = "gray",
    icon: str = "bi-question-diamond",
    numeric: bool = False,
    help_text: str = "",
) -> DataType:
    return tx.run(
        Q.import_terms_db_,
        name=name,
        color=color,
        icon=icon,
        numeric=numeric,
        help_text=help_text,
    ).value("type")


@write
def create_default_types(tx, vocabulary_id: str):
    tx.run(Q.create_default_types, vocabulary_id=vocabulary_id)


@write
def merge(tx, vocabulary_id: str, name: str) -> DataType:
    return tx.run(Q.merge, vocabulary_id=vocabulary_id, name=name).value("type")


@read
def is_numeric(tx, vocabulary_id: str, name: str) -> bool:
    return tx.run(Q.is_numeric, vocabulary_id=vocabulary_id, name=name).single()


@read
def numeric_types(tx, vocabulary_id: str) -> list[str]:
    return tx.run(Q.numeric_types, vocabulary_id=vocabulary_id).single()["types"]


@write
def delete(tx, vocabulary_id: str, datatype: str):
    tx.run(Q.delete, vocabulary_id=vocabulary_id, datatype=datatype)


@write
def reorder(tx, vocabulary_id: str, datatypes: list[str]):
    tx.run(Q.reorder, vocabulary_id=vocabulary_id, datatypes=list(enumerate(datatypes)))


@read
def toplevel_types(tx, vocabulary_id: str) -> list[str]:
    return tx.run(Q.toplevel_types, vocabulary_id=vocabulary_id).single()["types"]


@read
def get_child_datatype_configuration(tx, vocabulary_id: str) -> list[str]:
    return tx.run(
        Q.get_child_datatype_configuration, vocabulary_id=vocabulary_id
    ).value("types")


@write
def edit(
    tx,
    vocabulary_id: str,
    old_name: str,
    name: str,
    numeric: str,
    color: str,
    help_text: str,
    toplevel: bool,
    parents: list[str],
    icon: str,
):
    tx.run(
        Q.edit,
        vocabulary_id=vocabulary_id,
        old_name=old_name,
        name=name,
        numeric=numeric,
        color=color,
        help_text=help_text,
        toplevel=toplevel,
        parents=parents,
        icon=icon,
    )
