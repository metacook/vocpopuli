from . import write, read, read_terms_db, write_terms_db
from ._queries import RelationshipType as Q
from ..models.relationship_type import RelationshipType
from ..utils.terms import term_tag
from uuid import uuid4


@write
def create(
    tx, vocabulary_id: str, user_id: str, label: str, definition: str, related_iri: str
) -> RelationshipType:
    return tx.run(
        Q.create,
        vocabulary_id=vocabulary_id,
        user_id=user_id,
        id_t_global=f"{term_tag(label)}-{uuid4().hex}-vp",
        id_t_local=f"{uuid4().hex}-vp",
        id_t_version=f"{uuid4().hex}-vp",
        label=label,
        definition=definition,
        related_iri=related_iri,
    ).single()["relationship_type"]


@write
def edit(
    tx,
    vocabulary_id: str,
    relationship_id: str,
    definition: str,
    related_iri: str,
):
    tx.run(
        Q.edit,
        vocabulary_id=vocabulary_id,
        relationship_id=relationship_id,
        definition=definition,
        related_iri=related_iri,
    )


@write
def import_(
    tx,
    vocabulary_id: str,
    user_id: str,
    issue_iid: str,
    label: str,
    definition: str,
    id_t_global: str,
    id_t_local: str,
    id_t_version: str,
    created_at: str,
) -> RelationshipType:
    return tx.run(
        Q.import_,
        vocabulary_id=vocabulary_id,
        user_id=user_id,
        issue_iid=issue_iid,
        id_t_global=id_t_global,
        id_t_local=id_t_local,
        id_t_version=id_t_version,
        label=label,
        definition=definition,
        created_at=created_at,
    ).single()["relationship_type"]


@write
def remove(tx, id: str):
    tx.run(Q.remove, id=id)


@read
def get(tx, id: str) -> RelationshipType:
    result = tx.run(Q.get, id=id).single()
    return dict(result["relationship_type"])


@write
def update_issue_iid(tx, id: str, issue_iid: str):
    tx.run(Q.update_issue_iid, id=id, issue_iid=issue_iid)


@read
def get_all_with_relationships(tx, vocabulary_id: str):
    result = tx.run(Q.get_all_with_relationships, vocabulary_id=vocabulary_id).data()
    return [dict(relationship_type["relationship_type"]) for relationship_type in result]


@write_terms_db
def import_terms_db_(tx,
                     id_t_global: str,
                     id_t_local: str,
                     id_t_version: str,
                     label: str,
                     definition: str):
    tx.run(Q.import_terms_db_,
           id_t_global=id_t_global,
           id_t_local=id_t_local,
           id_t_version=id_t_version,
           label=label,
           definition=definition)


@write_terms_db
def import_relationships_terms_db_(tx,
                                  vocabulary_id: str,
                                  version_id: str,
                                  term_id: str,
                                  id_t_global: str,
                                  relationship_id: str):
    tx.run(Q.import_relationships_terms_db_,
           vocabulary_id=vocabulary_id,
           version_id=version_id,
           term_id=term_id,
           id_t_global=id_t_global,
           relationship_id=relationship_id)


@write
def import_from_terms_db_(
    tx,
    vocabulary_id: str,
    user_id: str,
    label: str,
    definition: str,
    id_t_global: str,
    id_t_local: str,
    id_t_version: str,
    created_at: str,
) -> RelationshipType:
    return tx.run(
        Q.import_from_terms_db_,
        vocabulary_id=vocabulary_id,
        user_id=user_id,
        id_t_global=id_t_global,
        id_t_local=id_t_local,
        id_t_version=id_t_version,
        label=label,
        definition=definition,
        created_at=created_at,
    ).single()["relationship_type"]


@write
def create_relationship(tx,
                        subject_version_id: str,
                        relationship_type_id: str,
                        relationship_id: str,
                        object_term_id: str,
                        ):
    tx.run(Q.create_relationship,
           subject_version_id=subject_version_id,
           relationship_type_id=relationship_type_id,
           relationship_id=relationship_id,
           object_term_id=object_term_id)



@write
def reorder(tx, id: str, relationship_types: list[str]):
    tx.run(Q.reorder, id=id, relationship_types=list(enumerate(relationship_types)))
