from . import read, write, write_terms_db, read_terms_db
from ._queries import Vocabulary as Q
from ..models.vocabulary import Vocabulary
from ..models.relationship_type import RelationshipType
from ..models.term import Term


@read
def get(tx, id: str) -> Vocabulary:
    result = tx.run(Q.get, id=id).single()
    return dict(result["vocabulary"]) if result else None


@read
def get_latest_id_v_local(tx, id: str) -> Vocabulary:
    result = tx.run(Q.get_latest_id_v_local, id=id).single()
    return result["id_v_local"] if result else None


@read
def all_terms(tx, id: str) -> list[Term]:
    results = tx.run(Q.all_terms, id=id).value("term")
    return [dict(result) for result in results]


@read
def top_level_terms(tx, id: str) -> list[Term]:
    results = tx.run(Q.top_level_terms, id=id).value("version")
    return [dict(result) for result in results]


@write
def create(tx,
           id: str,
           name: str,
           user_id: str,
           vocabulary_description: str,
           vocabulary_domain: str,
           institution_name: str,
           institution_ror: str,
           license_name: str,
           license_url: str,) -> Vocabulary:
    result = tx.run(Q.create,
                    id=str(id),
                    name=name,
                    user_id=user_id,
                    description=vocabulary_description,
                    domain=vocabulary_domain,
                    institution=institution_name,
                    institution_ror=institution_ror,
                    license_name=license_name,
                    license_url=license_url).single()
    return dict(result["vocabulary"])


@write_terms_db
def import_terms_db_(tx,
                     id: str,
                     name: str,
                     description: str,
                     domain: str,
                     institution: str,
                     institution_ror: str,
                     license_name: str,
                     license_url: str,
                     ) -> Vocabulary:

    result = tx.run(Q.import_terms_db_,
                    id=str(id),
                    name=name,
                    description=description,
                    domain=domain,
                    institution=institution,
                    institution_ror=institution_ror,
                    license_name=license_name,
                    license_url=license_url).single()
    return dict(result["vocabulary"])

@write_terms_db
def remove_terms_db_(tx, id: str):
    tx.run(Q.remove_terms_db_, id=str(id))


@read
def get_relationship_types(tx, id: str) -> list[RelationshipType]:
    results = tx.run(Q.get_relationship_types, id=id).value("type")
    return [dict(result) for result in results]


@read
def get_relationship_types_order_and_instances(tx, id: str) -> list[RelationshipType]:
    results = tx.run(Q.get_relationship_types_order_and_instances, id=id).value("type")
    return [dict(result) for result in results]


@read
def get_term_tree(tx, id: str) -> list:
    results = tx.run(Q.get_term_tree, id=id).value("version")
    return {result["term_id"]: dict(result) for result in results}


@read_terms_db
def get_term_tree_from_bank(tx, id: str) -> list:
    results = tx.run(Q.get_term_tree_from_bank, id=id).values("version", "broader", "narrower")
    return {
        version["term_id"]: (dict(version) | {"broader": broader, "narrower": narrower})
        for version, broader, narrower in results
    }


@read
def get_archived_term_tree(tx, id: str) -> list:
    results = tx.run(Q.get_archived_term_tree, id=id).values(
        "version", "broader", "narrower"
    )
    return {
        version["term_id"]: (dict(version) | {"broader": broader, "narrower": narrower})
        for version, broader, narrower in results
    }


@read
def top_level_ids(tx, id: str) -> list[str]:
    results = tx.run(Q.top_level_ids, id=id).data()
    return {result["type_name"]: result["term_ids"] for result in results}


@read_terms_db
def top_level_ids_from_bank(tx, id: str) -> list[str]:
    results = tx.run(Q.top_level_ids_from_bank, id=id).data()
    return {
        result["vp_type"]["name"]: (result["vp_type"], result["term_ids"])
        for result in results
    }


@read
def top_level_ids_archived(tx, id: str) -> list[str]:
    return tx.run(Q.top_level_ids_archived, id=id).single()["term_ids"]


@write
def update_metadata(tx,
                    id: str,
                    name: str,
                    description: str,
                    domain: list,
                    institution: str,
                    institution_ror: str,
                    license_name: str,
                    license_url: str):

    tx.run(Q.update_metadata,
                  id=id,
                  name=name,
                  description=description,
                  domain=domain,
                  institution=institution,
                  institution_ror=institution_ror,
                  license_name=license_name,
                  license_url=license_url)

@read_terms_db
def get_all_from_bank(tx) -> list[dict]:
    result = tx.run(Q.get_all_from_bank).data()
    return result


@read_terms_db
def get_from_bank(tx, id: str) -> Vocabulary:
    result = tx.run(Q.get_from_bank, id=id).single()
    return dict(result["vocabulary"]) if result else None


@read_terms_db
def get_all_terms_terms_db(tx) -> list:
    results = tx.run(Q.all_terms_terms_db).value("term")
    return [dict(result) for result in results]


@read_terms_db
def get_custom_relationships_from_bank(tx, vocabulary_id: str) -> list[dict]:
    results = tx.run(Q.get_custom_relationships_from_bank, vocabulary_id=vocabulary_id)
    return [dict(result["relationship_type"]) for result in results]
