from . import read, write, write_terms_db, read_terms_db
from ._queries import (
    Term as Q,
    TermVersion as QTermVersion,
    File as QFile,
    ExternalTerm as QExternalTerm,
)
from ..models.comment import Comment
from ..models.term import Term
from ..models.term_version import TermVersion
from ..models.user import User
from ..utils.terms import generate_uid
from ..utils.vocpopuli_types import determine_color_vocpopuli_type
from itertools import chain


@write
def create(
    tx,
    user_id: str,
    term_id: str,
    term_version_id: str,
    label: str,
    definition: str,
    reference: str,
    synonyms: list[str],
    translations: list[str],
    unit: str,
    datatype: str,
    broader: list[str],
    related: list[str],
    related_external_terms: list[str],
    required: bool,
    vocpopuli_type: str | None = None,
    vocabulary_id: str | None = None,
    min_: str | None = None,
    max_: str | None = None,
) -> Term:
    term = tx.run(
        Q.create,
        user_id=user_id,
        term_id=term_id,
        term_version_id=term_version_id,
        label=label,
        definition=definition,
        synonyms=synonyms,
        reference=reference,
        translations=translations,
        unit=unit,
        min_=min_,
        max_=max_,
        required=required,
        datatype=datatype,
        vocabulary_id=vocabulary_id,
    ).value("term")[0]
    term_version_id = term["latest_term_version_id"]

    if related:
        tx.run(
            QTermVersion.attach_related,
            id=term_version_id,
            related=related,
        )

    if broader:
        tx.run(
            QTermVersion.set_broader,
            id=term_version_id,
            broader=broader,
            previous_id=None,
        )
    else:
        tx.run(
            Q.set_as_top_level_term,
            id=term["id"],
            vocabulary_id=vocabulary_id,
        )
        tx.run(
            QTermVersion.set_vocpopuli_type,
            id=term_version_id,
            vocpopuli_type=vocpopuli_type,
            vocabulary_id=vocabulary_id,
            previous_id=None,
        )

    if related_external_terms:
        tx.run(
            QTermVersion.attach_external_terms,
            id=term_version_id,
            terms=related_external_terms,
        )

    return dict(term)
    # return Term(**dict(term))


@write
def import_(tx, id: str, author_id: str, vocabulary_id: str, created_at: str) -> Term:
    result = tx.run(
        Q.import_,
        id=id,
        author_id=author_id,
        vocabulary_id=vocabulary_id,
        created_at=created_at,
    ).single()
    return dict(result["term"])


@write_terms_db
def import_terms_db_(tx, id: str) -> Term:
    result = tx.run(
        Q.import_terms_db_,
        id=id,
    ).single()
    return dict(result["term"])


@read
def get(tx, id: str) -> Term:
    result = tx.run(Q.get, id=id).single()
    return dict(result["term"]) if result else None

@read_terms_db
def get_terms_db(tx, id: str) -> Term:
    result = tx.run(Q.get_terms_db, id=id).single()
    return dict(result["term"]) if result else None


@read
def is_archived(tx, id: str, vocabulary_id: str) -> bool:
    result = tx.run(Q.is_archived, id=id, vocabulary_id=vocabulary_id).single()
    return result["is_archived"] if result else None


@write
def delete_detach(tx, id: str, vocabulary_id: str):
    tx.run(Q.delete_detach, id=id, vocabulary_id=vocabulary_id)


@read
def all(tx) -> Term:
    results = tx.run(Q.all)
    return [dict(result["term"]) for result in results]


@read_terms_db
def all_bank(tx, vocabulary_id: str) -> Term:
    results = tx.run(Q.all_bank, vocabulary_id=vocabulary_id).data()
    return [dict(result["term"]) for result in results]


@read_terms_db
def all_bank_no_vocab(tx) -> Term:
    results = tx.run(Q.all_bank_no_vocab).data()
    return [dict(result) for result in results]


@read
def all_top_level(tx, vocabulary_id: str) -> list[Term]:
    results = tx.run(Q.all_top_level, vocabulary_id=vocabulary_id).value("term")
    return [dict(result) for result in results]


@read
def get_versions(tx,
                 id: str,
                 vocabulary_id: str) -> list[TermVersion]:
    results = tx.run(Q.get_versions, id=id, vocabulary_id=vocabulary_id).value("version")
    return [dict(result) for result in results]


@read
def get_versions_with_authors(tx, id: str, vocabulary_id: str) -> list[TermVersion]:
    results = tx.run(Q.get_versions_with_authors, id=id, vocabulary_id=vocabulary_id).values("version", "author")
    return [dict(version, created_by=dict(author)) for version, author in results]


@read
def get_comments(tx, id: str) -> list[User, Comment]:
    results = tx.run(Q.get_comments, id=id).values("author", "comment")
    return [(dict(author), dict(comment)) for author, comment in results]


@write
def set_as_top_level_term(tx,
                          id: str,
                          vocabulary_id: str):
    tx.run(Q.set_as_top_level_term,
           id=id,
           vocabulary_id=vocabulary_id)


@read
def get_for_graph(tx, vocabulary_id: str) -> dict:
    results = tx.run(
        Q.get_nodes_for_graph,
        vocabulary_id=vocabulary_id,
    ).value("results")
    for result in results:
        if result.get("vocpopuli_type") is not None:
            result["vocpopuli_type_color"] = determine_color_vocpopuli_type(result["vocpopuli_type"])
    links = chain.from_iterable(
        ({"source": result["id"], "target": target} for target in related)
        for result in results
        if (related := result.pop("related"))
    )
    custom_relationship_links = chain.from_iterable(
        ({"source": result["id"], "target": relationship["id"], "name": relationship["label"], "id": relationship["relationship_id"] } for relationship in c_related if len(relationship) != 0)
        for result in results
        if (c_related := result.pop("custom_related"))
    )
    return {"nodes": results, "links": list(links) + list(custom_relationship_links)}

@read
def get_for_graph_toplevel(tx, vocabulary_id: str) -> dict:
    results = tx.run(
        Q.get_nodes_for_graph_toplevel,
        vocabulary_id=vocabulary_id,
    ).value("results")
    for result in results:
        if result.get("vocpopuli_type") is not None:
            result["vocpopuli_type_color"] = determine_color_vocpopuli_type(result["vocpopuli_type"])
    links = chain.from_iterable(
        ({"source": result["id"], "target": target} for target in related if result["id"] != target)
        for result in results
        if (related := result.pop("related"))
    )
    custom_relationship_links = chain.from_iterable(
        ({"source": result["id"], "target": relationship["id"], "name": relationship["label"], "id": relationship["relationship_id"] } for relationship in c_related if len(relationship) != 0 and result["id"] != relationship["id"])
        for result in results
        if (c_related := result.pop("custom_related"))
    )
    return {"nodes": results, "links": list(links) + list(custom_relationship_links)}


@read
def get_latest_children(tx, id: str, vocabulary_id: str) -> list[TermVersion]:
    results = tx.run(
        Q.get_latest_children,
        id=id,
        vocabulary_id=vocabulary_id
        ).value("child")
    return [dict(result) for result in results]


@read
def get_parents_and_vocpopuli_type_bulk(tx, vocabulary_id: str, term_ids: list[str]):
    results = tx.run(
        Q.get_parents_and_vocpopuli_type_bulk,
        vocabulary_id=vocabulary_id,
        term_ids=list(term_ids),
    ).data()
    return {
        result["id"]: set(result["parents"]) or {result["vocpopuli_type"]}
        for result in results
    }


@write
def change_vocpopuli_type_bulk(
    tx, terms: dict[str, str], user_id: str, vocabulary_id: str
):
    termsList = [
        {
            "id": id,
            "vocpopuli_type": vocpopuli_type,
        }
        for id, vocpopuli_type in terms.items()
    ]
    tx.run(
        QTermVersion.set_vocpopuli_type_bulk,
        terms=termsList,
        vocabulary_id=vocabulary_id
    )


@write
def change_parents_bulk(
    tx, terms: dict[str, set[str]], user_id: str, vocabulary_id: str
):
    termsList = [
        {"id": id, "new_version_id": f"{generate_uid()}-vp", "parents": list(parents)}
        for id, parents in terms.items()
    ]
    tx.run(
        QTermVersion.set_broader_bulk,
        terms=termsList,
    )


@write
def rearrange_children_bulk(tx, terms: dict[str, list[str]], vocabulary_id: str):
    termsList = [
        {
            "id": id,
            "children": [
                {"id": child, "position": position}
                for position, child in enumerate(children)
            ],
        }
        for id, children in terms.items()
    ]
    tx.run(
        Q.rearrange_children_bulk,
        terms=termsList,
        vocabulary_id=vocabulary_id,
    )
