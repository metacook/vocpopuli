from . import read, write
from ._queries import (
    User as Q,
    TermVersion as QTermVersion,
    Vocabulary as QVocabulary)
from ..models.user import User
from ..models.term_version import TermVersion
from ..models.comment import Comment
from ..models.vocabulary import Vocabulary
from uuid import uuid4
import hashlib


@write
def create(
    tx,
    id: str,
    name: str,
    username: str,
    email: str,
    web_url: str,
    avatar_url: str,
) -> User:
    result = tx.run(
        Q.create,
        id=id,
        name=name,
        username=username,
        email=email,
        web_url=web_url,
        avatar_url=avatar_url,
    ).single()
    return dict(result["user"])


@read
def get(tx, id: str) -> User:
    result = tx.run(Q.get, id=id).single()
    return dict(result["user"]) if result else None


@write
def approve(tx, id: str, term_version_id: str, project_id: str) -> User:

    term_version = (
        tx.run(QTermVersion.get, id=term_version_id, vocabulary_id=project_id).single().get("term_version")
    )
    id_t_local = term_version.get("id_t_local")
    result = tx.run(
        Q.approve,
        id=id,
        term_version_id=term_version_id,
        id_t_local=id_t_local or f"{uuid4().hex}-vp",
    ).single()

    vocabulary_tree = tx.run(QVocabulary.get_term_tree, id=project_id).value("version")
    vocabulary_tree = {result["term_id"]: dict(result) for result in vocabulary_tree}

    id_v_local_str = ""

    id_t_local_all = [x.get("id_t_local","") for x in vocabulary_tree.values()]
    id_v_local_str += "".join(x for x in id_t_local_all)

    broader_all = [x.get("broader",[]) for x in vocabulary_tree.values()]
    broader_all = [x for sublist in broader_all for x in sublist]
    id_v_local_str += "".join(x for x in broader_all)

    narrower_all = [x.get("narrower",[]) for x in vocabulary_tree.values()]
    narrower_all = [x for sublist in narrower_all for x in sublist]
    id_v_local_str += "".join(x for x in narrower_all)

    custom_relationships_all = [x.get("custom_relationships",[]) for x in vocabulary_tree.values()]
    custom_relationships_all = [x for sublist in custom_relationships_all for x in sublist]
    id_v_local_str += "".join(x for x in custom_relationships_all)

    related_all = [x.get("related",[]) for x in vocabulary_tree.values()]
    related_all = [x for sublist in related_all for x in sublist]
    id_v_local_str += "".join(x for x in related_all)

    n_approved_str = f"{len([x for x in id_t_local_all if x]):06}"
    id_v_local = (
                n_approved_str
                + "-"
                + (hashlib.md5(id_v_local_str.encode("utf-8")).hexdigest())
                + "-vp"
            )

    tx.run(QVocabulary.create_id_v_local, id=project_id, id_v_local=id_v_local)

    return dict(result["user"])


@write
def set_as_archived(tx, id: str, term_id: str, project_id:str) -> User:

    result = tx.run(
        Q.set_as_archived,
        id=id,
        term_id=term_id,
        vocabulary_id=project_id,
    ).single()

    vocabulary_tree = tx.run(QVocabulary.get_term_tree, id=project_id).value("version")
    vocabulary_tree = {result["term_id"]: dict(result) for result in vocabulary_tree}

    id_v_local_str = ""
    id_t_local_all = [x.get("id_t_local","") for x in vocabulary_tree.values()]
    id_v_local_str += "".join(x for x in id_t_local_all)

    broader_all = [x.get("broader",[]) for x in vocabulary_tree.values()]
    broader_all = [x for sublist in broader_all for x in sublist]
    id_v_local_str += "".join(x for x in broader_all)

    narrower_all = [x.get("narrower",[]) for x in vocabulary_tree.values()]
    narrower_all = [x for sublist in narrower_all for x in sublist]
    id_v_local_str += "".join(x for x in narrower_all)

    custom_relationships_all = [x.get("custom_relationships",[]) for x in vocabulary_tree.values()]
    custom_relationships_all = [x for sublist in custom_relationships_all for x in sublist]
    id_v_local_str += "".join(x for x in custom_relationships_all)

    related_all = [x.get("related",[]) for x in vocabulary_tree.values()]
    related_all = [x for sublist in related_all for x in sublist]
    id_v_local_str += "".join(x for x in related_all)

    n_approved_str = f"{len([x for x in id_t_local_all if x]):06}"
    id_v_local = (
                n_approved_str
                + "-"
                + (hashlib.md5(id_v_local_str.encode("utf-8")).hexdigest())
                + "-vp"
            )

    tx.run(QVocabulary.create_id_v_local, id=project_id, id_v_local=id_v_local)

    return dict(result["user"])


@write
def vote_on_term_version(tx, id: str, term_version_id: str, vote: str):
    """
    Removes any existing user's vote on the given term version and, if `vote`
    is not an empty string, adds a vote corresponding to `vote`.
    """
    previous_vote = tx.run(Q.remove_vote, id=id, term_version_id=term_version_id)
    if vote == "up" and previous_vote != "up":
        tx.run(Q.thumb_up, id=id, term_version_id=term_version_id)
    if vote == "down" and previous_vote != "down":
        tx.run(Q.thumb_down, id=id, term_version_id=term_version_id)


@write
def comment_on_term_version(tx, id: str, term_version_id: str, body: str) -> Comment:
    result = tx.run(
        Q.comment_on_term_version,
        id=id,
        term_version_id=term_version_id,
        body=body,
        comment_id=uuid4().hex,
    ).single()

    return result["comment"]


@read
def get_vocabularies(tx, id: str) -> list[Vocabulary]:
    results = tx.run(Q.get_vocabularies, id=id).value("vocabulary")
    return [dict(result) for result in results]


@write
def set_orcid_id(tx, id: str, orcid_id: str):
    tx.run(
        Q.set_orcid_id,
        id=id,
        orcid_id=orcid_id.removeprefix("https://orcid.org/"),
    )


@read
def get_vocabulary_stats(tx, id: int, vocabulary_id: str):
    vocabulary = dict(
        tx.run(Q.get_vocabulary, id=id, vocabulary_id=vocabulary_id).single()["vocabulary"]
        )
    terms_stats = dict(
        tx.run(Q.get_terms_stats, id=id, vocabulary_id=vocabulary_id).single()["terms_stats"]
        )
    relationship_stats = dict(
        tx.run(Q.get_relationship_stats, id=id, vocabulary_id=vocabulary_id).single()["relationship_stats"]
        )
    additional_stats = dict(
        tx.run(Q.get_additional_stats, id=id, vocabulary_id=vocabulary_id).single()["additional_stats"]
        )

    return {**vocabulary, **terms_stats, **relationship_stats, **additional_stats}
