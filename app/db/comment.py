from . import read
from ._queries import Comment as Q
from ..models.comment import Comment


@read
def get(tx, id: str) -> Comment:
    result = tx.run(Q.get, id=id).single()["comment"]
    return dict(result)
