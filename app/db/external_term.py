from . import write
from ._queries import ExternalTerm as Q
from ..models.external_term import ExternalTerm
from uuid import uuid4


@write
def get_by_url(tx, url: str) -> ExternalTerm:
    return tx.run(Q.get_by_url, url=url, id=uuid4().hex).value("externalTerm")
