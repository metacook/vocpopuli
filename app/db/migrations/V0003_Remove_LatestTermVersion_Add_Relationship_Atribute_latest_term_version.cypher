MATCH (n:LatestTermVersion)-[r:IS_VERSION_OF]->(:Term)
REMOVE n:LatestTermVersion
SET r.latest_term_version = true
