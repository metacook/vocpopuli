// remove color attribute from all the vocpopuli types
MATCH (n:VocPopuliType)
REMOVE n.color;