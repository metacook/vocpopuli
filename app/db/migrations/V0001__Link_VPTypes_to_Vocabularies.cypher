// mark existing types with a :DeleteMe label
MATCH (type:VocPopuliType)
    WHERE NOT EXISTS((type)-[:BELONGS_TO]->(:Vocabulary))
SET type:DeleteMe
;

// create default types for all vocabularies
MATCH (vocabulary:Vocabulary)
WITH collect(vocabulary) AS vocabularies
FOREACH (
    v in vocabularies |
    MERGE (process:VocPopuliType {name: "process", color: "#0e6ffd"})
    MERGE (object:VocPopuliType {name: "object", color: "#0dcbf0"})
    MERGE (data:VocPopuliType {name: "data", color: "#ffc108"})
    MERGE (process)-[:BELONGS_TO]->(v)
    MERGE (object)-[:BELONGS_TO]->(v)
    MERGE (data)-[:BELONGS_TO]->(v)
)
;

// remap all TermVersions from the old types to the new ones
MATCH (old:DeleteMe)<-[:IS_OF_TYPE]-(tv:TermVersion)-[:IS_VERSION_OF]->
    (:Term)-[:BELONGS_TO]->(:Vocabulary)<-[:BELONGS_TO]-(new:VocPopuliType)
    WHERE old.name = new.name
CREATE (tv)-[:IS_OF_TYPE]->(new)
;

// delete the old types
MATCH (deleteMe:DeleteMe)
DETACH DELETE deleteMe
;

// add ids to the new types
MATCH (type:VocPopuliType)
    WHERE type.id IS NULL
SET type.id = apoc.create.uuid()
;
