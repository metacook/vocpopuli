// mark existing types (:Datatype, with lowercase T) with a :DeleteMe label
MATCH (type:Datatype)
SET type:DeleteMe
;

// create default types for all vocabularies
MATCH (vocabulary:Vocabulary)
WITH collect(vocabulary) AS vocabularies
FOREACH (
    v in vocabularies |
    MERGE (boolean:DataType {
        name: "boolean",
        color: "#44bcd8",
        icon: "bi-plus-slash-minus",
        numeric: false,
        help_text: "A single boolean value which can either be <i>true</i> or <i>false</i>."
    })
    MERGE (boolean)-[:BELONGS_TO]->(v)
    MERGE (integer:DataType {
        name: "integer",
        color: "#e28743",
        icon: "bi-123",
        numeric: true,
        help_text: "A single integer value. Integer values can optionally have a unit further describing them."
    })
    MERGE (integer)-[:BELONGS_TO]->(v)
    MERGE (float:DataType {
        name: "float",
        color: "#eab676",
        icon: "bi-dot",
        numeric: true,
        help_text: "A single floating point value. Float values can optionally have a unit further describing them."
    })
    MERGE (float)-[:BELONGS_TO]->(v)
    MERGE (string:DataType {
        name: "string",
        color: "#1e81b0",
        icon: "bi-file-text",
        numeric: false,
        help_text: "A single text value."
    })
    MERGE (string)-[:BELONGS_TO]->(v)
    MERGE (date:DataType {
        name: "date",
        color: "#358166",
        icon: "bi-calendar-plus",
        numeric: false,
        help_text: "A date and time value which can be selected from a date picker."
    })
    MERGE (date)-[:BELONGS_TO]->(v)
    MERGE (list:DataType {
        name: "list",
        color: "#154c79",
        icon: "bi-list-ul",
        numeric: false,
        help_text: "A nested value which is similar to dictionaries with the difference that none of the values in a list have a key."
    })
    MERGE (list)-[:BELONGS_TO]->(v)
    MERGE (dictionary:DataType {
        name: "dictionary",
        color: "#365985",
        icon: "bi-book",
        numeric: false,
        help_text: "A nested value which can be used to combine multiple metadata entries under a single key."
    })
    MERGE (dictionary)-[:BELONGS_TO]->(v)
    MERGE (individual:DataType {
        name: "individual",
        color: "#76b5c5",
        icon: "bi-person",
        numeric: false,
        help_text: ""
    })
    MERGE (individual)-[:BELONGS_TO]->(v)
    MERGE (option:DataType {
        name: "option",
        color: "#76b5c5",
        icon: "bi-circle-fill",
        numeric: false,
        help_text: ""
    })
    MERGE (option)-[:BELONGS_TO]->(v)
    MERGE (record:DataType {
        name: "record",
        color: "#707070",
        icon: "bi-journals",
        numeric: false,
        help_text: ""
    })
    MERGE (record)-[:BELONGS_TO]->(v)
    MERGE (none:DataType {
        name: "none",
        color: "#808080",
        icon: "bi-dash-circle-dotted",
        numeric: false,
        help_text: ""
    })
    MERGE (none)-[:BELONGS_TO]->(v)
)
;

// remap all DataTypes from the old types to the new ones
MATCH (old:DeleteMe)<-[:IS_OF_TYPE]-(tv:TermVersion)-[:IS_VERSION_OF]->
    (:Term)-[:BELONGS_TO]->(:Vocabulary)<-[:BELONGS_TO]-(new:DataType)
    WHERE old.name = new.name
CREATE (tv)-[:IS_OF_TYPE]->(new)
;

// delete the old types
MATCH (deleteMe:DeleteMe)
DETACH DELETE deleteMe
;

// add ids to the new types
MATCH (type:DataType)
    WHERE type.id IS NULL
SET type.id = apoc.create.uuid()
;
