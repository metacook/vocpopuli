from . import read, write, write_terms_db, read_terms_db
from ._queries import TermVersion as Q, Term as QTerm, ExternalTerm as QExternalTerm
from ..models.file import File
from ..models.term import Term
from ..models.term_version import TermVersion
from uuid import uuid4


@write
def update(
    tx,
    id: str,
    broader: list[str],
    related: list[str],
    related_external_terms: list[str],
    vocabulary_id: str,
    vocpopuli_type: str | None = None,
) -> TermVersion:
    term_version = tx.run(
        Q.get,
        id=id,
        vocabulary_id=vocabulary_id).single()["term_version"]
    term = tx.run(QTerm.get, id=term_version["term_id"]).single()["term"]

    if related:
        tx.run(
            Q.attach_related,
            id=id,
            related=related,
        )

    if broader:
        tx.run(Q.set_broader, id=id, broader=broader, previous_id=id)
        if term["is_top_level"]:
            tx.run(QTerm.unset_as_top_level_term, id=term["id"])
    else:
        tx.run(
            Q.set_vocpopuli_type,
            id=id,
            vocpopuli_type=vocpopuli_type,
            vocabulary_id=vocabulary_id,
            previous_id=id
        )
        tx.run(
            QTerm.set_as_top_level_term,
            id=term["id"],
            vocabulary_id=vocabulary_id
        )

    if related_external_terms:
        tx.run(
            Q.attach_external_terms,
            id=id,
            terms=related_external_terms,
        )

    return dict(term_version)


@write
def create(
    tx,
    id: str,
    term_id: str,
    user_id: str,
    label: str,
    definition: str,
    reference: str,
    synonyms: list[str],
    translations: list[str],
    unit: str,
    datatype: str,
    broader: list[str],
    related: list[str],
    related_external_terms: list[str],
    required: bool,
    vocabulary_id: str,
    generated_from: str = None,
    vocpopuli_type: str | None = None,
    min_: str | None = None,
    max_: str | None = None,
) -> TermVersion:
    term_version = tx.run(
        Q.create,
        id=id,
        term_id=term_id,
        user_id=user_id,
        label=label,
        definition=definition,
        reference=reference,
        synonyms=synonyms,
        translations=translations,
        unit=unit,
        min_=min_,
        max_=max_,
        required=required,
        datatype=datatype,
        generated_from=generated_from,
        vocabulary_id=vocabulary_id,
    ).single()["term_version"]
    term = tx.run(QTerm.get, id=term_version["term_id"]).single()["term"]

    if related:
        tx.run(
            Q.attach_related,
            id=id,
            related=related,
        )

    if broader:
        tx.run(Q.set_broader, id=id, broader=broader, previous_id=generated_from)
        if term["is_top_level"]:
            tx.run(QTerm.unset_as_top_level_term,
                   id=term["id"],
                   vocabulary_id=vocabulary_id)
    else:
        tx.run(
            Q.set_vocpopuli_type,
            id=id,
            vocpopuli_type=vocpopuli_type,
            vocabulary_id=vocabulary_id,
            previous_id=generated_from,
        )
        tx.run(
            QTerm.set_as_top_level_term,
            id=term["id"],
            vocabulary_id=vocabulary_id,
        )

    if related_external_terms:
        tx.run(
            Q.attach_external_terms,
            id=id,
            terms=related_external_terms,
        )

    return dict(term_version)


@write
def import_(
    tx,
    id: str,
    local_id: str,
    issue_iid: str,
    term_id: str,
    author_id: str,
    issue_version: str,
    label: str,
    definition: str,
    synonyms: list[str],
    translations: list[str],
    unit: str,
    created_at: str,
    datatype: str,
    related_external_terms: list[str],
    generated_from: str,
    min_: str,
    max_: str,
    required: bool,
    purl: str,
    gitlab_skos_url: str,
    vocabulary_id: str,
    vocpopuli_type: str | None = None,
) -> TermVersion:
    term_version = tx.run(
        Q.import_,
        id=id,
        term_id=term_id,
        issue_iid=issue_iid,
        author_id=author_id,
        issue_version=issue_version,
        label=label,
        definition=definition,
        synonyms=synonyms,
        translations=translations,
        unit=unit,
        created_at=created_at,
        datatype=datatype,
        generated_from=generated_from,
        min_=min_,
        max_=max_,
        required=required,
        purl=purl,
        gitlab_skos_url=gitlab_skos_url,
    ).single()["term_version"]

    if local_id:
        tx.run(Q.set_id_t_local, id=id, id_t_local=local_id)

    if vocpopuli_type and vocpopuli_type != "relationship_type":
        tx.run(Q.set_vocpopuli_type,
               id=id,
               vocpopuli_type=vocpopuli_type,
               vocabulary_id=vocabulary_id,
               previous_id=None)

    if related_external_terms:
        tx.run(
            Q.attach_external_terms,
            id=id,
            terms=related_external_terms,
        )

    return dict(term_version)


@write_terms_db
def import_terms_db_(
    tx,
    vocabulary_id: str,
    id: str,
    id_t_local: str,
    term_id: str,
    label: str,
    definition: str,
    synonyms: list[str],
    translations: list[str],
    unit: str,
    created_at: str,
    datatype: str,
    related_external_terms: list[str],
    min_: str,
    max_: str,
    required: bool,
    purl: str,
    gitlab_skos_url: str,
    vocpopuli_type: str | None = None,
) -> TermVersion:
    term_version = tx.run(
        Q.import_terms_db_,
        vocabulary_id=vocabulary_id,
        id=id,
        term_id=term_id,
        label=label,
        definition=definition,
        synonyms=synonyms,
        translations=translations,
        unit=unit,
        created_at=created_at,
        datatype=datatype,
        min_=min_,
        max_=max_,
        required=required,
        purl=purl,
        gitlab_skos_url=gitlab_skos_url,
        id_t_local=id_t_local,
    ).single()["term_version"]

    if vocpopuli_type and vocpopuli_type != "relationship_type":
        tx.run(Q.set_vocpopuli_type_terms_db_,
               id=id,
               vocpopuli_type=vocpopuli_type)
    if related_external_terms:
        tx.run(
            Q.attach_external_terms,
            id=id,
            terms=related_external_terms,
        )

    return dict(term_version)


@write
def set_broader(tx, id: str, broader: list[str]):
    tx.run(Q.set_broader, id=id, broader=broader, previous_id=None)


@write
def set_id_t_local(tx, id: str, local_id: str):
    tx.run(Q.set_id_t_local, id=id, id_t_local=local_id)


@write
def attach_related(tx, id: str, related: list[str]):
    tx.run(Q.attach_related, id=id, related=related)

@write_terms_db
def attach_broader_terms_db_(tx, id: str, broader: list[str], vocabulary_id: str):
    tx.run(Q.attach_broader_terms_db_, id=id, broader=broader, vocabulary_id=str(vocabulary_id))


@write_terms_db
def attach_related_terms_db_(tx, id: str, related: list[str], vocabulary_id: str):
    tx.run(Q.attach_related_terms_db_, id=id, related=related, vocabulary_id=str(vocabulary_id))


@read
def get(tx,
        id: str,
        vocabulary_id: str) -> TermVersion:
    result = tx.run(Q.get,
                    id=id,
                    vocabulary_id=vocabulary_id).single()
    return dict(result["term_version"]) if result else None


@read_terms_db
def get_terms_db(tx, id: str) -> TermVersion:
    result = tx.run(Q.get_terms_db, id=id).single()
    return dict(result["term_version"]) if result else None


@read_terms_db
def get_term_branch_versions_db(tx, term_version_id) -> list[TermVersion]:
    result = tx.run(Q.get_term_branch_versions_db, term_version_id=term_version_id).data()
    return {dict(term_version)["term_version"]["term_id"]: dict(term_version)["term_version"] for term_version in result}


@read_terms_db
def get_all_terms_db(tx, vocabulary_id: str) -> list[TermVersion]:
    result = tx.run(Q.get_all_terms_db, vocabulary_id=vocabulary_id).data()
    return [dict(term_version) for term_version in result]


@read_terms_db
def get_all_terms_db_no_vocab(tx) -> list[TermVersion]:
    result = tx.run(Q.get_all_terms_db_no_vocab).data()
    return [dict(term_version["term_version"]) for term_version in result]


@read
def all_latest_versions(tx, vocabulary_id: str) -> list[TermVersion]:
    results = tx.run(Q.all_latest_versions, vocabulary_id=vocabulary_id).values(
        "version", "broader", "narrower"
    )
    return [
        dict(version) | {"broader": broader, "narrower": narrower}
        for version, broader, narrower in results
    ]


@read
def all_latest_versions_with_custom_relationships(
    tx, vocabulary_id: str
) -> list[TermVersion]:
    results = tx.run(
        Q.all_latest_versions_with_custom_relationships, vocabulary_id=vocabulary_id
    ).values("version", "broader", "narrower", "relationships")
    return [
        dict(version)
        | {"broader": broader, "narrower": narrower, "relationships": relationships}
        for version, broader, narrower, relationships in results
    ]


@read
def get_by_issue_iid(tx, issue_iid: str) -> TermVersion:
    result = tx.run(Q.get_by_issue_iid, issue_iid=issue_iid).single()
    return dict(result["term_version"]) if result else None


@read
def get_related(tx, id: str) -> list[Term]:
    results = tx.run(Q.get_related, id=id).value("related")
    return [dict(result) for result in results]


@read
def get_broader(tx, id: str) -> list[Term]:
    results = tx.run(Q.get_broader, id=id).value("broader")
    return [dict(result) for result in results]


@read
def get_files(tx, id: str) -> list[File]:
    results = tx.run(Q.get_files, id=id).value("files")
    return [dict(result) for result in results]


@read
def get_image_ids(tx, id: str) -> list[str]:
    return tx.run(Q.get_image_ids, id=id).single()["image_ids"]


@read
def get_image_urls(tx, id: str) -> list[str]:
    return tx.run(Q.get_image_urls, id=id).single()["image_urls"]


@read
def get_votes(tx, id: str) -> dict:
    results = tx.run(Q.get_votes, id=id).single()
    return dict(results)


@read
def get_user_vote(tx, id: str, user_id: str) -> str:
    result = tx.run(Q.get_user_vote, id=id, user_id=user_id).value("vote")
    if result == ["THUMBED_UP"]:
        return "up"
    elif result == ["THUMBED_DOWN"]:
        return "down"
    else:
        return ""


@write
def update_issue_iid(tx, id: str, issue_iid: str) -> TermVersion:
    result = tx.run(Q.update_issue_iid, id=id, issue_iid=issue_iid).single()
    return dict(result["term_version"])


@read
def get_approval_status(tx,
                        id: str,
                        vocabulary_id: str) -> str:
    return tx.run(Q.get_approval_status, id=id, vocabulary_id=vocabulary_id).single()["approval_status"]


@write
def keep_images(tx, id: str) -> int:
    return tx.run(Q.keep_images, id=id).single()["file_count"]


@write
def set_latest(tx, id: str, vocabulary_id: str):
    tx.run(Q.set_latest, id=id, vocabulary_id=vocabulary_id)


@write
def set_vocpopuli_type(tx, id: str, vocpopuli_type: str, vocabulary_id: str):
    tx.run(Q.set_vocpopuli_type, id=id, vocpopuli_type=vocpopuli_type, vocabulary_id=vocabulary_id, previous_id=None)


@write
def add_custom_relationships(tx,
                             id: str,
                             relationships: list[dict],
                             vocabulary_id: str):
    cleaned_relationships = {}
    for entry in relationships:
        type_id = entry["type_id"]
        related = set(entry["related"])

        if type_id in cleaned_relationships:
            cleaned_relationships[type_id] |= related
        else:
            cleaned_relationships[type_id] = related
    
    # set old relationships to inactive
    tx.run(
        Q.deactivate_old_relationships,
        id=id,
        vocabulary_id=vocabulary_id,
    )

    for type_id, related in cleaned_relationships.items():
        if not related:
            continue

        relationship_id = uuid4().hex
        tx.run(
            Q.add_custom_relationship,
            id=id,
            relationship_id=relationship_id,
            relationship_type=type_id,
            vocabulary_id=vocabulary_id,
        )
        tx.run(
            Q.add_custom_relationship_objects,
            relationship_id=relationship_id,
            related_terms=list(related),
            vocabulary_id=vocabulary_id,
        )


@read
def get_custom_relationships(tx, id: str, vocabulary_id: str) -> list[dict]:
    results = tx.run(Q.get_custom_relationships, id=id, vocabulary_id=vocabulary_id)
    return [dict(result) for result in results]


@read
def get_custom_relationships_full(tx,
                                  id: str,
                                  vocabulary_id: str) -> list[dict]:
    results = tx.run(Q.get_custom_relationships_full, id=id, vocabulary_id=vocabulary_id)
    return [dict(result) for result in results]


@write
def restore_term(tx, id: str):
    tx.run(Q.restore_term, id=id)