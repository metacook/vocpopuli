class Comment:
    get = """
        MATCH (comment:Comment {id: $id})-[:ON_VERSION]->(version:TermVersion)
        RETURN comment {
            .*,
            issue_iid: version.issue_iid
        }
    """


class DataType:
    get_all = """
        MATCH (type:DataType)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (type)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(t:DataType)
        WITH type, COLLECT(t.name) AS parent_types
        RETURN type {.*, parent_types: parent_types} AS type
    """
    get_all_bank = """
        MATCH (version:TermVersion)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (type:DataType)<-[:IS_OF_TYPE]-(version)
        RETURN type
    """
    reorder = """
        UNWIND $datatypes AS type
        MATCH (vocab:Vocabulary {id: $vocabulary_id})<-[b:BELONGS_TO]-(dt:DataType {name: type[1]})
        SET b.position_number = type[0]
    """
    get_all_with_order_and_instances = """
        MATCH (t:DataType)-[belongs_to:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (t)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(pt:DataType)
        OPTIONAL MATCH (t)<-[:IS_OF_TYPE]-(tv:TermVersion)-[:IS_VERSION_OF]->(:Term)-[:BELONGS_TO]->(vocabulary)
        WITH  t AS t, COUNT(tv) AS instances, belongs_to AS belongs_to, COLLECT (distinct pt.name) AS parent_types
        RETURN t {
            .*,
            instances: instances,
            position: belongs_to.position_number,
            parent_types: parent_types
        } AS type
        ORDER BY type.position
    """
    delete = """
        MATCH (type:DataType {name: $datatype})-[b:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (type)-[:BELONGS_TO]->(v:Vocabulary)
        WITH type, b, COUNT(v) AS vocabularies
            DELETE b
        WITH type, b, vocabularies
        WHERE vocabularies = 1
            DETACH DELETE type
    """
    edit = """
        MATCH (type:DataType {name: $old_name})-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (type)-[old_parents:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(:DataType)
        DELETE old_parents
        SET type.name = $name, 
            type.numeric = $numeric,
            type.color = $color,
            type.help_text = $help_text,
            type.icon = $icon,
            type.toplevel = $toplevel
        WITH type, vocabulary
        UNWIND $parents AS parent
        MATCH (p:DataType {name: parent})-[:BELONGS_TO]->(vocabulary)
        CREATE (type)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(p)
    """
    names = """
        MATCH (type:DataType)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        RETURN DISTINCT type.name AS type
    """
    create = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        CREATE (type:DataType {
            id: $id,
            name: $name,
            color: $color,
            icon: $icon,
            numeric: $numeric,
            toplevel: $toplevel,
            help_text: $help_text
        })
        CREATE (type)-[:BELONGS_TO]->(vocabulary)
        WITH type, vocabulary
        UNWIND $parents AS parent
        MATCH (p:DataType {name: parent})-[:BELONGS_TO]->(vocabulary)
        CREATE (type)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(p)
        RETURN type
    """
    import_terms_db_ = """
        MERGE (type:DataType {
            name: $name,
            color: $color,
            icon: $icon,
            numeric: $numeric,
            help_text: $help_text
        })
        RETURN type
    """
    import_terms_db_ = """
        MERGE (type:DataType {
            name: $name,
            color: $color,
            icon: $icon,
            numeric: $numeric,
            help_text: $help_text
        })
        RETURN type
    """
    create_default_types = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MERGE (boolean:DataType {
            name: "boolean",
            color: "#44bcd8",
            icon: "bi-plus-slash-minus",
            numeric: false,
            toplevel: false,
            help_text: "A single boolean value which can either be <i>true</i> or <i>false</i>."
        })
        MERGE (boolean)-[:BELONGS_TO]->(vocabulary)
        MERGE (integer:DataType {
            name: "integer",
            color: "#e28743",
            icon: "bi-123",
            numeric: true,
            toplevel: false,
            help_text: "A single integer value. Integer values can optionally have a unit further describing them."
        })
        MERGE (integer)-[:BELONGS_TO]->(vocabulary)
        MERGE (float:DataType {
            name: "float",
            color: "#eab676",
            icon: "bi-dot",
            numeric: true,
            toplevel: false,
            help_text: "A single floating point value. Float values can optionally have a unit further describing them."
        })
        MERGE (float)-[:BELONGS_TO]->(vocabulary)
        MERGE (string:DataType {
            name: "string",
            color: "#1e81b0",
            icon: "bi-file-text",
            numeric: false,
            toplevel: false,
            help_text: "A single text value."
        })
        MERGE (string)-[:BELONGS_TO]->(vocabulary)
        MERGE (date:DataType {
            name: "date",
            color: "#358166",
            icon: "bi-calendar-plus",
            numeric: false,
            toplevel: false,
            help_text: "A date and time value which can be selected from a date picker."
        })
        MERGE (date)-[:BELONGS_TO]->(vocabulary)
        MERGE (dictionary:DataType {
            name: "dictionary",
            color: "#365985",
            icon: "bi-book",
            numeric: false,
            toplevel: false,
            help_text: "A nested value which can be used to combine multiple metadata entries under a single key."
        })
        MERGE (dictionary)-[:BELONGS_TO]->(vocabulary)
        MERGE (option:DataType {
            name: "option",
            color: "#76b5c5",
            icon: "bi-circle-fill",
            numeric: false,
            toplevel: false,
            help_text: ""
        })
        MERGE (option)-[:BELONGS_TO]->(vocabulary)
        MERGE (record:DataType {
            name: "record",
            color: "#707070",
            icon: "bi-journals",
            numeric: false,
            toplevel: true,
            help_text: ""
        })
        MERGE (record)-[:BELONGS_TO]->(vocabulary)
        MERGE (none:DataType {
            name: "none",
            color: "#808080",
            icon: "bi-dash-circle-dotted",
            numeric: false,
            toplevel: false,
            help_text: ""
        })
        MERGE (none)-[:BELONGS_TO]->(vocabulary)
        MERGE (subworkflow:DataType {
            name: "sub-workflow",
            color: "#b06e1e",
            icon: "bi-arrow-repeat",
            numeric: false,
            toplevel: false,
            help_text: "A datatype that is identical to a dictionary, but can be interpreted as having multiple chained dictionaries. Each one has a 'Sequential Order Position'."
        })
        MERGE (subworkflow)-[:BELONGS_TO]->(vocabulary)
        MERGE (boolean)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (boolean)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (integer)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (integer)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (float)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (float)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (string)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (string)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (date)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (date)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (dictionary)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (dictionary)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (option)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(integer)
        MERGE (option)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(float)
        MERGE (option)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(string)
        MERGE (option)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(date) 
        MERGE (subworkflow)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(dictionary)
        MERGE (subworkflow)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(record)
        MERGE (subworkflow)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
        MERGE (dictionary)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
        MERGE (boolean)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
        MERGE (string)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
        MERGE (float)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
        MERGE (integer)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
        MERGE (date)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(subworkflow)
    """
    merge = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MERGE (type:DataType {name: $name})
            ON CREATE
                SET type.id = apoc.create.uuid(),
                    type.color = "gray",
                    type.icon = "bi-question-diamond",
                    type.numeric = false,
                    type.toplevel = true,
                    type.help_text = ""
        MERGE (type)-[:BELONGS_TO]->(vocabulary)
        RETURN type
    """
    is_numeric = """
        MATCH (type:DataType {name: $name})-[:BELONGS_TO]->
            (:Vocabulary {id: $vocabulary_id})
        RETURN type.numeric
    """
    numeric_types = """
        MATCH (type:DataType)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
            WHERE type.numeric
        RETURN collect(type.name) AS types
    """
    toplevel_types = """
        MATCH (type:DataType)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
            WHERE type.toplevel
        RETURN collect(type.name) AS types
    """
    get_child_datatype_configuration = """
        MATCH (type:DataType)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (n:DataType)-[:PARENT_DATATYPE {vocabulary_id: $vocabulary_id}]->(type)
        WITH type, collect(n.name) AS childNames
        RETURN DISTINCT type { 
            .*,
            child_types: childNames } AS types
    """


class ExternalTerm:
    get_by_url = """
        MERGE (externalTerm:ExternalTerm {url: $url})
        ON CREATE
            SET externalTerm.id = $id,
            created_at = datetime()
        RETURN externalTerm
    """


class File:
    create = """
        MATCH (term_version:TermVersion {id: $term_version_id})
        CREATE (file: File {id: $file_id, url: $url})
        SET file.local = true
        MERGE (file)-[:IS_ATTACHED_TO {order_id: $order_id}]->(term_version)
        RETURN file
    """
    import_ = """
        MATCH (term_version:TermVersion {id: $term_version_id})
        CREATE (file: File {id: $file_id, url: $url})
        SET file.local = false
        MERGE (file)-[:IS_ATTACHED_TO {order_id: $order_id}]->(term_version)
        RETURN file
    """
    get = """
        MATCH (file:File {id: $id})
        RETURN file
    """
    set_url = """
        MATCH (file:File {id: $id})
        SET file.url = $new_url
        SET file.local = $local
        RETURN file
    """


class RelationshipType:
    create = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (user:User {id: $user_id})
        CREATE (relationship_type:RelationshipType {
            id_t_global: $id_t_global,
            id_t_local: $id_t_local,
            id_t_version: $id_t_version,
            label: $label,
            definition: $definition,
            related_iri: $related_iri,
            created_at: datetime()
        })
        MERGE (relationship_type)-[:BELONGS_TO]->(vocabulary)
        MERGE (user)-[:AUTHORED]->(relationship_type)
        RETURN relationship_type
    """
    edit = """
        MATCH (relationship_type:RelationshipType {id_t_global: $relationship_id})
            -[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        SET relationship_type.definition = $definition,
            relationship_type.related_iri = $related_iri
    """
    import_ = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (user:User {id: $user_id})
        CREATE (relationship_type:RelationshipType {
            id_t_global: $id_t_global,
            id_t_local: $id_t_local,
            id_t_version: $id_t_version,
            issue_iid: $issue_iid,
            label: $label,
            definition: $definition,
            created_at: $created_at
        })
        MERGE (relationship_type)-[:BELONGS_TO]->(vocabulary)
        MERGE (user)-[:AUTHORED]->(relationship_type)
        RETURN relationship_type
    """
    remove = """
        MATCH (relationship_type:RelationshipType {id_t_global: $id})-[r1:BELONGS_TO]->(:Vocabulary)
        MATCH (relationship_type)<-[r2:AUTHORED]-(:User)
        DELETE r1, r2, relationship_type
    """
    get = """
        MATCH (relationship_type:RelationshipType {id_t_global: $id})
        RETURN relationship_type
    """
    get_all_with_relationships = """
        MATCH (relationship_type:RelationshipType)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        MATCH (version:TermVersion)-[:IS_SUBJECT_OF]->
            (relationship:Relationship)-[:HAS_OBJECT]->(objectTerm:Term),
            (relationship)-[:IS_OF_TYPE]->(relationship_type)
        WITH relationship_type,
            collect(version.id) as version_id,
            collect(relationship.id) as relationship_id,
            collect(objectTerm.id) as term_id
        RETURN relationship_type{
            .*,
            version_id: version_id,
            relationship_id: relationship_id,
            term_id: term_id
        }
    """
    update_issue_iid = """
        MATCH (relationship_type:RelationshipType {id_t_global: $id})
        SET relationship_type.issue_iid = $issue_iid
    """
    reorder = """
        UNWIND $relationship_types AS type
        MATCH (vocab:Vocabulary {id: $id})<-[b:BELONGS_TO]-(dt:RelationshipType {id_t_global: type[1]})
        SET b.position_number = type[0]
    """
    import_terms_db_ = """
        MERGE (relationship_type:RelationshipType {
            id_t_global: $id_t_global,
            id_t_local: $id_t_local,
            id_t_version: $id_t_version,
            label: $label,
            definition: $definition
        })
        ON CREATE SET relationship_type.created_at = datetime()
        RETURN relationship_type
    """
    import_relationships_terms_db_ = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (version_subject:TermVersion {id: $version_id})
        MATCH (version_object:TermVersion)-[:IS_PUBLISHED_VERSION_OF]->(:Term {id: $term_id})
        MATCH (relationship_type:RelationshipType {id_t_global: $id_t_global})
        MERGE (relationship:Relationship {id: $relationship_id})
        
        MERGE (version_subject)-[:IS_SUBJECT_OF {vocabulary_id: $vocabulary_id}]->(relationship)
            -[:IS_OF_TYPE]->(relationship_type)
        MERGE (relationship)-[:HAS_OBJECT {vocabulary_id: $vocabulary_id}]->(version_object)
    """
    import_from_terms_db_ = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (user:User {id: $user_id})
        MERGE (relationship_type:RelationshipType {
            id_t_global: $id_t_global,
            id_t_local: $id_t_local,
            id_t_version: $id_t_version,
            label: $label,
            definition: $definition,
            created_at: $created_at
        })
        MERGE (relationship_type)-[:BELONGS_TO]->(vocabulary)
        MERGE (user)-[:AUTHORED]->(relationship_type)
        RETURN relationship_type
    """
    create_relationship = """
        MATCH (version:TermVersion {id: $subject_version_id})
        MATCH (type:RelationshipType {id_t_global: $relationship_type_id})
        MATCH (related:Term {id: $object_term_id})
        MERGE (relationship:Relationship {id: $relationship_id})
        MERGE (version)-[:IS_SUBJECT_OF]->(relationship)
            -[:IS_OF_TYPE]->(type)
        MERGE (relationship)-[:HAS_OBJECT]->(related)
    """


class Term:
    set_as_top_level_term = """
        MATCH (term:Term {id: $id})
            -[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        SET term:TopLevelTerm
    """
    create = """
        MATCH (user:User {id: $user_id})
        MERGE (vocabulary:Vocabulary {id: $vocabulary_id})
        WITH user, vocabulary
        MERGE (dataType:DataType {name: $datatype})-[:BELONGS_TO]->
            (vocabulary)
        MERGE (term:Term {id: $term_id})-[:BELONGS_TO]->(vocabulary)
        ON CREATE SET term.created_at = datetime()

        MERGE (version:TermVersion:TermVersion {
            id: $term_version_id,
            issue_version: 0,
            label: $label,
            definition: $definition,
            reference: $reference,
            synonyms: $synonyms,
            translations: $translations,
            unit: $unit,
            min_: $min_,
            max_: $max_,
            required: $required,
            created_at: datetime()
        })
        MERGE (version)-[:IS_VERSION_OF {latest_term_version: true}]->(term)
        MERGE (user)-[:AUTHORED]->(version)
        MERGE (user)-[:AUTHORED]->(term)
        MERGE (version)-[:IS_OF_TYPE {vocabulary_id: $vocabulary_id}]->(dataType)

        RETURN term {
            .*,
            label: version.label,
            id_t_global: version.id,
            latest_term_version_id: version.id
        }
    """
    import_ = """
        MATCH (author:User {id: $author_id})
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        CREATE (term:Term {
            id: $id,
            created_at: datetime($created_at)
        })
        MERGE (term)-[:BELONGS_TO]->(vocabulary)
        MERGE (author)-[:AUTHORED]->(term)
        RETURN term
    """
    import_terms_db_ = """
        MERGE (term:Term {
            id: $id
        })
        ON CREATE SET term.created_at = datetime()
        RETURN term
    """
    get = """
        MATCH (term:Term {id: $id})
        OPTIONAL MATCH (term)<-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
        MATCH (term)<-[:AUTHORED]-(user:User)
        RETURN term {
            .*,
            is_top_level: (term:TopLevelTerm),
            label: version.label,
            author_id: user.id
        }
    """
    get_terms_db = """
        MATCH (term:Term {id: $id})
        RETURN term
    """
    is_archived = """
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term {id: $id})<-[:IS_VERSION_OF]-(termVersion:TermVersion)
        WHERE NOT (term)<-[:ARCHIVED]-(:User) OR NOT (termVersion)<-[:ARCHIVED]-(:User)
        WITH COUNT(term) + COUNT(termVersion) AS totalCount,
            COUNT(term) AS termCount,
            COUNT(termVersion) AS termVersionCount
        RETURN totalCount = 0 OR (termCount = 0 AND termVersionCount = 0) AS is_archived
    """
    delete_detach = """
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term {id: $id})<-[:ARCHIVED]-(:User)
        MATCH (term:Term)<-[:IS_VERSION_OF]-(termVersion:TermVersion)
        DETACH DELETE term, termVersion
    """
    all = """
        MATCH (term:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
        RETURN term {
            .*,
            is_top_level: (term:TopLevelTerm),
            label: version.label
        }
    """
    all_bank = """
        MATCH (term:Term)<-[:IS_PUBLISHED_VERSION_OF]-(version:TermVersion)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        RETURN term {
            .*,
            label: version.label
        }
    """
    all_bank_no_vocab = """
        MATCH (term:Term)<-[:IS_PUBLISHED_VERSION_OF]-(version:TermVersion)
        RETURN term, COLLECT(DISTINCT version.label) AS labels
    """
    all_top_level = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-
            (term:TopLevelTerm)<-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
        MATCH (type)-[belongs_to:BELONGS_TO]->(vocabulary)
        MATCH (version)-[of_type:IS_OF_TYPE]->(type:VocPopuliType)
        WITH term, version, belongs_to, of_type ORDER BY belongs_to.position_number ASC, of_type.position_number ASC
        RETURN term {
            .*,
            is_top_level: true,
            label: version.label
        }
    """
    get_versions = """
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term {id: $id})
            <-[:IS_VERSION_OF]-(version:TermVersion)
        RETURN version {
            .*,
            is_top_level: (term:TopLevelTerm),
            is_approved: exists((:User)-[:APPROVED]->(version)),
            id_t_global: version.id
        }
    """
    get_versions_with_authors = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term {id: $id})<-[:IS_VERSION_OF]-(version:TermVersion)
        <-[:AUTHORED]-(author:User)
        RETURN DISTINCT author, version {
            .*,
            is_top_level: (term:TopLevelTerm),
            is_approved: exists((:User)-[:APPROVED]->(version)),
            id_t_global: version.id
        } ORDER BY version.issue_version DESC
    """
    get_latest_children = """
        MATCH (term:Term {id: $id})<-[:IS_NARROWER_THAN {active: true}]-(child:TermVersion)
            -[:IS_VERSION_OF {latest_term_version: true}]->(:Term)
        RETURN child {
            .*,
            is_top_level: (term:TopLevelTerm)
        }
    """
    get_comments = """
        MATCH (:Term {id: $id})<-[:IS_VERSION_OF]-(version:TermVersion)<-[:ON_VERSION]-(comment:Comment)
        OPTIONAL MATCH (author:User)-[:AUTHORED]->(comment)
        RETURN author, comment {
            .*,
            version_id: version.id,
            issue_name: (
                CASE version.issue_version
                    WHEN 0 THEN "[Initial version] "
                    ELSE "[Version Update #" + version.issue_version + "] "
                END
                ) + version.label
        } ORDER BY comment.created_at DESC
    """
    unset_as_top_level_term = """
        MATCH (term:TopLevelTerm {id: $id})
            -[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        REMOVE term:TopLevelTerm
    """
    get_nodes_for_graph = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term)
            <-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
        OPTIONAL MATCH (version)-[:IS_OF_TYPE {active: true}]->(type:VocPopuliType)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO]->(related:Term)
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->(relationship:Relationship)
            -[:HAS_OBJECT]->(c_related:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (relationship)-[:IS_OF_TYPE]->(custom_relationship:RelationshipType)
        WITH term, version, type, collect(DISTINCT related.id) AS related_ids, 
             COLLECT(DISTINCT CASE WHEN c_related.id IS NOT NULL THEN {id: c_related.id, label: custom_relationship.label, relationship_id: relationship.id} ELSE {} END) AS related_list
        RETURN {
            id: term.id,
            term_version_id: version.id,
            label: version.label,
            custom_related: related_list,
            related: related_ids,
            vocpopuli_type: type.name
        } AS results
    """
    get_nodes_for_graph_toplevel = """
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:TopLevelTerm)
            <-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
        OPTIONAL MATCH (version)-[:IS_OF_TYPE {vocabulary_id: $vocabulary_id}]->(type:VocPopuliType)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO]->(related:Term)
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->(relationship:Relationship)
            -[:HAS_OBJECT]->(c_related:TopLevelTerm)
        OPTIONAL MATCH (relationship)-[:IS_OF_TYPE]->(custom_relationship:RelationshipType)
        WITH term, version, type, collect(DISTINCT related.id) AS related_ids, 
             COLLECT(DISTINCT CASE WHEN c_related.id IS NOT NULL THEN {id: c_related.id, label: custom_relationship.label, relationship_id: relationship.id} ELSE {} END) AS related_list
        RETURN {
            id: term.id,
            term_version_id: version.id,
            label: version.label,
            vocpopuli_type: type.name,
            vocpopuli_type_color: type.color,
            related: related_ids,
            custom_related: related_list
        } AS results
    """
    get_parents_and_vocpopuli_type_bulk = """
        UNWIND $term_ids AS term_id
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-
            (term:Term {id: term_id})<-[:IS_VERSION_OF {latest_term_version: true}]-
            (version:TermVersion)
        OPTIONAL MATCH (version)-[:IS_NARROWER_THAN {active: true}]->(parent:Term)
        OPTIONAL MATCH (version)-[r:IS_OF_TYPE {active: true}]->(type:VocPopuliType)
        WHERE r.vocabulary_id = $vocabulary_id
        RETURN 
            term.id AS id,
            collect(parent.id) AS parents,
            type.name AS vocpopuli_type
    """
    duplicate_term_version_bulk = """
        UNWIND $terms AS termData
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-
            (term:Term {id: termData.id})
            <-[old_latest_term_version_rel:IS_VERSION_OF {latest_term_version: true}]-
            (oldVersion:TermVersion),
            (vocabulary)<-[:HAS_ACCESS_TO]-(user:User {id: $user_id})

        // clone node with relationships
        CALL apoc.refactor.cloneNodes([oldVersion], true)
        YIELD output AS newVersion

        MATCH (newVersion)<-[deleteMe:AUTHORED]-(:User)
        DELETE deleteMe
        MERGE (newVersion)<-[:AUTHORED]-(user)

        REMOVE old_latest_term_version_rel.latest_term_version
        SET newVersion.id = termData.new_version_id,
            newVersion.created_at = datetime(),
            newVersion.issue_version = oldVersion.issue_version + 1
        RETURN newVersion
    """
    rearrange_children_bulk = """
        UNWIND $terms AS termData
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term {id: termData.id})
        UNWIND termData.children AS child
        MATCH (term)<-[r:IS_NARROWER_THAN]-(:TermVersion)
            -[:IS_VERSION_OF {latest_term_version: true}]->(:Term {id: child.id})
        SET r.position_number = child.position
        
        UNION
        
        UNWIND $terms AS termData
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(type:VocPopuliType {name: termData.id})
        UNWIND termData.children AS child
        MATCH (type)<-[r2:IS_OF_TYPE {vocabulary_id: $vocabulary_id, active: true}]-(:TermVersion)
            -[:IS_VERSION_OF {latest_term_version: true}]->(:Term {id: child.id})
        SET r2.position_number = child.position
    """


class TermVersion:
    set_broader = """
        MATCH (version:TermVersion {id: $id}),
            (broaderTerm:Term) WHERE broaderTerm.id in $broader
        OPTIONAL MATCH (version)-[existingRelationship:IS_NARROWER_THAN {active: true}]->(broaderTerm)
        OPTIONAL MATCH (version)-[removedRelationship:IS_NARROWER_THAN {active: true}]->(:Term)
            WHERE NOT (version)-[removedRelationship:IS_NARROWER_THAN {active: true}]->(broaderTerm)
        OPTIONAL MATCH (:TermVersion {id: $previous_id})-[previousRelationship:IS_NARROWER_THAN]->(broaderTerm)
        SET removedRelationship.inactive_at = COALESCE(removedRelationship.inactive_at, []) + [datetime()],
            removedRelationship.active = false
        WITH version, broaderTerm, existingRelationship, removedRelationship, previousRelationship
        WHERE NOT (version)-[:IS_NARROWER_THAN {active: true}]->(broaderTerm)
        MERGE (version)-[newRelationship:IS_NARROWER_THAN]->(broaderTerm)
        SET newRelationship.active_at = COALESCE(newRelationship.active_at, []) + [datetime()],
            newRelationship.inactive_at = COALESCE(newRelationship.inactive_at, []),
            newRelationship.active = true,
            newRelationship.position_number = previousRelationship.position_number
    """
    set_broader_bulk = """
        UNWIND $terms AS termData
        MATCH (term:Term {id: termData.id})<-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion),
            (broaderTerm:Term) WHERE broaderTerm.id in termData.parents
        OPTIONAL MATCH (version)-[existingRelationship:IS_NARROWER_THAN {active: true}]->(broaderTerm)
        OPTIONAL MATCH (version)-[removedRelationship:IS_NARROWER_THAN {active: true}]->(:Term)
            WHERE NOT (version)-[removedRelationship:IS_NARROWER_THAN {active: true}]->(broaderTerm)
        SET removedRelationship.inactive_at = COALESCE(removedRelationship.inactive_at, []) + [datetime()],
            removedRelationship.active = false
        WITH version, broaderTerm, existingRelationship, removedRelationship
        OPTIONAL MATCH (version)-[oldVPTypeRelationship:IS_OF_TYPE {active: true}]->(:VocPopuliType)
        SET oldVPTypeRelationship.inactive_at = COALESCE(oldVPTypeRelationship.inactive_at, []) + [datetime()],
            oldVPTypeRelationship.active = false
        WITH version, broaderTerm, existingRelationship, removedRelationship
        WHERE NOT (version)-[:IS_NARROWER_THAN {active: true}]->(broaderTerm)
        MERGE (version)-[newRelationship:IS_NARROWER_THAN]->(broaderTerm)
        SET newRelationship.active_at = COALESCE(newRelationship.active_at, []) + [datetime()],
            newRelationship.inactive_at = COALESCE(newRelationship.inactive_at, []),
            newRelationship.active = true
    """
    attach_broader_terms_db_ = """
        MATCH (version:TermVersion {id: $id})
        MATCH (broaderTermVersion:TermVersion) WHERE broaderTermVersion.id in $broader
        MERGE (version)-[:IS_NARROWER_THAN {vocabulary_id: $vocabulary_id}]->(broaderTermVersion)
    """
    attach_related_terms_db_ = """
        MATCH (version:TermVersion {id: $id})
        MATCH (relatedTermVersion:TermVersion) WHERE relatedTermVersion.id in $related
        MERGE (version)-[:IS_RELATED_TO {vocabulary_id: $vocabulary_id}]->(relatedTermVersion)
    """
    attach_related = """
        MATCH (version:TermVersion {id: $id})
        MATCH (relatedTerm:Term) WHERE relatedTerm.id in $related
        OPTIONAL MATCH (version)-[existingRelationship:IS_RELATED_TO {active: true}]->(relatedTerm)
        OPTIONAL MATCH (version)-[removedRelationship:IS_RELATED_TO {active: true}]->(:Term)
            WHERE NOT (version)-[removedRelationship:IS_RELATED_TO {active: true}]->(relatedTerm)
        SET removedRelationship.inactive_at = COALESCE(removedRelationship.inactive_at, []) + [datetime()],
            removedRelationship.active = false
        WITH version, relatedTerm, existingRelationship, removedRelationship
        WHERE NOT (version)-[:IS_RELATED_TO {active: true}]->(relatedTerm)
        MERGE (version)-[newRelationship:IS_RELATED_TO]->(relatedTerm)
        SET newRelationship.active_at = COALESCE(newRelationship.active_at, []) + [datetime()],
            newRelationship.inactive_at = COALESCE(newRelationship.inactive_at, []),
            newRelationship.active = true
    """
    attach_external_terms = """
        MATCH (version:TermVersion {id: $id})
        MATCH (relatedExternalTerm:ExternalTerm) WHERE relatedExternalTerm.url in $terms
        OPTIONAL MATCH (version)-[existingRelationship:IS_RELATED_TO {active: true}]->(relatedExternalTerm)
        OPTIONAL MATCH (version)-[removedRelationship:IS_RELATED_TO {active: true}]->(:ExternalTerm)
            WHERE NOT (version)-[removedRelationship:IS_RELATED_TO {active: true}]->(relatedExternalTerm)
        SET removedRelationship.inactive_at = COALESCE(removedRelationship.inactive_at, []) + [datetime()],
            removedRelationship.active = false
        WITH version, relatedExternalTerm, existingRelationship, removedRelationship
        WHERE NOT (version)-[:IS_RELATED_TO {active: true}]->(relatedExternalTerm)
        MERGE (version)-[newRelationship:IS_RELATED_TO]->(relatedExternalTerm)
        SET newRelationship.active_at = COALESCE(newRelationship.active_at, []) + [datetime()],
            newRelationship.inactive_at = COALESCE(newRelationship.inactive_at, []),
            newRelationship.active = true
    """
    create = """
        MATCH (previousVersion:TermVersion)
        -[prev_latest_term_version_rel:IS_VERSION_OF {latest_term_version: true}]->
            (term:Term {id: $term_id})-[:BELONGS_TO]->
            (vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (user:User {id: $user_id})
        MATCH (dataType:DataType {name: $datatype})-[:BELONGS_TO]->
            (vocabulary)
        REMOVE prev_latest_term_version_rel.latest_term_version

        CREATE (version:TermVersion {
            id: $id,
            issue_version: previousVersion.issue_version + 1,
            label: $label,
            definition: $definition,
            reference: $reference,
            synonyms: $synonyms,
            translations: $translations,
            unit: $unit,
            min_: $min_,
            max_: $max_,
            required: $required,
            generated_from: $generated_from,
            created_at: datetime()
        })
        MERGE (version)-[:IS_VERSION_OF {latest_term_version: true}]->(term)
        MERGE (user)-[:AUTHORED]->(version)
        MERGE (version)-[:IS_OF_TYPE {vocabulary_id: $vocabulary_id}]->(dataType)
        RETURN version {
            .*,
            term_id: term.id
        } AS term_version
    """
    import_ = """
        MATCH (term:Term {id: $term_id})-[:BELONGS_TO]->
            (vocabulary:Vocabulary)
        MATCH (author:User {id: $author_id})
        MATCH (dataType:DataType {name: $datatype})-[:BELONGS_TO]->
            (vocabulary)

        CREATE (version:TermVersion {
            id: $id,
            issue_version: $issue_version,
            label: $label,
            definition: $definition,
            synonyms: $synonyms,
            translations: $translations,
            unit: $unit,
            created_at: datetime($created_at),
            issue_iid: $issue_iid,
            generated_from: $generated_from,
            min_: $min_,
            max_: $max_,
            required: $required,
            purl: $purl,
            gitlab_skos_url: $gitlab_skos_url
        })
        MERGE (version)-[:IS_VERSION_OF]->(term)
        MERGE (author)-[:AUTHORED]->(version)
        MERGE (version)-[:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(dataType)
        RETURN version {
            .*,
            term_id: term.id
        } AS term_version
    """
    import_terms_db_ = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (term:Term {id: $term_id})
        MERGE (dataType:DataType {name: $datatype})

        MERGE (version:TermVersion {
            id: $id})
            ON CREATE 
                SET
                    version.label = $label,
                    version.definition = $definition,
                    version.synonyms = $synonyms,
                    version.translations = $translations,
                    version.unit = $unit,
                    version.created_at = datetime($created_at),
                    version.min_ = $min_,
                    version.max_ = $max_,
                    version.required = $required,
                    version.purl = $purl,
                    version.gitlab_skos_url = $gitlab_skos_url,
                    version.id_t_local = $id_t_local
        MERGE (version)-[:IS_PUBLISHED_VERSION_OF]->(term)
        MERGE (version)-[:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(dataType)
        MERGE (version)-[:BELONGS_TO]->(vocabulary)
        RETURN version {
            .*,
            term_id: term.id
        } AS term_version
    """
    set_id_t_local = """
        MATCH (version:TermVersion {id: $id})
        SET version.id_t_local = $id_t_local
    """
    get = """
        MATCH (term_version:TermVersion {id: $id})-[:IS_VERSION_OF]->(term:Term)
            -[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (term_version)<-[:AUTHORED]-(author:User)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE {vocabulary_id: $vocabulary_id}]->(dataType:DataType)
        OPTIONAL MATCH (term_version)-[r1:IS_OF_TYPE {active: true}]->(vocpopuliType:VocPopuliType)
        WHERE r1.vocabulary_id = $vocabulary_id
        OPTIONAL MATCH (term_version)-[:IS_NARROWER_THAN {active: true}]->(t1:Term)
            <-[:IS_VERSION_OF  {latest_term_version: true}]-(broader_version:TermVersion)
        WHERE (t1)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO {active: true}]->(t2:Term)
            <-[:IS_VERSION_OF  {latest_term_version: true}]-(related_version:TermVersion)
        WHERE (t2)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO {active: true}]->(external:ExternalTerm)
        WITH
            term_version,
            author,
            term,
            dataType,
            vocpopuliType,
            collect(broader_version.id) AS broader_ids,
            collect(broader_version.label) AS broader_labels,
            collect(related_version.id) AS related_ids,
            collect(related_version.label) AS related_labels,
            collect(external.url) AS external_urls
        RETURN term_version {
            .*,
            is_approved: exists((term_version)<-[:APPROVED]-(:User)),
            is_latest: exists((term_version)-[:IS_VERSION_OF {latest_term_version: true}]->(:Term)),
            author_id: author.id,
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            broader: broader_ids,
            broader_labels: broader_labels,
            related: related_ids,
            related_labels: related_labels,
            related_external_terms: external_urls
        }
    """
    get_approval_status = """
        MATCH (term_version:TermVersion {id: $id})-[:IS_VERSION_OF]
            ->(term:Term)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (term)<-[:IS_VERSION_OF]-(approved_version:TermVersion)
            <-[:APPROVED]-(:User)
        WITH
            term_version,
            max(approved_version.created_at) AS last_approval,
            exists((term_version)-[:IS_VERSION_OF {latest_term_version: true}]->(term)) AS is_latest
        RETURN CASE
            WHEN
                last_approval IS NULL
                OR term_version.created_at > last_approval
            THEN "not approved"
            WHEN
                term_version.created_at = last_approval
                AND is_latest
            THEN "approved"
            ELSE "deprecated"
        END AS approval_status
    """
    get_terms_db = """
        MATCH (term_version:TermVersion {id: $id})-[:IS_PUBLISHED_VERSION_OF]->(term:Term)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(dataType:DataType)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(vocpopuliType:VocPopuliType)
        WITH
            term_version,
            term,
            dataType,
            vocpopuliType
        RETURN term_version {
            .*,
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name
        }
    """
    get_term_branch_versions_db = """
        MATCH (term_version:TermVersion)-[:IS_PUBLISHED_VERSION_OF]->(term:Term)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(dataType:DataType)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(vocpopuliType:VocPopuliType)
        OPTIONAL MATCH (narrower_term:Term)<-[:IS_PUBLISHED_VERSION_OF]-(narrower_version:TermVersion)-[:IS_NARROWER_THAN]->(term_version)
        OPTIONAL MATCH (term_version:TermVersion)-[:IS_NARROWER_THAN]->(broader_version:TermVersion)-[:IS_PUBLISHED_VERSION_OF]->(broader_term:Term)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO]->(external:ExternalTerm)
        WITH
            term_version,
            term,
            dataType,
            vocpopuliType,
            collect(narrower_term.id) AS narrower_ids,
            collect(narrower_version.label) AS narrower_labels,
            collect(broader_term.id) AS broader_ids,
            collect(broader_version.label) AS broader_labels,
            collect(external.url) AS external_urls
        RETURN term_version {
            .*,
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            narrower: narrower_ids,
            narrower_labels: narrower_labels,
            broader: broader_ids,
            broader_labels: broader_labels,
            related_external_terms: external_urls
        }
    """
    get_all_terms_db = """
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term_version:TermVersion)-[:IS_PUBLISHED_VERSION_OF]->(term:Term)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(dataType:DataType)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(vocpopuliType:VocPopuliType)
        OPTIONAL MATCH (term_version)-[:IS_NARROWER_THAN {vocabulary_id: $vocabulary_id}]->(broader_version:TermVersion)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO {vocabulary_id: $vocabulary_id}]->(related_version:TermVersion)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO]->(external:ExternalTerm)
        WITH
            term_version,
            term,
            dataType,
            vocpopuliType,
            collect(broader_version.id) AS broader_ids,
            collect(broader_version.label) AS broader_labels,
            collect(related_version.id) AS related_ids,
            collect(related_version.label) AS related_labels,
            collect(external.url) AS external_urls
        RETURN term_version {
            .*,
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            broader: broader_ids,
            broader_labels: broader_labels,
            related: related_ids,
            related_labels: related_labels,
            related_external_terms: external_urls
        }
    """
    get_all_terms_db_no_vocab = """
        MATCH (vocabulary:Vocabulary)<-[:BELONGS_TO]-(term_version:TermVersion)-[:IS_PUBLISHED_VERSION_OF]->(term:Term)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(dataType:DataType)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(vocpopuliType:VocPopuliType)
        OPTIONAL MATCH (term_version)-[:IS_NARROWER_THAN]->(broader_version:TermVersion)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO]->(related_version:TermVersion)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO]->(external:ExternalTerm)
        WITH
            term_version,
            term,
            dataType,
            vocpopuliType,
            vocabulary,
            collect(broader_version.id) AS broader_ids,
            collect(broader_version.label) AS broader_labels,
            collect(related_version.id) AS related_ids,
            collect(related_version.label) AS related_labels,
            collect(external.url) AS external_urls
        RETURN term_version {
            .*,
            vocabulary_name: vocabulary.name,
            vocabulary_id: vocabulary.id,
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            broader: broader_ids,
            broader_labels: broader_labels,
            related: related_ids,
            related_labels: related_labels,
            related_external_terms: external_urls
        }
    """
    all_latest_versions = """
        MATCH (version:TermVersion)-[:IS_VERSION_OF  {latest_term_version: true}]->
            (term:Term)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        WHERE NOT (:User)-[:ARCHIVED]->(version)
        OPTIONAL MATCH (version)-[:IS_OF_TYPE {vocabulary_id: $vocabulary_id}]->(dataType:DataType)
        OPTIONAL MATCH (version)-[r1:IS_OF_TYPE {active: true}]->(vocpopuliType:VocPopuliType)
            WHERE r1.vocabulary_id = $vocabulary_id
        OPTIONAL MATCH (version)-[:IS_NARROWER_THAN {active: true}]->
            (broader:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (vocabulary)<-[:BELONGS_TO]-(narrower:Term)<-[:IS_VERSION_OF  {latest_term_version: true}]-
            (:TermVersion)-[:IS_NARROWER_THAN {active: true}]->(term)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO {active: true}]->(related:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO {active: true}]->(external:ExternalTerm)
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->(relationship:Relationship {active: true})
            -[:IS_OF_TYPE]->(relationshipType:RelationshipType)-[:BELONGS_TO]->(vocabulary)
        RETURN version {
            .*,
            is_approved: exists((version)<-[:APPROVED]-(:User)),
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            related: related.id,
            related_external: external.url
        },
            collect(broader.id) AS broader,
            collect(distinct narrower.id) AS narrower
    """
    all_latest_versions_with_custom_relationships = """
        MATCH (version:TermVersion)-[:IS_VERSION_OF {latest_term_version: true}]->
            (term:Term)-[:BELONGS_TO]-(:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->
            (relationship:Relationship {active: true})-[:HAS_OBJECT]->(target:Term),
            (relationship)-[:IS_OF_TYPE]->(relationshipType:RelationshipType)
        WITH version, term, relationship {type_: relationshipType.id_t_global},
            collect(distinct target.id) AS relationshipTargets
        WITH version, term, relationship {.*, related: relationshipTargets} AS relationships
        OPTIONAL MATCH (version)-[:IS_OF_TYPE {vocabulary_id: $vocabulary_id}]->(dataType:DataType)
        OPTIONAL MATCH (version)-[r1:IS_OF_TYPE {active: true}]->(vocpopuliType:VocPopuliType)-[r0:BELONGS_TO]-(:Vocabulary {id: $vocabulary_id})
            WHERE r1.vocabulary_id = $vocabulary_id
        OPTIONAL MATCH (version)-[r2:IS_NARROWER_THAN {active: true}]->(broader:Term)
        OPTIONAL MATCH (narrower:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-
            (:TermVersion)-[r3:IS_NARROWER_THAN {active: true}]->(term)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO {active: true}]->(related:Term)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO {active: true}]->(external:ExternalTerm)
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->(relationship:Relationship {active: true})
            -[:IS_OF_TYPE]->(relationshipType:RelationshipType)
        WITH term, version, dataType, vocpopuliType, broader,
            narrower, relationships, external, related, r3, r2, r1, r0 ORDER BY r3.position_number ASC, narrower.created_at ASC
        RETURN version {
            .*,
            is_approved: exists((version)<-[:APPROVED]-(:User)),
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            related: related.id,
            related_external: external.url
        },
            collect(broader.id) AS broader,
            collect(distinct narrower.id) AS narrower,
            collect(distinct relationships) AS relationships
    """
    get_by_issue_iid = """
        MATCH (term_version:TermVersion {issue_iid: $issue_iid})-[:IS_VERSION_OF]->(term:Term)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE]->(dataType:DataType)
        OPTIONAL MATCH (term_version)-[:IS_OF_TYPE {active: true}]->(vocpopuliType:VocPopuliType)
        OPTIONAL MATCH (term_version)-[:IS_RELATED_TO {active: true}]->(external:ExternalTerm)
        RETURN term_version {
            .*,
            is_approved: exists((term_version)<-[:APPROVED]-(:User)),
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name,
            related_external_terms: external.url
        }
    """
    get_broader = """
        MATCH (term_version:TermVersion {id: $id})-[:IS_NARROWER_THAN {active: true}]->(broader:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(broaderVersion:TermVersion)
        RETURN broader {
            .*,
            label: broaderVersion.label
        }
    """
    get_related = """
        MATCH (term_version:TermVersion {id: $id})-[:IS_RELATED_TO {active: true}]->(related:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(relatedVersion:TermVersion)
        RETURN related {
            .*,
            label: relatedVersion.label
        }
    """
    get_files = """
        MATCH (files:File)-[r:IS_ATTACHED_TO]->(v:TermVersion {id: $id})
        RETURN files {.*, order_id: r.order_id} ORDER BY r.order_id
    """
    get_image_ids = """
        MATCH (file:File)-[r:IS_ATTACHED_TO]->(:TermVersion {id: $id})
        WITH file ORDER BY r.order_id ASC
        RETURN collect(file.id) AS image_ids
    """
    get_image_urls = """
        MATCH (file:File)-[r:IS_ATTACHED_TO]->(:TermVersion {id: $id})
        WITH file ORDER BY r.order_id ASC
        RETURN collect(file.url) AS image_urls
    """
    get_votes = """
        MATCH (version:TermVersion {id: $id})
        OPTIONAL MATCH (version)<-[up:THUMBED_UP]-(:User)
        OPTIONAL MATCH (version)<-[down:THUMBED_DOWN]-(:User)
        RETURN count(up) AS thumbs_up, count(down) AS thumbs_down
    """
    get_user_vote = """
        MATCH (version:TermVersion {id: $id})
        MATCH (user:User {id: $user_id})
        MATCH (user)-[r:THUMBED_UP|THUMBED_DOWN]->(version)
        RETURN type(r) AS vote
    """
    update_issue_iid = """
        MATCH (term_version:TermVersion {id: $id})
        SET term_version.issue_iid = $issue_iid
        RETURN term_version
    """
    keep_images = """
        MATCH (new_version:TermVersion)-[:IS_VERSION_OF {latest_term_version: true}]->(term:Term)
        MATCH (previous_version:TermVersion)-[:IS_VERSION_OF]->(term)
            WHERE previous_version.issue_version = new_version.issue_version - 1
        MATCH (file:File)-[r:IS_ATTACHED_TO]->(previous_version)
        MERGE (file)-[:IS_ATTACHED_TO {order_id: r.order_id}]->(new_version)
        RETURN count(file) AS file_count
    """
    set_latest = """
        MATCH (version:TermVersion {id: $id})-[rel:IS_VERSION_OF]->(:Term)
        -[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        SET rel.latest_term_version = true
    """
    set_vocpopuli_type = """
        MATCH (version:TermVersion {id: $id})-[:IS_VERSION_OF]->
            (:Term)-[:BELONGS_TO]->
            (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-
            (type:VocPopuliType {name: $vocpopuli_type})
        OPTIONAL MATCH (version)-[oldRelationship:IS_OF_TYPE {active: true}]->(:VocPopuliType)
        OPTIONAL MATCH (:TermVersion {id: $previous_id})-[previousRelationship:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(type)
        SET oldRelationship.inactive_at = COALESCE(oldRelationship.inactive_at, []) + [datetime()],
            oldRelationship.active = false
        MERGE (version)-[newRelationship:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(type)
        SET newRelationship.active_at = COALESCE(newRelationship.active_at, []) + [datetime()],
            newRelationship.active = true,
            newRelationship.position_number = previousRelationship.position_number
    """
    set_vocpopuli_type_bulk = """
        UNWIND $terms AS termData
        MATCH (version:TermVersion)-[:IS_VERSION_OF {latest_term_version: true}]->
            (term:Term {id: termData.id})-[:BELONGS_TO]->
            (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-
            (type:VocPopuliType {name: termData.vocpopuli_type})
        OPTIONAL MATCH (version)-[oldRelationship:IS_OF_TYPE {active: true, vocabulary_id: vocabulary.id}]->(:VocPopuliType)
        OPTIONAL MATCH (version)-[narrower:IS_NARROWER_THAN]->(:Term)
        SET oldRelationship.inactive_at = COALESCE(oldRelationship.inactive_at, []) + [datetime()],
            oldRelationship.active = false
        MERGE (version)-[newRelationship:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(type)
        SET newRelationship.active_at = COALESCE(newRelationship.active_at, []) + [datetime()],
            newRelationship.active = true
        SET term:TopLevelTerm
        DELETE narrower
    """
    set_vocpopuli_type_terms_db_ = """
        MATCH (version:TermVersion {id: $id})
        MERGE (type:VocPopuliType {name: $vocpopuli_type})
        MERGE (version)-[:IS_OF_TYPE]->(type)
    """
    deactivate_old_relationships = """
        MATCH (version:TermVersion {id: $id})-[:IS_VERSION_OF]
            ->(:Term)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->(oldRelationship:Relationship {active: true})
        SET oldRelationship.active = false,
            oldRelationship.inactive_at = datetime()
    """
    add_custom_relationship = """
        MATCH (version:TermVersion {id: $id})-[:IS_VERSION_OF]
            ->(:Term)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        MATCH (type:RelationshipType {id_t_global: $relationship_type})

        CREATE (relationship:Relationship {id: $relationship_id, active: true, active_at: datetime()})
        MERGE (version)-[:IS_SUBJECT_OF]->(relationship)
            -[:IS_OF_TYPE]->(type)
    """
    add_custom_relationship_objects = """
        MATCH (relationship:Relationship {id: $relationship_id})
        MATCH (related:Term)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
            WHERE related.id in $related_terms
        MERGE (relationship)-[:HAS_OBJECT]->(related)
    """
    get_custom_relationships = """
        MATCH (version:TermVersion {id: $id})-[:IS_SUBJECT_OF]->(relationship:Relationship {active: true})
            -[:IS_OF_TYPE]->(type:RelationshipType)-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        WITH relationship, type.id_t_global AS type_
        ORDER BY relationship.active_at
        MATCH (relationship)-[:HAS_OBJECT]->(related_term:Term)
        RETURN type_, collect(related_term.id) as related
    """
    get_custom_relationships_full = """
        MATCH (version:TermVersion {id: $id})-[:IS_SUBJECT_OF]->(relationship:Relationship {active: true})
            -[:IS_OF_TYPE]->(type_:RelationshipType)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        WITH relationship, type_, vocabulary
        ORDER BY relationship.active_at
        MATCH (relationship)-[:HAS_OBJECT]->(related_term:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(latest:TermVersion)
        WHERE (related_term)-[:BELONGS_TO]->(vocabulary)
        RETURN type_, collect(related_term {
            .*,
            latest_term_version_id: latest.id,
            label: latest.label
        }) as related
    """
    restore_term = """
    MATCH (version:TermVersion {id: $id})-[:IS_VERSION_OF]->(term:Term)
    MATCH (all_versions:TermVersion)-[:IS_VERSION_OF]->(term)
    MATCH (:User)-[archived_term:ARCHIVED]->(term)
    MATCH (:User)-[archived_versions:ARCHIVED]->(all_versions)
    DELETE archived_term, archived_versions
    RETURN all_versions
    """


class User:
    create = """
        MERGE (user:User {id: $id})
        SET user.name = $name,
            user.username = $username,
            user.email = $email,
            user.web_url = $web_url,
            user.avatar_url = $avatar_url
        RETURN user
    """
    get = """
        MATCH (user:User {id: $id})
        RETURN user
    """
    approve = """
        MATCH (user:User {id: $id})
        MATCH (termVersion:TermVersion {id: $term_version_id})
        CREATE (user)-[:APPROVED {created_at: datetime()}]->(termVersion)
        SET termVersion.id_t_local = $id_t_local
        RETURN user
    """
    set_as_archived = """
        MATCH (user:User {id: $id})
        MATCH (:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-(term:Term {id: $term_id})<-[:IS_VERSION_OF]-(termVersions:TermVersion)
        MERGE (user)-[archived:ARCHIVED {created_at: datetime()}]->(termVersions)
        ON CREATE SET archived.created_at = datetime()
        MERGE (user)-[arhivedTerm:ARCHIVED {created_at: datetime()}]->(term)
        ON CREATE SET arhivedTerm.created_at = datetime()
        RETURN DISTINCT user
    """
    remove_vote = """
        MATCH (user:User {id: $id})
        MATCH (version:TermVersion {id: $term_version_id})
        MATCH (user)-[r:THUMBED_UP|THUMBED_DOWN]->(version)
        DELETE r
    """
    thumb_up = """
        MATCH (user:User {id: $id})
        MATCH (version:TermVersion {id: $term_version_id})
        MERGE (user)-[:THUMBED_UP]->(version)
    """
    thumb_down = """
        MATCH (user:User {id: $id})
        MATCH (version:TermVersion {id: $term_version_id})
        MERGE (user)-[:THUMBED_DOWN]->(version)
    """
    comment_on_term_version = """
        MATCH (user:User {id: $id})
        MATCH (version:TermVersion {id: $term_version_id})
        CREATE (comment:Comment)
        SET comment.id = $comment_id,
            comment.body = $body,
            comment.created_at = datetime()
        MERGE (user)-[:AUTHORED]->(comment)-[:ON_VERSION]->(version)
        RETURN comment
    """
    get_vocabularies = """
        MATCH (:User {id: $id})-[:HAS_ACCESS_TO]->(vocabulary:Vocabulary)
        RETURN vocabulary
    """
    set_orcid_id = """
        MATCH (user:User {id: $id})
        SET user.orcid_id = $orcid_id
    """
    get_vocabulary = """
    MATCH (:User {id: $id})-[:HAS_ACCESS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
    RETURN vocabulary {
        .*,
        description: vocabulary.description,
        domain: vocabulary.domain,
        license: vocabulary.license,
        license_url: vocabulary.license_url,
        institution: vocabulary.institution,
        institution_ror: vocabulary.institution_ror
        }
    """
    get_terms_stats = """
        MATCH (:User {id: $id})-[:HAS_ACCESS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (term:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (tlTerm:TopLevelTerm)-[:BELONGS_TO]->(vocabulary)
        WITH COUNT(DISTINCT term) as n_terms,
            COUNT(DISTINCT tlTerm) as n_top_level_terms
        RETURN {
            n_terms: n_terms,
            n_top_level_terms: n_top_level_terms
            } as terms_stats
    """
    get_relationship_stats = """
        MATCH (:User {id: $id})-[:HAS_ACCESS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (cr:RelationshipType)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(:TermVersion)-[nt:IS_NARROWER_THAN]->(:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(:TermVersion)-[rt:IS_RELATED_TO]->(:Term)-[:BELONGS_TO]->(vocabulary)
        WITH COUNT(DISTINCT cr) as n_custom_rels,
            COUNT(DISTINCT nt) as n_nt_rels,
            COUNT(DISTINCT rt) as n_related_rels
        RETURN {
            n_custom_rels: n_custom_rels,
            n_nt_rels: n_nt_rels,
            n_related_rels: n_related_rels
            } as relationship_stats
    """
    get_additional_stats = """
        MATCH (:User {id: $id})-[:HAS_ACCESS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (vp:VocPopuliType)-[:BELONGS_TO]->(vocabulary)
        WITH COUNT(DISTINCT vp) as n_vp_types
        RETURN {
            n_vp_types: n_vp_types
        } as additional_stats
    """


class Vocabulary:
    get = """
        MATCH (vocabulary:Vocabulary {id: $id})
        RETURN vocabulary
    """
    get_latest_id_v_local = """
        MATCH (vocabulary:Vocabulary {id: $id})<-[:BELONGS_TO]-(idvlocalnode:LocalVocabularyID)
        RETURN idvlocalnode.id_v_local as id_v_local
        ORDER BY idvlocalnode.created_at DESC
        LIMIT 1
    """
    get_all_from_bank = """
        MATCH (vocabulary:Vocabulary)
        WITH vocabulary.id as id, vocabulary.domain as domain, vocabulary.name as name
        RETURN id, domain, name
    """
    get_from_bank = """
        MATCH (vocabulary:Vocabulary {id: $id})
        RETURN vocabulary
    """
    all = """
        MATCH (vocabulary:Vocabulary)
        RETURN vocabulary
    """
    all_terms = """
        MATCH (:Vocabulary {id: $id})<-[:BELONGS_TO]-
            (term:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
            -[:IS_OF_TYPE]->(type:DataType)
        RETURN term {
            .*,
            is_top_level: (term:TopLevelTerm),
            label: version.label,
            datatype: type.name 
        }
    """
    all_terms_terms_db = """
        MATCH (term:Term)-[:IS_PUBLISHED_VERSION_OF]-(version:TermVersion)
        RETURN term {
            .*,
            label: version.label
        }
    """
    all_terms_terms_db = """
        MATCH (term:Term)-[:IS_PUBLISHED_VERSION_OF]-(version:TermVersion)
        RETURN term {
            .*,
            label: version.label
        }
    """
    create = """
        MATCH (user:User {id: $user_id})
        MERGE (vocabulary:Vocabulary {id: $id})
        MERGE (user)-[:HAS_ACCESS_TO]->(vocabulary)
        SET vocabulary.name = $name,
            vocabulary.updated_at = datetime(),
            vocabulary.description = $description,
            vocabulary.domain = $domain,
            vocabulary.institution = $institution,
            vocabulary.institution_ror = $institution_ror,
            vocabulary.license_name = $license_name,
            vocabulary.license_url = $license_url
        RETURN vocabulary
    """
    create_id_v_local = """
    MATCH (vocabulary:Vocabulary {id: $id})
    MERGE (:LocalVocabularyID {id_v_local: $id_v_local, created_at: datetime()})-[:BELONGS_TO]->(vocabulary)
    """
    remove_terms_db_ = """
        MATCH (v:Vocabulary {id: $id})
        DETACH DELETE v 
    """
    import_terms_db_ = """
        MERGE (vocabulary:Vocabulary {id: $id})
        SET vocabulary.name = $name,
            vocabulary.description = $description,
            vocabulary.domain = $domain,
            vocabulary.institution = $institution,
            vocabulary.institution_ror = $institution_ror,
            vocabulary.license_name = $license_name,
            vocabulary.license_url = $license_url,
            vocabulary.updated_at = datetime()
        RETURN vocabulary
    """
    set_updated_at = """
        MATCH (vocabulary:Vocabulary {id: $id})
        SET vocabulary.updated_at = datetime()
        RETURN vocabulary.updated_at AS timestamp
    """
    get_cached_tree = """
        MATCH (vocabulary:Vocabulary {id: $id})
        RETURN vocabulary.cached_tree AS cached_tree
    """
    remove_cached_tree = """
        MATCH (vocabulary:Vocabulary {id: $id})
        REMOVE vocabulary.cached_tree
    """
    set_cached_tree = """
        MATCH (vocabulary:Vocabulary {id: $id})
        SET vocabulary.cached_tree = $tree
    """
    get_relationship_types = """
        MATCH (:Vocabulary {id: $id})<-[:BELONGS_TO]-(type:RelationshipType)
        RETURN type
    """
    get_relationship_types_order_and_instances = """
        MATCH (:Vocabulary {id: $id})<-[belongs_to:BELONGS_TO]-(type:RelationshipType)
        OPTIONAL MATCH (type)<-[:IS_OF_TYPE]-(r:Relationship)<-[:IS_SUBJECT_OF]-(:TermVersion)-[:IS_VERSION_OF]->(:Term)-[:BELONGS_TO]->(vocabulary)
        WITH COUNT(r) AS instances, belongs_to AS belongs_to, type AS type
        RETURN type {
            .*,
            instances: instances,
            position: belongs_to.position_number
        } AS type
        ORDER BY type.position
    """
    get_archived_term_tree = """
        MATCH (user:User)-[:ARCHIVED]->(version:TermVersion)-[:IS_VERSION_OF {latest_term_version: true}]->
            (term:Term)-[:BELONGS_TO]-(vocabulary:Vocabulary {id: $id})
        OPTIONAL MATCH (version)-[:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(dataType:DataType)
        OPTIONAL MATCH (version)-[r1:IS_OF_TYPE {active: true}]->(vocpopuliType:VocPopuliType)
            WHERE r1.vocabulary_id = vocabulary.id
        OPTIONAL MATCH (version)-[relationship:IS_NARROWER_THAN {active: true}]->(broader:Term)
        OPTIONAL MATCH (narrower:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-
            (:TermVersion)-[:IS_NARROWER_THAN {active: true}]->(term)
        WITH term, version, dataType, vocpopuliType, broader,
            narrower ORDER BY relationship.position_number
        WITH version {
            label: version.label,
            id: version.id,
            term_id: term.id,
            is_top_level: (term:TopLevelTerm),
            is_approved: exists((version)<-[:APPROVED]-(:User)),
            datatype: dataType.name,
            vocpopuli_type: 'archived'
        },
            collect(broader.id) AS broader,
            collect(distinct narrower.id) AS narrower
        RETURN version {
            .*,
            broader: broader,
            narrower: narrower
        }
    """
    get_term_tree = """
        MATCH (version:TermVersion)-[:IS_VERSION_OF {latest_term_version: true}]->
            (term:Term)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $id})
        WHERE NOT (:User)-[:ARCHIVED]->(version)
        OPTIONAL MATCH (version)-[:IS_OF_TYPE {vocabulary_id: vocabulary.id}]->(dataType:DataType)
        OPTIONAL MATCH (version)-[r1:IS_OF_TYPE {active: true}]->(vocpopuliType:VocPopuliType)
            WHERE r1.vocabulary_id = vocabulary.id
        OPTIONAL MATCH (version)-[:IS_NARROWER_THAN {active: true}]->(broader:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (version)-[:IS_RELATED_TO {active: true}]->(related:Term)-[:BELONGS_TO]->(vocabulary)
        OPTIONAL MATCH (version)-[:IS_SUBJECT_OF]->(customRelationship:Relationship {active: true})
        OPTIONAL MATCH (vocabulary)<-[:BELONGS_TO]-(narrower:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-
            (:TermVersion)-[relationship:IS_NARROWER_THAN {active: true}]->(term)
        WITH term, version, dataType, vocpopuliType, broader,
            narrower, customRelationship, related ORDER BY relationship.position_number ASC, narrower.created_at ASC
        WITH version {
            .*,
            term_id: term.id,
            is_top_level: (term:TopLevelTerm),
            is_approved: exists((version)<-[:APPROVED]-(:User)),
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name
        },
            collect(broader.id) AS broader,
            collect(DISTINCT narrower.id) AS narrower,
            collect(DISTINCT customRelationship.id) as custom_relationships,
            collect(DISTINCT related.id) AS related
        RETURN version {
            .*,
            broader: broader,
            narrower: narrower,
            related: related,
            custom_relationships: custom_relationships
        }
    """
    top_level_terms = """
    MATCH (:Vocabulary {id: $id})<-[:BELONGS_TO]-
            (:Term)<-[:IS_VERSION_OF {latest_term_version: true}]-
            (version:TermVersion)-[:IS_OF_TYPE]->(datatype:DataType)
    WHERE NOT (version)-[:IS_NARROWER_THAN]->(:Term)
    WITH version, datatype.name as datatype
    RETURN version {
            .*,
            datatype: datatype
        }
    """
    get_term_tree_from_bank = """
        MATCH (version:TermVersion)-[:BELONGS_TO]->(:Vocabulary {id: $id})
        MATCH (version)-[:IS_PUBLISHED_VERSION_OF]->(term:Term)
        OPTIONAL MATCH (version)-[:IS_OF_TYPE]->(dataType:DataType)
        OPTIONAL MATCH (version)-[:IS_OF_TYPE]->(vocpopuliType:VocPopuliType)
        OPTIONAL MATCH (version)-[:IS_NARROWER_THAN {vocabulary_id: $id}]->(:TermVersion)-[:IS_PUBLISHED_VERSION_OF]->(broader:Term)
        OPTIONAL MATCH (narrower:Term)<-[:IS_PUBLISHED_VERSION_OF]-
            (:TermVersion)-[:IS_NARROWER_THAN {vocabulary_id: $id}]->(version)
        RETURN version {
            label: version.label,
            id: version.id,
            term_id: term.id,
            datatype: dataType.name,
            vocpopuli_type: vocpopuliType.name
        },
            collect(broader.id) AS broader,
            collect(distinct narrower.id) AS narrower
    """
    top_level_ids = """
        MATCH (:Vocabulary {id: $id})<-[:BELONGS_TO]-
            (term:TopLevelTerm)<-[:IS_VERSION_OF {latest_term_version: true}]-(version:TermVersion)
            -[relationship:IS_OF_TYPE {active: true}]->(vp_type:VocPopuliType)
        WHERE NOT (:User)-[:ARCHIVED]->(version)
        WITH vp_type, term ORDER BY relationship.position_number
        RETURN
            vp_type.name AS type_name,
            collect(term.id) AS term_ids
    """
    top_level_ids_from_bank = """
        MATCH (:Vocabulary {id: $id})<-[:BELONGS_TO]-(version:TermVersion)-[:IS_OF_TYPE]->(vp_type:VocPopuliType)
        MATCH (term:Term)<-[:IS_PUBLISHED_VERSION_OF]-(version)
        RETURN
            vp_type,
            collect(term.id) AS term_ids
    """
    top_level_ids_archived = """
        MATCH (:Vocabulary {id: $id})<-[:BELONGS_TO]-
            (term:TopLevelTerm)<-[:IS_VERSION_OF {latest_term_version: true}]-(ver:TermVersion)<-[:ARCHIVED]-(:User)
        RETURN collect(term.id) AS term_ids
    """
    update_metadata = """
        MATCH (vocabulary:Vocabulary {id: $id})
        SET vocabulary.name = $name,
            vocabulary.description = $description,
            vocabulary.domain = $domain,
            vocabulary.institution = $institution,
            vocabulary.institution_ror = $institution_ror,
            vocabulary.license_name = $license_name,
            vocabulary.license_url = $license_url
    """

    get_custom_relationships_from_bank = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})<-[:BELONGS_TO]-
        (subject_version:TermVersion)-[:IS_SUBJECT_OF]->(relationship:Relationship)

        MATCH (relationship_type:RelationshipType)<-[:IS_OF_TYPE]-
        (relationship)-[:HAS_OBJECT]->
        (object_version:TermVersion)-[:BELONGS_TO]->(vocabulary)

        MATCH (object_term:Term)<-[:IS_PUBLISHED_VERSION_OF]-(object_version)

        WITH relationship_type,
            collect(relationship.id) as relationships,
            collect(subject_version.id) as subject_version_ids,
            collect(object_term.id) as object_term_ids

        RETURN relationship_type{
            .*,
            relationships: relationships,
            subject_version_ids: subject_version_ids,
            object_term_ids: object_term_ids
        }
    """


class VocPopuliType:
    all = """
        MATCH (type:VocPopuliType)-[relationship:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        RETURN type
        ORDER BY relationship.position_number
    """
    all_bank = """
        MATCH (version:TermVersion)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (type:VocPopuliType)<-[:IS_OF_TYPE]-(version)
        RETURN DISTINCT type
    """
    all_with_instances = """
        MATCH (type:VocPopuliType)-[relationship:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        OPTIONAL MATCH (type)<-[:IS_OF_TYPE]-(tv:TermVersion)-[:IS_VERSION_OF]->(:Term)-[:BELONGS_TO]->(vocabulary)
        WITH COUNT(tv) AS instances, relationship AS relationship, type AS type
        RETURN type {
            .*,
            instances: instances
        }
        ORDER BY
            type.name = "detached",  // put "detached" last
            relationship.position_number
    """
    names = """
        MATCH (type:VocPopuliType)-[:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        RETURN DISTINCT type.name AS type
    """
    create = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        CREATE (type:VocPopuliType {id: $id, name: $name})
        CREATE (type)-[:BELONGS_TO]->(vocabulary)
        RETURN type
    """
    import_terms_db_ = """
        MERGE (type:VocPopuliType {name: $name, color: $color})
        RETURN type
    """
    create_default_types = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MERGE (process:VocPopuliType {name: "process"})
        MERGE (object:VocPopuliType {name: "object"})
        MERGE (data:VocPopuliType {name: "data"})
        MERGE (detached:VocPopuliType {name: "detached"})
        
        MERGE (process)-[:BELONGS_TO]->(vocabulary)
        MERGE (object)-[:BELONGS_TO]->(vocabulary)
        MERGE (data)-[:BELONGS_TO]->(vocabulary)
        MERGE (detached)-[:BELONGS_TO]->(vocabulary)
    """
    merge = """
        MATCH (vocabulary:Vocabulary {id: $vocabulary_id})
        MERGE (type:VocPopuliType {name: $name})
            ON CREATE
                SET type.id = apoc.create.uuid()
        MERGE (type)-[:BELONGS_TO]->(vocabulary)
        RETURN type
    """
    set_new_order = """
        UNWIND $new_order AS type
        MATCH (:VocPopuliType {name: type.name})-[relationship:BELONGS_TO]->
            (:Vocabulary {id: $vocabulary_id})
        SET relationship.position_number = type.position
    """
    delete = """
        MATCH (type:VocPopuliType {name: $name})-[b:BELONGS_TO]->(vocabulary:Vocabulary {id: $vocabulary_id})
        MATCH (type)-[:BELONGS_TO]->(v:Vocabulary)
        WITH type, b, COUNT(v) AS vocabularies
            DELETE b
        WITH type, b, vocabularies
        WHERE vocabularies = 1
            DETACH DELETE type
    """
    edit = """
        MATCH (type:VocPopuliType {name: $old_name})-[:BELONGS_TO]->(:Vocabulary {id: $vocabulary_id})
        SET type.name = $name
    """
    all_types_in_all_vocabularies = """
        MATCH (type:VocPopuliType)
        RETURN type
    """
