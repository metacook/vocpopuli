from . import read, write, read_terms_db, write_terms_db
from ..utils.vocpopuli_types import add_color_vocpopuli_types, determine_color_vocpopuli_type
from ._queries import VocPopuliType as Q
from ..models.vocpopuli_type import VocPopuliType
from uuid import uuid4


@read
def all(tx, vocabulary_id: str) -> list[VocPopuliType]:
    types = tx.run(Q.all, vocabulary_id=vocabulary_id).value("type")
    reordered = [dict(x) for x in types if x["name"] != "detached"]
    reordered.append([dict(x) for x in types if x["name"] == "detached"][0])

    return add_color_vocpopuli_types(reordered)


@read
def all_with_instances(tx, vocabulary_id: str) -> list[VocPopuliType]:
    types = tx.run(Q.all_with_instances, vocabulary_id=vocabulary_id).value("type")
    return  add_color_vocpopuli_types([dict(type_) for type_ in types])


@read
def get_all_dict(tx, vocabulary_id: str) -> dict[str, str]:
    types = tx.run(Q.all, vocabulary_id=vocabulary_id).value("type")
    color_dict = {x["name"]: determine_color_vocpopuli_type(x["name"]) for x in types}
    return color_dict


@read_terms_db
def all_bank(tx, vocabulary_id: str) -> list[VocPopuliType]:
    types = tx.run(Q.all_bank, vocabulary_id=vocabulary_id).value("type")
    reordered = [x for x in types if x["name"] != "detached"]
    detached = [x for x in types if x["name"] == "detached"]
    if detached:
        reordered.append(detached[0])

    return reordered


@read
def names(tx, vocabulary_id: str) -> list[VocPopuliType]:
    return tx.run(Q.names, vocabulary_id=vocabulary_id).value("type")


@write
def create(tx, vocabulary_id: str, name: str) -> VocPopuliType:
    return tx.run(
        Q.create, vocabulary_id=vocabulary_id, id=uuid4().hex, name=name
    ).value("type")


@write_terms_db
def import_terms_db_(tx, name: str, color: str = "gray") -> VocPopuliType:
    return tx.run(Q.import_terms_db_, name=name, color=color).value("type")


@write
def create_default_types(tx, vocabulary_id: str):
    tx.run(Q.create_default_types, vocabulary_id=vocabulary_id)


@write
def merge(tx, vocabulary_id: str, name: str) -> VocPopuliType:
    return tx.run(Q.merge, vocabulary_id=vocabulary_id, name=name).value("type")


@write
def set_new_order(tx, vocabulary_id: str, new_order: list[str]):
    new_order = [{"name": name, "position": i} for i, name in enumerate(new_order)]
    tx.run(Q.set_new_order, vocabulary_id=vocabulary_id, new_order=new_order)


@write
def delete(tx, vocabulary_id: str, name: str):
    tx.run(Q.delete, vocabulary_id=vocabulary_id, name=name)


@write
def edit(tx, vocabulary_id: str, name: str, old_name: str):
    tx.run(Q.edit, vocabulary_id=vocabulary_id, old_name=old_name, name=name)


@read
def all_types_in_all_vocabularies(tx) -> list[VocPopuliType]:
    types = tx.run(Q.all_types_in_all_vocabularies).value("type")
    reordered = [x for x in types if x["name"] != "detached"]
    reordered.append(
        [x for x in types if x["name"] == "detached"][0]
    )
    return reordered
