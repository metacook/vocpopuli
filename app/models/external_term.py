from dataclasses import dataclass
from datetime import datetime


@dataclass
class ExternalTerm:
    id: str
    url: str
    created_at: datetime
