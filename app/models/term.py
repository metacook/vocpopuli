from dataclasses import dataclass
from datetime import datetime


@dataclass
class Term:
    id: str  # id_t_global
    is_top_level: bool
    created_at: datetime
