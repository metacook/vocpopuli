from dataclasses import dataclass


@dataclass
class DataType:
    name: str
