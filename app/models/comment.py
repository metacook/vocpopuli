from dataclasses import dataclass
from datetime import datetime


@dataclass
class Comment:
    id: str
    body: str
    created_at: datetime
