from dataclasses import dataclass
from datetime import datetime


@dataclass
class User:
    id: str
    name: str
    username: str
    web_url: str
    avatar_url: str
    created_at: datetime
    updated_at: datetime
