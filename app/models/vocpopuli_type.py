from dataclasses import dataclass


@dataclass
class VocPopuliType:
    name: str
    color: str
