from dataclasses import dataclass


@dataclass
class RelationshipType:
    id: str
    label: str
    definition: str
