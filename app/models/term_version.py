from dataclasses import dataclass
from datetime import datetime


@dataclass
class TermVersion:
    id: str  # id_t_version
    id_t_local: str
    issue_version: int  # 0 = "initial version"
    label: str
    definition: str
    reference: str
    synonyms: list[str]
    unit: str
    created_at: datetime
    # related properties not stored on the node
    # None = has not been fetched
    generated_from: str | None = None
    is_approved: bool | None = None
    datatype: str | None = None
    vocpopuli_type: str | None = None
    contextual_type: str | None = None
    issue_iid: str | None = None
