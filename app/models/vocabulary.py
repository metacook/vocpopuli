from dataclasses import dataclass


@dataclass
class Vocabulary:
    id: str
