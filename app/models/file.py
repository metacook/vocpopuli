from dataclasses import dataclass
from datetime import datetime


@dataclass
class File:
    id: str
    url: str
    local: bool
    created_at: datetime
    updated_at: datetime
