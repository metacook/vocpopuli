from . import celery, DOTENV_PATH
from .db import (
    comment as db_comment,
    datatype as db_datatype,
    file as db_file,
    relationship_type as db_relationship_type,
    term as db_term,
    term_version as db_term_version,
    user as db_user,
    vocabulary as db_vocabulary,
)
from .gitlab_api import api
from .gitlab_api.repository_endpoints import Endpoints
from .models.file import File
from .utils.image import delete_image
from .utils.parsing import new_term_to_markdown
from .utils.repo import load_repositories
from .utils.terms import generate_uid
from shutil import copytree, move
from flask import current_app
from git import Repo
from werkzeug.datastructures import FileStorage
import requests
import dotenv
import os
import json

dotenv.load_dotenv(DOTENV_PATH)
GITLAB = "https://gitlab.com/api/v4"


@celery.task
def git_new_term(
    user_id,
    id_t_version,
    formdata,
    vocabulary_name,
    images,
    project_id,
    oauth_token,
):
    """
    Adds a new term
    """
    term_version = db_term_version.get(id_t_version)
    timestamp = term_version["created_at"].isoformat()
    id_t_global = term_version["term_id"]
    user = db_user.get(user_id)
    # generate term metadata
    term_metadata = {
        "created_by_user_id": user_id,
        "created_on": timestamp,
        "edited_by_user_id": user_id,
        "edited_on": timestamp,
        "id_t_global": id_t_global,
        "id_t_local": "",
        "id_t_version": id_t_version,
    }

    # generate the term's dictionary
    term_dict = {
        "metadata": term_metadata,
        "term": formdata,
    }

    # create a new repository branch and commit the new term definition as
    # a json file
    branch_name = f"new_term_{id_t_global}"
    term_label = term_version["label"]

    commit_message = f"New term definition: {term_label}. Global Term ID: {id_t_global}"

    # commit the file in a new branch
    main_branch_dir = os.path.join(
        current_app.config["VOCABULARIES_DIR"],
        vocabulary_name,
        f"{vocabulary_name}-main",
    )

    new_branch_dir = os.path.join(
        current_app.config["VOCABULARIES_DIR"],
        vocabulary_name,
        f"{vocabulary_name}-{branch_name}",
    )
    main_branch_repo = Repo(main_branch_dir)
    main_branch_repo.git.pull()
    copytree(main_branch_dir, new_branch_dir)

    # Initialize the new branch's repository
    new_branch_repo = Repo(new_branch_dir)
    # Create and checkout the new branch
    new_branch_repo.git.checkout("-b", branch_name)
    # Set the creator's credentials
    (
        new_branch_repo.config_writer()
        .set_value("user", "name", user["name"])
        .set_value("user", "email", user["email"])
        .set_value("pull", "rebase", "false")
        .release()
    )

    # Upload images (if such exist), and update JSON
    valid_img_upload_urls = []

    base_url = requests.get(
        f"{GITLAB}/projects/{project_id}",
        headers={"Authorization": f"Bearer {oauth_token}"},
    ).json()["web_url"]

    for i, file_id in enumerate(images):
        img_node = db_file.get(id=file_id)
        filepath = img_node["url"]
        with open(filepath, "rb") as f:
            img = FileStorage(f)
            img_file_name = f"{id_t_global}_{i}"
            _, ext = os.path.splitext(img.filename.lower())
            img_path = f"images/{img_file_name}{ext}"

            img.save(os.path.join(new_branch_repo.working_tree_dir, img_path))

        img_url = f"{base_url}/-/raw/{branch_name}/images/{img_file_name}{ext}"
        db_file.set_url(file_id, img_url, local=False)
        delete_image(filepath)
        valid_img_upload_urls.append(img_url)

    # update the term JSON with the image file names
    term_dict["image_files"] = valid_img_upload_urls

    # Create the issue
    term_markdown = new_term_to_markdown(term_dict)
    issue_title = f"[Initial Version] {term_label} | {id_t_global}"
    issue_resp = requests.post(
        f"{GITLAB}/projects/{project_id}/issues",
        data={
            "title": issue_title,
            "description": term_markdown,
            "labels": "new_term",
        },
        headers={"Authorization": f"Bearer {oauth_token}"},
    )

    issue_iid = issue_resp.json()["iid"]
    term_dict["metadata"]["issue_iid"] = issue_iid
    db_term_version.update_issue_iid(id_t_version, issue_iid=issue_iid)

    # the term's file path
    file_path = f"candidate_terms/{id_t_global}.json"
    with open(os.path.join(new_branch_repo.working_tree_dir, file_path), "w+") as f:
        json.dump(term_dict, f, indent=4)
        f.write(" \n")

    # Stage the new changes, and commit
    new_branch_repo.git.add(".")
    new_branch_repo.git.commit(
        "-m", commit_message, author=f"{user['name']} <{user['email']}>"
    )
    # TODO: optional push
    new_branch_repo.git.push("-u", "origin", branch_name)
    load_repositories(oauth_token)


@celery.task
def update_file_nodes(files: dict[str, str]):
    for file_id, new_url in files.items():
        File.set_url(file_id, new_url)


def _get_term_json_filename(term_id, term_version):
    # get the name of the respective term JSON (if not approved)
    if term_version["issue_version"] == 0:
        return f"{term_id}.json"

    return f"{term_id}_edit_{term_version['issue_version']}.json"


def _generate_term_metadata(
    user_id,
    term,
    term_version,
    previous_term_version,
    formdata,
    term_file_name,
    project_id,
):
    created_at = term["created_at"].isoformat()
    term_metadata = {
        "created_by_user_id": term["author_id"],
        "created_on": created_at,
        "edited_by_user_id": user_id,
        "edited_on": created_at,
        "id_t_global": term["id"],
        "id_t_local": "",
        "id_t_version": term_version["id"],
        "generated_from_id_t_version": previous_term_version["id"],
        "generated_from_file_name": term_file_name,
    }
    # generate the term's dictionary
    term_dict = {
        "metadata": term_metadata,
        "term": formdata,
    }
    # if record moved under another term, remove vocpopuli type
    if term_dict["term"]["broader"]:
        term_dict["term"]["vocpopuli_type"] = ""
    # if datatype not int or float, remove unit
    if not db_datatype.is_numeric(project_id, term_dict["term"]["datatype"]):
        term_dict["term"]["unit"] = ""
    return term_dict


@celery.task
def git_edit_term(
    user_id,
    term_version_id,
    previous_term_version_id,
    vocabulary_name,
    formdata,
    project_id,
    oauth_token,
):
    term_version = db_term_version.get(term_version_id)
    term = db_term.get(term_version["term_id"])
    previous_term_version = db_term_version.get(previous_term_version_id)
    user = db_user.get(user_id)
    vocabulary_branches_path = os.path.join(
        current_app.config["VOCABULARIES_DIR"], vocabulary_name
    )
    term_file_name = _get_term_json_filename(term["id"], previous_term_version)
    auth_headers = {"Authorization": f"Bearer {oauth_token}"}

    # generate term metadata
    term_dict = _generate_term_metadata(
        user_id,
        term,
        term_version,
        previous_term_version,
        formdata,
        term_file_name,
        project_id,
    )
    id_t_global = term["id"]

    if previous_term_version["is_approved"]:
        # if the term to be edited has already been approved,
        # it needs to be reset to an unapproved version
        # this includes the following steps:

        # 1. create the new .json which reflects the edit

        # 2. create a new branch reflecting the re-edit
        # and move the previously approved .JSON in the
        # candidate_terms folder of the new branch
        relevant_branches = list(
            api.get_branch_names(
                project_id=project_id,
                drop_merged=False,
                search=term["id"],
                oauth_token=oauth_token,
            )
        )

        n_reopenings = sum(1 for x in relevant_branches if "term_reopening_" in x)
        n_relevant_branches = len(relevant_branches)
        new_branch_name = f"term_reopening_{n_reopenings+1}_{term['id']}"

        match n_relevant_branches:
            case 0:
                last_branch_name = "main"
            case 1:
                last_branch_name = relevant_branches[0]
            case _:
                last_branch_name = f"term_reopening_{n_reopenings}_{id_t_global}"

        new_branch_commit_message = f"Reopened {term['id']} for editing."
        last_branch_dir = os.path.join(
            vocabulary_branches_path, f"{vocabulary_name}-{last_branch_name}"
        )
        # the path to the approved JSON in its previous branch
        last_branch_file_path = os.path.join(
            vocabulary_branches_path,
            f"{vocabulary_name}-{new_branch_name}",  # used after copy
            "approved_terms",
            f"{id_t_global}.json",
        )

        new_branch_dir = os.path.join(
            vocabulary_branches_path, f"{vocabulary_name}-{new_branch_name}"
        )
        # the path of the previously approved JSON in the new branch (w/ _edit_n.json)
        new_branch_file_path = os.path.join(
            new_branch_dir, "candidate_terms", f"{term_file_name}"
        )
        last_branch_repo = Repo(last_branch_dir)
        last_branch_repo.git.checkout(last_branch_name)
        last_branch_repo.git.pull()
        copytree(src=last_branch_dir, dst=new_branch_dir)
        repo = Repo(new_branch_dir)
        repo.git.checkout("-b", new_branch_name)
        (
            repo.config_writer()
            .set_value("user", "name", user["name"])
            .set_value("user", "email", user["email"])
            .set_value("pull", "rebase", "false")
            .release()
        )

        move(src=last_branch_file_path, dst=new_branch_file_path)
        repo.git.add(".")
        repo.git.commit("-m", new_branch_commit_message)

        branch_dir = new_branch_dir
        branch_name = new_branch_name
        # 3. Create a new .JSON in the new branch which reflects the edit

    else:
        branch_name = next(
            api.get_branch_names(
                project_id=project_id, search=term["id"], oauth_token=oauth_token
            )
        )
        branch_dir = os.path.join(
            vocabulary_branches_path, f"{vocabulary_name}-{branch_name}"
        )
        repo = Repo(branch_dir)
        (
            repo.config_writer()
            .set_value("user", "name", user["name"])
            .set_value("user", "email", user["email"])
            .set_value("pull", "rebase", "false")
            .release()
        )

    edit_count = term_version["issue_version"]

    term_label = term_dict["term"]["label"]
    commit_message = (
        f"Term edit #{edit_count}: {term_label}. " f"Global Term ID: {term['id']}"
    )

    base_url = requests.get(
        f"{GITLAB}/projects/{project_id}",
        headers=auth_headers,
    ).json()["web_url"]

    # Upload images (if such exist), and update JSON
    for file in db_term_version.get_files(term_version_id):
        if not file["local"]:
            continue

        filepath = file["url"]
        with open(filepath, "rb") as f:
            img = FileStorage(f)
            img_file_name = f"{term['id']}_{file['order_id']}"
            _, ext = os.path.splitext(img.filename.lower())
            img_path = f"images/{img_file_name}{ext}"
            img.save(os.path.join(repo.working_tree_dir, img_path))
        img_url = f"{base_url}/-/raw/{branch_name}/images/{img_file_name}{ext}"
        db_file.set_url(file["id"], img_url, local=False)
        delete_image(filepath)
    # update the term JSON with the image file names
    term_dict["image_files"] = db_term_version.get_image_urls(term_version_id)

    # Create the issue
    term_markdown = new_term_to_markdown(term_dict)
    if previous_term_version["is_approved"]:
        labels = "term_edit, term_reopening"
    else:
        labels = "term_edit"
    new_issues_response = api.post_new_term_issue(
        title=f"[Version Update #{edit_count}] {term_label} | {id_t_global}",
        description=term_markdown,
        labels=labels,
        project_id=project_id,
        oauth_token=oauth_token,
    )

    issue_iid = new_issues_response.json()["iid"]
    term_dict["metadata"]["issue_iid"] = issue_iid
    db_term_version.update_issue_iid(term_version_id, issue_iid=issue_iid)
    # upload the term's JSON
    file_name = f"{id_t_global}_edit_{edit_count}"

    file_path = os.path.join(branch_dir, "candidate_terms", f"{file_name}.json")

    with open(file_path, "w+") as f:
        json.dump(term_dict, f, indent=4)
        f.write(" \n")

    # Stage the new files, and commit
    repo.git.add(".")
    repo.git.commit("-m", commit_message, author=f"{user['name']} <{user['email']}>")

    if previous_term_version["is_approved"]:
        # 4. Reopen all of the term's closed issues, and remove the 'approved' tag
        relevant_closed_issues = api.get_issues_list(
            params={"state": "closed", "search": term["id"], "in": "title"},
            project_id=project_id,
            oauth_token=oauth_token,
        )
        for x in relevant_closed_issues:
            params = {"state_event": "reopen"}
            if "approved" in x["labels"]:
                params["remove_labels"] = "approved"
            requests.put(
                f"{GITLAB}/projects/{project_id}/issues/{x['iid']}",
                params=params,
                headers=auth_headers,
            )

    # TODO: optional push
    repo.git.push("-u", "origin", branch_name)
    load_repositories(oauth_token)


@celery.task
def git_approve_term(project_id, user_id, term_version, id_t_global, oauth_token):
    user = db_user.get(user_id)
    vocabulary = db_vocabulary.get(project_id)
    vocabulary_name = vocabulary["name"].replace(" ", "-")
    vocabulary_branches_path = os.path.join(
        current_app.config["VOCABULARIES_DIR"], vocabulary_name
    )
    headers = {"Authorization": f"Bearer {oauth_token}"}

    # get the name of the respective term json file
    if term_version["issue_version"] == 0:
        term_file_name = id_t_global + ".json"
    else:
        term_file_name = f"{id_t_global}_edit_{term_version['issue_version']}.json"

    # get the current term branch
    # branches = [x for x in api.get_branch_names(project_id=project_id) if id_t_global in x]
    # term_branch_name = branches[0]
    ep = Endpoints(project_id)
    for _ in range(10):
        r = requests.get(
            f"{GITLAB}/projects/{project_id}/repository/branches",
            params={"search": id_t_global},
            headers=headers,
        )
        if r.ok:
            break

    term_branch_name = sorted(
        r.json(), key=lambda x: x["commit"]["committed_date"], reverse=True
    )[0]["name"]

    # TODO: add pagination
    all_term_issues = requests.get(
        f"{GITLAB}/projects/{project_id}/issues",
        params={"search": id_t_global, "in": "title"},
        headers=headers,
    ).json()

    term_branch_dir = os.path.join(
        vocabulary_branches_path, f"{vocabulary_name}-{term_branch_name}"
    )
    term_file_path = os.path.join(term_branch_dir, "candidate_terms", term_file_name)
    term_branch_repo = Repo(term_branch_dir)
    term_branch_repo.git.checkout(term_branch_name)
    term_branch_repo.git.merge("origin/main")
    term_branch_repo.git.pull()
    # add an id_t_local to the approved term's json
    with open(term_file_path, "r") as f:
        term_dict = json.load(f)
    term_dict["metadata"]["id_t_local"] = generate_uid() + "-vp"
    with open(term_file_path, "w") as f:
        json.dump(term_dict, f, indent=4)

    new_term_file_path = os.path.join(
        term_branch_dir, "approved_terms", f"{id_t_global}.json"
    )

    (
        term_branch_repo.config_writer()
        .set_value("user", "name", user["name"])
        .set_value("user", "email", user["email"])
        .set_value("pull", "rebase", "false")
        .release()
    )
    move(src=term_file_path, dst=new_term_file_path)
    term_branch_repo.git.add(".")
    term_branch_repo.git.commit("-m", f"Approved {term_file_name}")
    term_branch_repo.git.push()
    term_branch_repo.git.checkout("main")
    term_branch_repo.git.pull()
    term_branch_repo.git.merge(term_branch_name)

    # empty the 'candidate_terms' folder of the 'main' branch
    candidate_terms_dir = os.path.join(term_branch_dir, "candidate_terms")
    candidate_jsons = os.listdir(candidate_terms_dir)
    jsons = [x for x in candidate_jsons if x.endswith(".json")]
    if jsons:
        for file in jsons:
            os.remove(os.path.join(candidate_terms_dir, file))
        term_branch_repo.git.add(".")
        term_branch_repo.git.commit("-m", "Deleted the candidate terms from main")
        term_branch_repo.git.push()

    # pull the main branch directory
    main_branch_dir = os.path.join(vocabulary_branches_path, f"{vocabulary_name}-main")
    main_branch_repo = Repo(main_branch_dir)
    main_branch_repo.git.pull()

    # close the issues, and add an 'approved' tag
    for x in all_term_issues:
        if x["iid"] == term_version["issue_iid"]:
            desc = x["description"]
            desc = desc.replace(
                "Local Term ID:</strong> ",
                f"Local Term ID:</strong> {term_dict['metadata']['id_t_local']}",
            )
            params = {
                "state_event": "close",
                "add_labels": "approved",
                "description": desc,
            }
        else:
            params = {"state_event": "close"}

        requests.put(
            f"{GITLAB}/{ep.ISSUES_EP}/{x['iid']}", params=params, headers=headers
        )
    load_repositories(oauth_token)


@celery.task
def git_vote(project_id, user_id, term_version_id, vote, oauth_token):
    term_version = db_term_version.get(term_version_id)
    url = (
        f"{GITLAB}/projects/{project_id}/issues/{term_version['issue_iid']}/award_emoji"
    )
    headers = {"Authorization": f"Bearer {oauth_token}"}

    # remove existing award(s)
    awards = requests.get(url, headers=headers).json()
    for award_id in (award["id"] for award in awards if award["user"]["id"] == user_id):
        requests.delete(f"{url}/{award_id}", headers=headers)

    if vote == "up":
        requests.post(url, params={"name": "thumbsup"}, headers=headers)
    elif vote == "down":
        requests.post(url, params={"name": "thumbsdown"}, headers=headers)


@celery.task
def git_comment(project_id: str, comment_id: str, oauth_token: str):
    comment = db_comment.get(comment_id)
    requests.post(
        f"{GITLAB}/projects/{project_id}/issues/{comment['issue_iid']}/notes",
        data={"body": comment["body"]},
        headers={"Authorization": f"Bearer {oauth_token}"},
    )


@celery.task
def git_new_relationship_type(
    user_id,
    type_id,
    vocabulary_name,
    project_id,
    oauth_token,
):
    # get all the data
    relationship_type = db_relationship_type.get(type_id)
    timestamp = relationship_type["created_at"].isoformat()
    id_t_global = relationship_type["id_t_global"]
    user = db_user.get(user_id)

    # generate term metadata
    term_metadata = {
        "created_by_user_id": user_id,
        "created_on": timestamp,
        "edited_by_user_id": user_id,
        "edited_on": timestamp,
        "id_t_global": id_t_global,
        "id_t_local": relationship_type["id_t_local"],
        "id_t_version": relationship_type["id_t_version"],
    }

    # generate the term's dictionary
    term_dict = {
        "metadata": term_metadata,
        "term": {
            "label": relationship_type["label"],
            "definition": relationship_type["definition"],
            "related_iri": relationship_type["related_iri"],
        },
    }

    # add empty fields
    term_dict["term"]["synonyms"] = [""]
    term_dict["term"]["translations"] = [""]
    term_dict["term"]["datatype"] = ""
    term_dict["term"]["unit"] = ""
    term_dict["term"]["min_"] = ""
    term_dict["term"]["max_"] = ""
    term_dict["term"]["required"] = False
    term_dict["term"]["vocpopuli_type"] = "relationship_type"
    term_dict["term"]["broader"] = []
    term_dict["term"]["related"] = []
    term_dict["term"]["related_external"] = [""]
    term_dict["term"]["custom_relationships"] = []
    term_dict["image_files"] = []

    type_label = relationship_type["label"]
    commit_message = (
        f"New relationship type: {type_label}. Global Term ID: {id_t_global}"
    )
    sanitized_vocabulary_name = vocabulary_name.replace(" ", "-")
    main_branch_dir = os.path.join(
        current_app.config["VOCABULARIES_DIR"],
        sanitized_vocabulary_name,
        f"{sanitized_vocabulary_name}-main",
    )

    main_branch_repo = Repo(main_branch_dir)
    main_branch_repo.git.pull()
    # Set the creator's credentials
    (
        main_branch_repo.config_writer()
        .set_value("user", "name", user["name"])
        .set_value("user", "email", user["email"])
        .set_value("pull", "rebase", "false")
        .release()
    )
    headers = {"Authorization": f"Bearer {oauth_token}"}

    # Create the issue
    term_markdown = new_term_to_markdown(term_dict)
    issue_title = f"[Initial Version] {type_label} | {id_t_global}"
    issue_resp = requests.post(
        f"{GITLAB}/projects/{project_id}/issues",
        data={
            "title": issue_title,
            "description": term_markdown,
            "labels": "new_term",
        },
        headers=headers,
    )

    issue_iid = issue_resp.json()["iid"]
    term_dict["metadata"]["issue_iid"] = issue_iid
    db_relationship_type.update_issue_iid(type_id, issue_iid=issue_iid)

    # Close the issue and mark as approved
    params = {
        "state_event": "close",
        "add_labels": "approved",
    }
    requests.put(
        f"{GITLAB}/projects/{project_id}/issues/{issue_iid}",
        params=params,
        headers=headers,
    )

    # the term's file path
    file_path = f"approved_terms/{id_t_global}.json"
    with open(os.path.join(main_branch_repo.working_tree_dir, file_path), "w+") as f:
        json.dump(term_dict, f, indent=4)
        f.write(" \n")

    # Stage the new changes, and commit
    main_branch_repo.git.add(".")
    main_branch_repo.git.commit(
        "-m", commit_message, author=f"{user['name']} <{user['email']}>"
    )
    # TODO: optional push
    main_branch_repo.git.push("-u", "origin", "main")
    load_repositories(oauth_token)
