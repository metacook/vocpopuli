"""
Views used in the main blueprint
"""

from . import main
from .. import celery
from .forms import (
    AddNewCommentForm,
    CreateDataTypeForm,
    CreateRelationshipTypeForm,
    CreateVocPopuliTypeForm,
    GitlabImportForm,
    KadiTemplateExport,
    NewTermForm,
    PublishVocabularyForm,
    RelationshipForm,
    SelectProjectForm,
    TermsJSONsUploadForm,
    UserSettingsForm,
    VocabularyMetadata,
    SelectVocabularyFromBankForm,
    SelectTermFromBankForm,
)
from .. import DOTENV_PATH
from ..db import (
    datatype as db_datatype,
    file as db_file,
    relationship_type as db_relationship_type,
    term as db_term,
    term_version as db_term_version,
    user as db_user,
    vocabulary as db_vocabulary,
    vocpopuli_type as db_vocpopuli_type,
)
from ..gitlab_api.repository_endpoints import Endpoints
from ..gitlab_api import api
from ..decorators import login_required, selected_project_required
from ..celery_tasks import (
    git_approve_term,
    git_comment,
    git_edit_term,
    git_new_relationship_type,
    git_new_term,
    git_vote,
)
from ..utils.gitlab_import import (
    get_approved_versions,
    get_comments,
    get_commits,
    get_issues,
    get_latest_branches,
    get_term_versions,
    get_user_id,
    get_votes,
    merge_user,
)
from ..utils.image import (
    delete_image,
    filter_valid_images,
    img_filetypes,
    load_image,
    save_in_uploads,
)
from ..utils.terms import get_term_jsons, generate_uid, term_tag
from ..utils.tree import term_tree, term_tree_from_bank, full_tree
from ..utils.parsing import (
    new_term_to_markdown,
    parse_issue_description,
    pre_jsonify_form,
)
from ..utils.repo import load_repositories
from ..utils.skos import SKOSSerializer
from ..utils.license import CC_BY_40
from ..utils.vocpopuli_types import determine_color_vocpopuli_type
from ..utils.relation import generate_version_json
from flask import (
    current_app,
    redirect,
    render_template,
    request,
    Response,
    send_file,
    session,
    url_for,
    flash,
)
from flask_dance.contrib.gitlab import gitlab
from requests import HTTPError
from uuid import uuid4
from werkzeug.datastructures import FileStorage

from git import Repo

from kadi_apy import KadiManager
from datetime import datetime, timedelta
from shutil import copytree, move
from markdown import markdown
import dotenv

import hashlib
import re
import os
import json
import time
import uuid
import requests

dotenv.load_dotenv(DOTENV_PATH)


def _submit_new_term(form, return_to_list_terms: bool = True):
    """
    A wrapper function which takes the submitted form
    containing a new term's info, and creates a term from it.
    """
    # get logged-in user's info
    user_id = session["USER_ID"]
    project_id = session["PROJECT_ID"]
    formdata = pre_jsonify_form(form.data)

    id_t_global = f"{term_tag(formdata['label'])}-{generate_uid()}-vp"
    id_t_version = f"{generate_uid()}-vp"

    if not db_datatype.is_numeric(project_id, formdata["datatype"]):
        for field_name in ("unit", "min_", "max_"):
            formdata[field_name] = ""

    term = db_term.create(
        user_id=user_id,
        term_id=id_t_global,
        term_version_id=id_t_version,
        label=formdata["label"],
        definition=formdata["definition"],
        reference=formdata.get("reference", ""),
        synonyms=formdata["synonyms"],
        translations=formdata["translations"],
        unit=formdata["unit"],
        datatype=formdata["datatype"],
        broader=formdata["broader"],
        related=formdata["related"],
        related_external_terms=formdata["related_external"],
        vocpopuli_type=formdata["vocpopuli_type"] if len(formdata["broader"]) == 0 else None,
        # TODO change to an actual vocabulary id
        vocabulary_id=session.get("PROJECT_ID"),
        min_=formdata.get("min_"),
        max_=formdata.get("max_"),
        required=formdata["required"],
    )
    db_term_version.add_custom_relationships(
        id=term["latest_term_version_id"],
        relationships=formdata["custom_relationships"],
        vocabulary_id=session.get("PROJECT_ID"),
    )
    images = []
    for i, image in enumerate(filter_valid_images(form.images.data)):
        file_id = uuid4().hex
        path = save_in_uploads(image, file_id)
        file = db_file.create(
            id=file_id,
            term_version_id=id_t_version,
            url=path,
            order_id=i,
        )
        images.append(file["id"])

    vocabulary_id = session["PROJECT_ID"]
    vocabulary_name = db_vocabulary.get(vocabulary_id)["name"].replace(" ", "-")
    # git_new_term.delay(
    #     user_id,
    #     id_t_version,
    #     formdata,
    #     vocabulary_name,
    #     images,
    #     session.get("PROJECT_ID"),
    #     gitlab.token["access_token"],
    # )
    
    session["last_added_term"] = id_t_version
    if not return_to_list_terms:
        return term["latest_term_version_id"]

    return redirect(url_for(".view_term", term_version_id=term["latest_term_version_id"]))


def _submit_edit_term(form, term, previous_version, create_new_version=False):
    formdata = pre_jsonify_form(form.data)
    user_id = session["USER_ID"]
    project_id = session["PROJECT_ID"]

    if not db_datatype.is_numeric(project_id, formdata["datatype"]):
        for field_name in ("unit", "min_", "max_"):
            formdata[field_name] = ""

    if create_new_version:
        term_version = db_term_version.create(
            id=f"{generate_uid()}-vp",
            term_id=term["id"],
            user_id=user_id,
            label=formdata["label"],
            definition=formdata["definition"],
            reference=formdata.get("reference", ""),
            synonyms=formdata["synonyms"],
            translations=formdata["translations"],
            unit=formdata["unit"],
            datatype=formdata["datatype"],
            broader=formdata["broader"],
            related=formdata["related"],
            related_external_terms=formdata["related_external"],
            vocpopuli_type=formdata["vocpopuli_type"] if len(formdata["broader"]) == 0 else None,
            min_=formdata["min_"],
            max_=formdata["max_"],
            required=formdata["required"],
            vocabulary_id=project_id,
            generated_from=previous_version["id"]
        )
    else:
        term_version = db_term_version.update(
            id=previous_version["id"],
            broader=formdata["broader"],
            related=formdata["related"],
            related_external_terms=formdata["related_external"],
            vocpopuli_type=formdata["vocpopuli_type"],
            vocabulary_id=project_id
        )

    db_term_version.add_custom_relationships(
        id=term_version["id"],
        relationships=formdata["custom_relationships"],
        vocabulary_id=project_id,
    )

    no_of_kept_images = 0
    if form.keep_prev_imgs.data:
        no_of_kept_images = db_term_version.keep_images(term_version["id"])

    new_images = []
    for i, image in enumerate(
        filter_valid_images(form.images.data), start=no_of_kept_images
    ):
        file_id = uuid4().hex
        path = save_in_uploads(image, file_id)
        file = db_file.create(
            id=file_id,
            term_version_id=term_version["id"],
            url=path,
            order_id=i,
        )
        new_images.append(file["id"])

    vocabulary_id = session["PROJECT_ID"]
    vocabulary_name = db_vocabulary.get(vocabulary_id)["name"]

    # git_edit_term.delay(
    #     user_id,
    #     term_version["id"],
    #     previous_version["id"],
    #     vocabulary_name.replace(" ", "-"),
    #     formdata,
    #     vocabulary_id,
    #     gitlab.token["access_token"],
    # )

    return redirect(url_for(".view_term", term_version_id=term_version["id"]))


def _get_select_project_form():
    return SelectProjectForm(
        project_id=session.get("PROJECT_ID"), user_id=session["USER_ID"]
    )


@main.route("/")
def root():
    if not gitlab.authorized:
        return redirect(url_for("gitlab.login_prompt"))
    else:
        return redirect(url_for("main.index"))


@main.route("/choose_project", methods=["POST"])
@login_required
def choose_project():
    selected_vocabulary = request.form.get("project")
    vocabulary = db_vocabulary.get(selected_vocabulary)

    if not vocabulary:
        session["PROJECT_ID"] = None
        session["LAST_ACTIVITY"] = None
        return redirect(url_for(".index"))

    session["PROJECT_ID"] = selected_vocabulary
    # saving it as a datetime object doesn't work with Flask_Session(2)
    session["LAST_ACTIVITY"] = vocabulary["updated_at"].isoformat()
    return redirect(url_for(".list_terms"))


@main.route("/index")
@login_required
def index():
    user_id = session["USER_ID"]

    project_id = session.get("PROJECT_ID")
    project_name = (
        db_vocabulary.get(project_id)["name"]
        if project_id
        else "Choose a vocabulary project:"
    )
    other_project_names = [
        vocabulary["name"]
        for vocabulary in db_user.get_vocabularies(user_id)
        if vocabulary["name"] != project_name
    ]
    return render_template(
        "index.html",
        select_project_form=_get_select_project_form(),
        project_name=project_name,
        other_project_names=other_project_names,
    )


@main.route("/child_term/<term_version_id>", methods=["GET", "POST"])
@selected_project_required
def child_term(term_version_id):
    vocabulary_id = session["PROJECT_ID"]
    parent = db_term_version.get(id=term_version_id,
                                 vocabulary_id=vocabulary_id)

    # TODO: add back default children type
    # ("option" for "string", "boolean", "integer", "float", "date")
    form = NewTermForm(
        vocabulary_id,
        broader=[parent["term_id"]],
        label=request.args.get("label", ""),
        definition=request.args.get("definition", ""),
        datatype=request.args.get("datatype", ""),
    )

    # check if submission is valid and submit new term
    if form.validate_on_submit():
        return _submit_new_term(form)

    return render_template(
        "child_term.html",
        form=form,
        _template=RelationshipForm(),
        term_version_id=term_version_id,
        tree_dict=term_tree(vocabulary_id),
        vocpopuli_types=db_vocpopuli_type.all(vocabulary_id),
        select_project_form=_get_select_project_form(),
        data_types_dict=db_datatype.get_all_dict(vocabulary_id),
    )

@main.route("/copy_term/<term_version_id>", methods=["GET", "POST"])
@selected_project_required
def copy_term(term_version_id):
    vocabulary_id = session["PROJECT_ID"]
    old_term = db_term_version.get(term_version_id, vocabulary_id)
    
    broader = [
        broader["id"] for broader in db_term_version.get_broader(old_term["id"])
    ]
    related = [
        related["id"] for related in db_term_version.get_related(old_term["id"])
    ]
    related_external = (
        ",".join(old_term["related_external_terms"])
        if old_term["related_external_terms"]
        else ""
    )
    custom_relationships = db_term_version.get_custom_relationships(old_term["id"], vocabulary_id)
    # fill the form for editing the term
    form = NewTermForm(
        vocabulary_id,
        label=old_term.get("label", ""),
        datatype=old_term.get("datatype", ""),
        reference=old_term.get("reference", ""),
        definition=old_term.get("definition", ""),
        unit=old_term["unit"],
        min_=old_term.get("min_", ""),
        max_=old_term.get("max_", ""),
        required_data=old_term.get("required"),
        vocpopuli_type=old_term["vocpopuli_type"],
        broader=broader,
        related=related,
        related_external=related_external,
        custom_relationships=custom_relationships,
    )
    
    # check if submission is valid and submit new term
    if form.validate_on_submit():
        return _submit_new_term(form)

    return render_template(
        "new_term.html",
        form=form,
        _template=RelationshipForm(),
        term_version_id=term_version_id,
        tree_dict=term_tree(vocabulary_id),
        vocpopuli_types=db_vocpopuli_type.all(vocabulary_id),
        select_project_form=_get_select_project_form(),
    )


@main.route("/new_term", methods=["GET", "POST"])
@selected_project_required
def new_term():
    """
    The view function used in the creation of a new term.
    It renders an instance of NewTermForm, submits its data to
    the respective vocabulary's repository, and creates a new issue
    which reflects the creation of a new term.
    """
    project_id = session["PROJECT_ID"]
    form = NewTermForm(project_id)

    # check if submission is valid and submit new term
    if form.validate_on_submit():
        return _submit_new_term(form)

    return render_template(
        "new_term.html",
        form=form,
        _template=RelationshipForm(),
        tree_dict=term_tree(project_id),
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        select_project_form=_get_select_project_form(),
        data_types_dict=db_datatype.get_all_dict(project_id),
    )


@main.route("/edit_term/<term_version_id>", methods=["GET", "POST"])
@selected_project_required
def edit_term(term_version_id):
    """
    The view function used to edit a given term.

    issue_iid --- The IID of the issue linked to the version of the
    term which the user wants to edit.
    """
    project_id = session["PROJECT_ID"]
    term_version = db_term_version.get(id=term_version_id,
                                       vocabulary_id=project_id)
    term = db_term.get(term_version["term_id"])
    label = term_version["label"]

    broader = [
        broader["id"] for broader in db_term_version.get_broader(term_version["id"])
    ]
    related = [
        related["id"] for related in db_term_version.get_related(term_version["id"])
    ]
    related_external = (
        ",".join(term_version["related_external_terms"])
        if term_version["related_external_terms"]
        else ""
    )
    custom_relationships = db_term_version.get_custom_relationships(term_version["id"], project_id)
    # fill the form for editing the term
    form = NewTermForm(
        project_id,
        edited_term_id=term["id"],
        label=label,
        datatype=term_version["datatype"],
        reference=term_version.get("reference", ""),
        unit=term_version["unit"],
        min_=term_version.get("min_", ""),
        max_=term_version.get("max_", ""),
        required_data=term_version.get("required"),
        vocpopuli_type=term_version["vocpopuli_type"],
        broader=broader,
        related=related,
        related_external=related_external,
        custom_relationships=custom_relationships,
    )
    # form.label.data = form_data_dict['label']
    form.label.validators[1].values = []

    tree_dict = term_tree(project_id)
    create_new_version = False

    if request.method == "GET":
        form.definition.data = term_version["definition"]
        form.synonym.data = ",".join(term_version["synonyms"])
        form.translations.data = ",".join(term_version.get("translations", []))

    # TODO: implement a "smarter" diff checker
    # TODO: decide on VocPopuli Type
    if request.method == "POST":
        label_change = (form.label.data != term_version["label"])
        datatype_change = (form.datatype.data != term_version["datatype"])
        unit_change = (form.unit.data != term_version.get("unit", ""))
        min_change = (form.min_.data != term_version.get("min_", ""))
        max_change = (form.max_.data != term_version.get("max_", ""))
        required_change = (form.required.data != term_version.get("required"))
        # vocpopuli_type_change = (form.vocpopuli_type.data != term_version["vocpopuli_type"])
        definition_change = (form.definition.data != term_version["definition"])
        reference_change = (form.reference.data != term_version.get("reference", ""))
        synonyms_change = (form.synonym.data != ",".join(term_version["synonyms"]))
        translations_change = (form.translations.data != ",".join(term_version.get("translations", [])))

        if (label_change or
            datatype_change or
            unit_change or
            min_change or
            max_change or
            required_change or
            # vocpopuli_type_change or
            definition_change or
            reference_change or
            synonyms_change or
            translations_change):
            create_new_version = True

    # check if submission is valid and submit new term
    if form.validate_on_submit():
        return _submit_edit_term(form,
                                 term,
                                 term_version,
                                 create_new_version=create_new_version)

    return render_template(
        "edit_term.html",
        form=form,
        _template=RelationshipForm(),
        term_version_id=term_version_id,
        edit_term=term_version["label"],
        tree_dict=tree_dict,
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        select_project_form=_get_select_project_form(),
        data_types_dict=db_datatype.get_all_dict(project_id),
    )


@main.route("/view_term/<term_version_id>", methods=["GET", "POST"])
@selected_project_required
def view_term(term_version_id):
    project_id = session.get("PROJECT_ID")
    user_id = session["USER_ID"]
    term_version = db_term_version.get(id=term_version_id,
                                       vocabulary_id=project_id)
    id_t_global = term_version["term_id"]

    all_term_versions = db_term.get_versions(id=id_t_global,
                                             vocabulary_id=project_id)

    # get the status of the term version
    # check if any version has been approved
    term_version["status"] = db_term_version.get_approval_status(id=term_version_id,
                                                                 vocabulary_id=project_id)

    comments = db_term.get_comments(term_version["term_id"])
    images = [load_image(image) for image in db_term_version.get_files(term_version_id)]

    # load comments section
    comments_form = AddNewCommentForm()
    if comments_form.validate_on_submit():
        comment = db_user.comment_on_term_version(
            user_id,
            term_version_id,
            body=comments_form.comment.data,
        )
        # git_comment.delay(
        #     project_id, comment["id"], oauth_token=gitlab.token["access_token"]
        # )
        return redirect(url_for(".view_term", term_version_id=term_version_id))

    votes = db_term_version.get_votes(term_version_id)
    user_vote = db_term_version.get_user_vote(term_version_id, user_id)
    # check if a vote has been sent TODO: replace with javascript
    if vote := request.args.get("vote"):
        if vote == user_vote:
            vote = ""
        db_user.vote_on_term_version(user_id, term_version_id, vote)
        # git_vote.delay(
        #     project_id,
        #     user_id,
        #     term_version_id,
        #     vote,
        #     oauth_token=gitlab.token["access_token"],
        # )

        return redirect(url_for(".view_term", term_version_id=term_version_id))
    tree_dict = term_tree(project_id)
    # set read session key
    project_name = db_vocabulary.get(project_id)["name"]
    session_key = f"visited_{term_version_id}_{project_name}"
    if session_key not in session:
        session[session_key] = True

    if db_datatype.is_numeric(project_id, term_version["datatype"]).get("type.numeric"):
        fields = (
            "definition",
            "min_",
            "max_",
            "synonyms",
            "translations",
            "datatype",
        )
    else:
        fields = (
            "definition",
            "synonyms",
            "translations",
            "datatype",
        )

    custom_relationships = db_term_version.get_custom_relationships_full(
        id=term_version["id"],
        vocabulary_id=project_id
    )
    is_archived = db_term.is_archived(id_t_global,
                                      vocabulary_id=project_id)
    
    # for the version graph
    related_version_json = generate_version_json(db_term.get_versions_with_authors(id_t_global, project_id))
  
    return render_template(
        "view_term.html",
        term_version=term_version,
        images=images,
        votes=votes,
        user_vote=user_vote,
        comments=comments,
        comments_form=comments_form,
        all_term_versions=all_term_versions,
        related_version_json=related_version_json,
        tree_dict=tree_dict,
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        vocpopuli_type_colors=db_vocpopuli_type.get_all_dict(project_id),
        datatypes_dict=db_datatype.get_all_dict(project_id),
        select_project_form=_get_select_project_form(),
        fields=fields,
        custom_relationships=custom_relationships,
        is_archived=is_archived,
    )


@main.route("/authorization_status", methods=["GET"])
def authorization_status():
    """
    Checks the user's gitlab authorization status.
    """
    return Response(
        json.dumps(
            {
                "authorized": gitlab.authorized,
                "expires_in": session.get("gitlab_oauth_token", {}).get(
                    "expires_in", 0
                ),
            }
        ),
        content_type="application/json",
    )


@main.route("/tree_dict_status", methods=["GET"])
@selected_project_required
def tree_dict_status():
    """
    Checks the status of the celery task responsible
    for loading the vocabulary tree. Returns a Response
    object which can be read on the front-end using
    JavaScript's fetch() API.
    """

    def response(value):
        return Response(json.dumps({"done": value}), content_type="application/json")

    project_id = session["PROJECT_ID"]
    project_name = db_vocabulary.get(project_id)["name"]
    task_id = session.get(f"tree_dict_task_id_{project_name}")
    if not task_id:
        return response("halt")

    result = celery.AsyncResult(task_id)
    ready = result.ready()
    if not ready:
        return response(False)

    session.pop(f"tree_dict_task_id_{project_name}")
    new_tree = result.get()

    if str(new_tree) != str(session[f"tree_dict_{project_name}"]):
        session[f"tree_dict_{project_name}"] = new_tree
        return response(ready)

    return response("halt")


def _get_vocab_tree(as_json: bool = False):
    """
    A wrapper function which initiates the celery task
    used to load the vocabualry tree
    """
    project_id = session.get("PROJECT_ID")

    vocabulary_terms = {
        version["term_id"]: version
        for version in db_term_version.all_latest_versions_with_custom_relationships(
            project_id
        )
    }

    top_level_term_ids = [term["id"] for term in db_term.all_top_level(project_id)]
    if not top_level_term_ids:
        return {}

    tree_json = full_tree(
        vocabulary_terms=vocabulary_terms,
        top_level_term_ids=top_level_term_ids,
        project_id=project_id,
    )

    if as_json:
        return tree_json

    tree_dict = json.loads(tree_json)
    return tree_dict


@main.route("/list_terms", methods=["GET", "POST"])
@selected_project_required
def list_terms():
    """
    The view function used to visualize the structure of
    the vocabulary in the 'List terms' view.
    """
    project_id = session["PROJECT_ID"]
    project_name = db_vocabulary.get(project_id)["name"]

    # quick term addition
    new_term_form = NewTermForm(project_id, min_="", max_="")

    if request.method == "POST":
        new_term_form.label.data = request.form.get("term_name")
        new_term_form.definition.data = request.form.get("description", "n/a")
        new_term_form.datatype = request.form.get("datatype", "dictionary")
        broader_term = request.form.get("broader_term_id")
        new_term_form.broader.data = [broader_term] if broader_term else []
        new_term_form.synonym.data = ""
        new_term_form.related_external.data = ""
        new_term_form.translations.data = ""
        session["last_added_term"] = _submit_new_term(new_term_form, return_to_list_terms=False)
    
    if session.get("last_added_term") is not None:
        unfold_term = session.get("last_added_term")
        session["last_added_term"] = None
    else:
        unfold_term = None

    tree_dict = term_tree(project_id)
    return render_template(
        "list_terms.html",
        project_name=project_name,
        tree_dict=tree_dict,
        datatypes_dict=db_datatype.get_all_dict(project_id),
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        select_project_form=_get_select_project_form(),
        new_term_form=new_term_form,
        datatype_configuration=json.dumps({x["name"]: x["child_types"] for x in db_datatype.get_child_datatype_configuration(project_id)}),
        unfold_term=unfold_term,
    )


@main.route("/archived_terms", methods=["GET", "POST"])
@selected_project_required
def archived_terms():
    """
    The view function used to visualize the structure of
    the archived terms in a given vocabulary
    """
    project_id = session["PROJECT_ID"]
    project_name = db_vocabulary.get(project_id)["name"]
    tree_dict = term_tree(project_id, archived_only=True)
    return render_template(
        "list_terms.html",
        project_name=project_name,
        tree_dict=tree_dict,
        datatypes_dict=db_datatype.get_all_dict(project_id),
        vocpopuli_types=[{"color": "gray", "name": "archived"}],
        select_project_form=_get_select_project_form(),
        new_term_form=NewTermForm(project_id),
        archive = True
    )


@main.route("/vocabulary_graphs", methods=["GET"])
@selected_project_required
def vocabulary_graphs():
    """
    A function used to pass the data needed for
    the graph visualization of the vocabulary
    to the respective html template
    """
    project_id = session["PROJECT_ID"]

    return render_template(
        "vocabulary_graphs.html",
        tree_json=_get_vocab_tree(as_json=True),
        datatypes=db_datatype.icon_dict(project_id),
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        related_json=json.dumps(db_term.get_for_graph(project_id)),
        toplevel_related_json = json.dumps(db_term.get_for_graph_toplevel(project_id)),
        select_project_form=_get_select_project_form(),
    )


@main.route("/download_json", methods=["GET"])
@selected_project_required
def download_json():
    """
    A function used to download the JSON file
    describing the whole vocabulary.
    """
    project_id = session["PROJECT_ID"]
    project_name = db_vocabulary.get(project_id)["name"]

    filename = f"{project_name.replace(' ', '_')}.json"
    # trigger file download
    headers = {"Content-Disposition": f"attachment;filename={filename}"}

    return Response(
        _get_vocab_tree(as_json=True),
        mimetype="application/json",
        headers=headers,
    )


@main.route("/import_existing_vocabulary", methods=["GET", "POST"])
@login_required
def import_existing_vocabulary():
    form = GitlabImportForm()

    if form.validate_on_submit():
        user_id = session["USER_ID"]
        project_id = form.project_id.data
        ep = Endpoints(project_id=project_id)

        vocabulary = gitlab.get(ep.PROJECT_EP).json()

        merge_user(user_id)
        db_vocabulary.create(project_id, vocabulary["name"], user_id)
        db_datatype.create_default_types(project_id)
        db_vocpopuli_type.create_default_types(project_id)

        filename_to_term_version_id = {}

        # get the latest branch for each term (reopening with the highest number)
        latest_branches = get_latest_branches(project_id)

        parsed_issues = {}
        issue_iid_to_term_version_id = {}
        term_issues = {}
        issue_metadata_dict = {}
        approved_term_version_ids = set()
        for issue in get_issues(project_id):
            parsed_issue = parse_issue_description(
                issue["description"], vocabulary["id"]
            )

            issue_metadata_dict[issue["iid"]] = issue

            term_id = parsed_issue["id_t_global"]
            if term_id in term_issues:
                term_issues[term_id][issue["iid"]] = parsed_issue
            else:
                term_issues[term_id] = {issue["iid"]: parsed_issue}

            parsed_issues[issue["iid"]] = parsed_issue
            term_version_id = parsed_issue["id_t_version"]
            issue_iid_to_term_version_id[issue["iid"]] = term_version_id
            if "approved" in issue["labels"]:
                approved_term_version_ids.add(term_version_id)

        approved_versions = {}
        for term_version in get_approved_versions(project_id):
            approved_versions[term_version["metadata"]["id_t_global"]] = term_version

        # filter out relationship types, import them, and remove them from
        # term_issues
        actual_term_ids = set()
        for term_id, issues_dict in list(term_issues.items()):
            # assume the first issue has the correct data, since it's not
            # currently possible to edit the relationship types
            issue_iid, issue = list(issues_dict.items())[0]
            vp_type = issue.get("vocpopuli_type", "relationship_type")
            if vp_type == "relationship_type" or (
                vp_type == ""
                and issue.get("broader", []) == []
                and issue.get("related", []) == []
                and issue.get("datatype", "") in {"", "none"}
            ):
                term_version = approved_versions[term_id]
                term_author_id = merge_user(
                    term_version["metadata"]["created_by_user_id"]
                )
                try:
                    db_relationship_type.import_(
                        vocabulary_id=project_id,
                        user_id=term_author_id,
                        issue_iid=issue_iid,
                        label=term_version["term"]["label"],
                        definition=term_version["term"]["definition"],
                        id_t_global=term_version["metadata"]["id_t_global"],
                        id_t_local=term_version["metadata"]["id_t_local"],
                        id_t_version=term_version["metadata"]["id_t_version"],
                        created_at=term_version["metadata"].get("created_at"),
                    )
                except Exception as e:
                    raise NotImplementedError(e, term_id, issue, term_version)
                # del term_issues[term_id]
            else:
                actual_term_ids.add(term_id)

        broader_dict = {}
        related_dict = {}
        top_level_terms = set()
        latest_term_version = {}
        custom_relationships = {}
        # for term_id, issues_dict in term_issues.items():
        for term_id in actual_term_ids:
            issues_dict = term_issues[term_id]
            files = {}
            if term_id in approved_versions:
                term_version = approved_versions[term_id]
                files[term_version["metadata"]["id_t_version"]] = term_version

            if term_id in latest_branches:
                branch = latest_branches[term_id]
                for filename, term_version in get_term_versions(
                    project_id, term_id, branch[1]["name"]
                ):
                    # may overwrite the approved version, but the data should be the same
                    term_version_id = term_version["metadata"]["id_t_version"]
                    files[term_version_id] = term_version
                    filename_to_term_version_id[filename] = term_version_id

            # use the data from the latest file to create the term node
            term_author_id = merge_user(term_version["metadata"]["created_by_user_id"])
            db_term.import_(
                id=term_id,
                author_id=term_author_id,
                vocabulary_id=project_id,
                created_at=term_version["metadata"]["created_on"],
            )

            is_top_level = False
            latest_term_version_id = None
            issues = sorted(issues_dict.items())
            for i, (issue_iid, issue) in enumerate(issues):
                version_id = issue["id_t_version"]
                if version_id in files:
                    file = files[issue["id_t_version"]]
                    author_id = merge_user(file["metadata"]["edited_by_user_id"])

                    if "issue_iid" in file["metadata"]:
                        assert file["metadata"]["issue_iid"] == issue_iid

                    try:
                        broader = file["term"]["broader"]
                    except KeyError:
                        raise NotImplementedError(
                            file, version_id, term_id, issues_dict
                        )
                    vocpopuli_type = file["term"].get("contextual_type") or file[
                        "term"
                    ].get("vocpopuli_type")
                    related = file["term"]["related"]

                    local_id = file["metadata"]["id_t_local"]
                    label = file["term"]["label"]
                    definition = file["term"]["definition"]
                    synonyms = file["term"]["synonyms"]
                    translations = file["term"].get("translations")
                    unit = file["term"]["unit"]
                    created_at = file["metadata"]["edited_on"]
                    datatype = file["term"]["datatype"]
                    related_external_terms = file["term"]["related_external"]
                    generated_from = file["metadata"].get("generated_from_id_t_version")
                    min_ = file["term"].get("min_") or file["term"].get("min")
                    max_ = file["term"].get("max_") or file["term"].get("max")
                    required = file["term"].get("required", False)
                    custom = file["term"].get("custom_relationships", [])
                else:
                    issue_metadata = issue_metadata_dict[issue_iid]
                    author_id = merge_user(issue_metadata["author"]["id"])

                    broader = issue["broader"]
                    vocpopuli_type = issue["vocpopuli_type"]
                    related = issue["related"]

                    local_id = issue["id_t_local"]
                    label = issue["label"]
                    definition = issue["definition"]
                    synonyms = issue["synonyms"]
                    translations = issue.get("translations")
                    unit = issue["unit"]
                    created_at = issue_metadata["created_at"]
                    datatype = issue["datatype"]
                    related_external_terms = issue["related_external"]
                    generated_from = issue["generated_from"]
                    min_ = issue.get("min_")
                    max_ = issue.get("max_")
                    required = issue.get("required", "").strip().lower() == "true"
                    custom = issue["custom"]

                custom_relationships[version_id] = custom

                # only available on the issue
                purl = issue.get("purl")
                gitlab_skos_url = issue.get("gitlab_skos_url")

                # skip terms that have been deleted
                broader = [b for b in broader if b in term_issues]
                if broader:
                    is_top_level = False
                    vocpopuli_type = None
                    broader_dict[version_id] = broader
                else:
                    is_top_level = True

                if related:
                    related_dict[version_id] = related

                # make sure the VocPopuliType exists
                if vocpopuli_type and vocpopuli_type != "relationship_type":
                    db_vocpopuli_type.merge(project_id, vocpopuli_type)

                db_datatype.merge(project_id, datatype)
                db_term_version.import_(
                    id=version_id,
                    local_id=local_id,
                    issue_iid=issue_iid,
                    term_id=term_id,
                    author_id=author_id,
                    issue_version=i,
                    label=label,
                    definition=definition,
                    synonyms=synonyms,
                    translations=translations,
                    unit=unit,
                    created_at=created_at,
                    datatype=datatype,
                    related_external_terms=related_external_terms,
                    vocpopuli_type=vocpopuli_type,
                    generated_from=generated_from,
                    min_=min_,
                    max_=max_,
                    required=required,
                    purl=purl,
                    gitlab_skos_url=gitlab_skos_url,
                    vocabulary_id=project_id,
                )

                for j, url in enumerate(file["image_files"]):
                    db_file.import_(
                        term_version_id=version_id,
                        url=url,
                        order_id=j,
                    )

                latest_term_version_id = version_id

            latest_term_version[term_id] = latest_term_version_id
            if is_top_level:
                top_level_terms.add(term_id)

        for term_version_id in latest_term_version.values():
            db_term_version.set_latest(term_version_id, project_id)

        for term_id in top_level_terms:
            if vocpopuli_type != "relationship_type":
                db_term.set_as_top_level_term(term_id, project_id)

        for source, targets in broader_dict.items():
            db_term_version.set_broader(source, targets)

        for source, targets in related_dict.items():
            db_term_version.attach_related(source, targets)

        for issue_iid in parsed_issues:
            # create comments
            for comment in get_comments(project_id, issue_iid):
                if comment["system"]:
                    continue

                user_id = merge_user(comment["author"]["id"])
                db_user.comment_on_term_version(
                    user_id, term_version_id, comment["body"]
                )

            # create votes
            for vote in get_votes(project_id, issue_iid):
                user_id = merge_user(vote["user"]["id"])
                vote_name = vote["name"].removeprefix("thumbs")
                db_user.vote_on_term_version(user_id, term_version_id, vote_name)

        # create approvals
        for commit in get_commits(project_id):
            if not commit["title"].startswith("Approved "):
                continue

            filename = commit["title"].split()[1]
            if filename in filename_to_term_version_id:
                # term has been reopened
                term_version_id = filename_to_term_version_id[filename]
            else:
                # term has not been reopened
                term_id = "_".join(filename.split("_")[:3]).removesuffix(".json")
                if term_id not in approved_versions:
                    continue
                term_version_id = approved_versions[term_id]["metadata"]["id_t_version"]

            user_id = get_user_id(commit["committer_name"])
            if db_term_version.get(id=term_version_id,
                                   vocabulary_id=project_id):
                db_user.approve(user_id, term_version_id, project_id)

        for term_version_id in approved_term_version_ids:
            term_version = db_term_version.get(id=term_version_id,
                                               vocabulary_id=project_id)
            if term_version:
                merge_user(term_version["author_id"])
                db_user.approve(term_version["author_id"], term_version_id)

        for term_version_id, custom_dict in custom_relationships.items():
            db_term_version.add_custom_relationships(term_version_id, custom_dict, project_id)

        session["PROJECT_ID"] = project_id
        return redirect(url_for(".list_terms"))

    return render_template(
        "import_existing_vocabulary.html",
        form=form,
        select_project_form=_get_select_project_form(),
    )


@main.route("/import_vocabulary", methods=["GET", "POST"])
@login_required
def import_vocabulary():
    """
    A function used to upload the JSON files of multiple terms, from which a new
    vocabulary is generated.
    """
    form = TermsJSONsUploadForm()

    if form.validate_on_submit():
        # create a new repository, and select it for
        # use by VocPopuli

        user_id = session["USER_ID"]
        repository_name = form.vocab_name.data
        vocabulary_description = form.vocabulary_description.data
        vocabulary_domain = form.vocabulary_domain.data
        institution_name = form.institution_name.data
        institution_ror = form.institution_ror.data
        license_name = form.license_name.data
        license_url = form.license_url.data

        vocabulary_id = session.get("PROJECT_ID")
        if repository_name:
            try:
                repository_response = api.create_new_vocab_repository(
                    repository_name,
                )
            except KeyError:  # 'id' not found in the response
                # TODO: flash an error message
                return redirect(url_for(".import_vocabulary"))

            # store the new repository information in the session cookie
            session["PROJECT_ID"] = str(repository_response["id"])
            vocabulary_id = str(repository_response["id"])
            db_vocabulary.create(id=vocabulary_id,
                                 name=repository_name,
                                 user_id=user_id,
                                 vocabulary_description=vocabulary_description,
                                 vocabulary_domain=vocabulary_domain,
                                 institution_name=institution_name,
                                 institution_ror=institution_ror,
                                 license_name=license_name,
                                 license_url=license_url
                                 )
            db_datatype.create_default_types(vocabulary_id)
            db_vocpopuli_type.create_default_types(vocabulary_id)

        # TODO: make sure the imported data also gets stored in the database

        # store the terms' JSON dicts in memory
        terms_json_dicts = []
        all_repository_labels = {x["label"] for x in get_term_jsons(vocabulary_id)}
        for file in form.files.data:
            if not file.filename.lower().endswith(".json"):
                continue

            json_str = file.read()
            json_dict = json.loads(json_str)
            # If the term is already in the repository -> disregard it
            if json_dict["term"]["label"] in all_repository_labels:
                terms_json_dicts.append(json_dict)

        all_terms_labels = {x["term"]["label"] for x in terms_json_dicts}
        now = datetime.utcnow().isoformat()
        ep = Endpoints()

        for x in terms_json_dicts:
            # validate whether all term relations point to existing terms
            all_related_terms = set(x["term"]["broader"]) | set(x["term"]["related"])
            if missing := all_related_terms - all_terms_labels:
                # TODO: flash an error
                print(
                    f"Term {x['term']['label']} has relations to the following"
                    "terms, which are not in the proposed vocabulary:"
                )
                for term in missing:
                    print(term)

            x["metadata"] = {
                "created_by_user_id": user_id,
                "created_on": now,
                "edited_by_user_id": user_id,
                "edited_on": now,
                "id_t_global": "-".join(
                    term_tag(x["term"]["label"]),
                    generate_uid(),
                    "vp",
                ),
                "id_t_local": f"{generate_uid()}-vp",
                "id_t_version": f"{generate_uid()}-vp",
            }
            x["image_files"] = []
            x["term"]["broader_labels"] = x["term"]["broader"]
            x["term"]["broader"] = []
            x["term"]["related_labels"] = x["term"]["related"]
            x["term"]["related"] = []
            if "related_external" not in x["term"]:
                x["term"]["related_external"] = [""]
            if "vocpopuli_type" not in x["term"]:
                x["term"]["vocpopuli_type"] = None
            if "unit" not in x["term"]:
                x["term"]["unit"] = ""

        label_to_id_t_global = {}
        for term in terms_json_dicts:
            if (label := term["term"]["label"]) not in label_to_id_t_global:
                label_to_id_t_global[label] = term["metadata"]["id_t_global"]

        for x in terms_json_dicts:
            x["term"]["broader"] = [
                label_to_id_t_global[label]
                for label in x["term"]["broader_labels"]
                if label in label_to_id_t_global
            ]
            x["term"]["related"] = [
                label_to_id_t_global[label]
                for label in x["term"]["related_labels"]
                if label in label_to_id_t_global
            ]

            file = json.dumps(x, indent=4)
            file += " \n"  # add a new line at the end of the file

            id_t_global = x["metadata"]["id_t_global"]
            term_label = x["term"]["label"]
            file_name = id_t_global
            # TODO: differentiate between approved/not approved terms
            # use URL-encoding for '/' and '.' when defining
            # the term's file path
            file_path = f"approved_terms%2F{file_name}%2Ejson"

            commit_message = (
                f"VocPopuli import: {term_label}. " f"Global Term ID: {id_t_global}"
            )

            # commit the file in the main branch

            commit_response = api.add_new_file(
                file=file,
                file_path=file_path,
                branch="main",
                commit_message=commit_message,
            )

            if commit_response:
                # generate an issue description
                term_markdown = new_term_to_markdown(x)

                # post a new issue related to the term's definition
                # try multiple times because sometimes the API returns
                # a 500 code on the first try
                issue_response = api.retry(
                    api.post_new_term_issue,
                    title=f"[Initial Version] {term_label} | {id_t_global}",
                    description=term_markdown,
                    labels="new_term, approved",
                )

                if issue_response.ok:
                    # close the issue because the term is approved
                    params = {"state_event": "close"}
                    iid = issue_response.json()["iid"]
                    gitlab.put(f"{ep.ISSUES_EP}/{iid}", params=params)

            else:
                print(term_label, file_name)
        # load_repositories()
        return redirect(url_for(".list_terms"))

    return render_template(
        "import_vocabulary.html",
        form=form,
        select_project_form=_get_select_project_form(),
    )


@main.route("/approve_term/<term_version_id>", methods=["GET"])
@selected_project_required
def approve_term(term_version_id):
    """
    The function used to approve a given term.
    """
    project_id = session["PROJECT_ID"]
    term_version = db_term_version.get(id=term_version_id,
                                       vocabulary_id=project_id)
    user_id = session["USER_ID"]
    project_id = session["PROJECT_ID"]
    
    db_user.approve(id=user_id,
                    term_version_id=term_version["id"],
                    project_id=project_id)

    term_version["created_at"] = term_version["created_at"].isoformat()
    term = db_term.get(term_version["term_id"])
    # git_approve_term.delay(
    #     project_id=project_id,
    #     user_id=user_id,
    #     term_version=term_version,
    #     id_t_global=term["id"],
    #     oauth_token=gitlab.token["access_token"],
    # )

    return redirect(url_for(".view_term", term_version_id=term_version_id))


@main.route("/kadi_export", methods=["GET", "POST"])
@selected_project_required
def kadi_export():
    records = [
        version["label"]
        for version in db_vocabulary.top_level_terms(id=session["PROJECT_ID"])
        if version.get("datatype", "") == "record"
        ]

    form = KadiTemplateExport(possible_templates=records)

    if form.validate_on_submit():
        tree_json = _get_vocab_tree()
        chosen_template_name = form.template.data

        for term in tree_json["children"]:
            if term["name"] == chosen_template_name:
                chosen_record = term

        if not chosen_record["approved"]:
            return render_template(
                "kadi_export.html",
                form=form,
                error="The record-level term needs to be approved before exporting!",
                select_project_form=_get_select_project_form(),
            )
        if not form.title_template.data.startswith(chosen_template_name):
            form.title_template.data = chosen_template_name + " " + form.title_template.data
            return render_template(
                "kadi_export.html",
                form=form,
                error="Title has to start with name of record.",
                select_project_form=_get_select_project_form(),
            )

        datatype_conversion = {
            "dictionary": "dict",
            "string": "str",
            "integer": "int",
            "boolean": "bool",
        }

        def read_layer(layer):
            """
            Converts a VocPopuli tree layer into Kadi4Mat template extras
            """
            # layer is a list of terms
            if isinstance(layer, list):
                result = []
                missing_terms = []
                for term in layer:
                    converted_term = read_layer(term)
                    if isinstance(converted_term, tuple) and converted_term[0] == 'Not approved!':
                        missing_terms += converted_term[1]
                    else:
                        result.append(converted_term)
                if len(missing_terms) > 0:
                    return ("Not approved!", missing_terms)
                return result
            # layer is a key
            elif "name" in layer and "datatype" in layer:

                # term is not a key but an option
                if layer["datatype"] in ["individual", "option"]:
                    if not layer.get("approved", False):
                        return ("Not approved!", [layer['name']])
                    return layer["name"]

                key = to_key(layer)
                if "children" in layer:
                    values = read_layer(layer["children"])
                    if (
                        isinstance(values, list)
                        and len(values) > 0
                        and isinstance(values[0], dict)
                    ):
                        key["value"] = values
                    elif (
                        isinstance(values, list)
                        and len(values) > 0
                        and not isinstance(values[0], list)
                    ):
                        key["validation"] = {"options": values}
                    else:
                        key["value"] = values
                        
                    # term in sub layers was not approved
                    if isinstance(values, tuple) and values[0] == 'Not approved!' and layer.get("approved", False):
                        return ("Not approved!", values[1])
                    # this term and sublayers are not approved
                    elif isinstance(values, tuple) and values[0] == 'Not approved!' and not layer.get("approved", False):
                        return ("Not approved!", values[1] + [layer['name']])
                    
                # check that the term is approved
                if not layer.get("approved", False):
                    return ("Not approved!", [layer['name']])
                
                return key
            else:
                print("incomplete key")
                return None

        def to_key(term_dict: dict) -> dict:
            key_dict = {
                "key": term_dict["name"],
                "type": datatype_conversion.get(term_dict["datatype"], term_dict["datatype"])
            }
            if term_dict.get("id_t_local", None):
                key_dict["term"] = term_dict.get("id_t_local")

            return key_dict

        extras = []
        if chosen_record.get("children"):
            extras = read_layer(chosen_record["children"])
            if isinstance(extras, tuple) and extras[0] == 'Not approved!':
                error_msg = "Make sure all terms in the record are approved! The following terms are not approved:\n "
                error_msg += ', '.join(extras[1])
                return render_template(
                    "kadi_export.html",
                    form=form,
                    error=error_msg,
                    select_project_form=_get_select_project_form(),
                    )

        if (tags := form.tags.data):
            tags = [x.strip() for x in tags.split(",")]
        else:
            tags = []

        tags += [
            tree_json["id_v_global"],
            tree_json["id_v_local"],
            chosen_record["id_t_global"],
            chosen_record["id_t_local"],
        ]

        kadi_template_data = {
            "description": form.description.data,
            "extras": extras,
            "license": "CC-BY-4.0",
            "tags": tags,
            "title": form.title_template.data,
            "type": chosen_record["datatype"],
        }


        if form.pat.data and "submit" in request.form:
            try:
                kadi_template = KadiManager(host=form.host.data, token=form.pat.data).template(
                    identifier=(chosen_record["id_t_local"] + str(uuid.uuid4())),
                    type="record",
                    data=kadi_template_data,
                    create=True,
                    title=form.title_template.data,
                    description=form.description.data,
                )
                kadi_template.add_group_role(group_id=8, role_name="editor")
            except Exception as e:
                return render_template("kadi_export.html", form=form, error=str(e), select_project_form=_get_select_project_form())

            return redirect(url_for("main.list_terms"))
        # if no PAT to Kadi is given the template will be downloaded as a json
        elif "download" in request.form:
            title = form.title_template.data.replace(" ", "_")
            mimetype = "application/json"
            headers = {
                "Content-Disposition": f"attachment;filename={title}_template.json"
            }
            return Response(
                json.dumps(kadi_template_data, indent=4), mimetype=mimetype, headers=headers
            )
        else:
            return render_template("kadi_export.html", form=form, error="Please provide a personal access token to export the template to Kadi4Mat.", select_project_form=_get_select_project_form())

    return render_template("kadi_export.html", form=form, select_project_form=_get_select_project_form())


@main.route("/public_view_term/<id_v_global>/<id_t_global>", methods=["GET"])
def public_view_term(id_v_global, id_t_global):
    raise NotImplementedError
    # get https://gitlab.com/projects/41151548/ -> get 'location' -> use location for raw file
    id_v_global = str(id_v_global)
    id_t_global = str(id_t_global)
    # id_v_global has the form: xxxxxxx-<project_id>-vp
    project_id = id_v_global.split("-")[1]
    project_id = project_id.split("-")[0]

    issues_endpoint = f"https://gitlab.com/api/v4/projects/{project_id}/issues/"
    search_params = {
        "private_token": os.environ.get("VOCPOPULI_TOKEN"),
        "search": id_t_global,  # only issues containing id_t_global
        "order_by": "created_at",
        "sort": "desc",
        "per_page": 100,
    }
    issues_response = requests.get(f"{issues_endpoint}", params=search_params)
    issues_dict_list = issues_response.json()

    # handle pagination
    for page in range(2, 1 + int(issues_response.headers["X-Total-Pages"])):
        search_params["page"] = page
        next_page = requests.get(f"{issues_endpoint}", params=search_params)
        issues_dict_list += next_page.json()

    # double-check
    issues_dict_list = [x for x in issues_dict_list if id_t_global in x["title"]]

    # get approval status
    is_approved = any(["approved" in x["labels"] for x in issues_dict_list])

    if is_approved:
        issue = [x for x in issues_dict_list if "approved" in x["labels"]][0]
    else:
        issue = issues_dict_list[0]  # get latest issue

    term_definition = issue["description"]
    term_definition = parse_issue_description(term_definition=term_definition)
    term_definition.pop("label")
    term_definition.pop("broader_labels")
    term_definition.pop("related_labels")
    term_definition = {
        k if k != "broader" else "Part of": v for k, v in term_definition.items()
    }
    term_definition = {
        k if k != "related_external" else "Related (external)": v
        for k, v in term_definition.items()
    }
    term_definition["data type"] = term_definition.pop("datatype")
    term_definition["vocpopuli type"] = term_definition.pop("vocpopuli_type")
    term_definition["unit"] = term_definition.pop("unit")

    data = {}
    data["title"] = issue["title"].split(" | ")[0]
    data["iid"] = issue["iid"]
    data["definition"] = term_definition

    return render_template(
        "public_view_term.html",
        data=data,
        # TODO: replace dtype_ with datatypes from the database
        dtype_color_map=dtype_color_map,
        dtype_icon_map=dtype_icon_map,
    )


@main.route("/publish_vocabulary", methods=["GET", "POST"])
@selected_project_required
def publish_vocabulary():
    raise NotImplementedError
    project_id = session["PROJECT_ID"]
    project_name = db_vocabulary.get(project_id)["name"]

    namespaces_resp = gitlab.get("namespaces")
    namespaces_json = namespaces_resp.json()
    namespaces = [(x["id"], x["name"]) for x in namespaces_json]

    form = PublishVocabularyForm(namespaces=namespaces)

    if form.validate_on_submit():
        ep = Endpoints()

        # Format the vocabulary tag
        vocab_tag = form.vocabulary_tag.data.lower()
        vocab_tag = "".join(x for x in vocab_tag if x.isalnum())

        if len(vocab_tag) >= 7:
            vocab_tag = vocab_tag[:7]
        else:
            rest = 7 - len(vocab_tag)
            vocab_tag = vocab_tag + "x" * rest

        # Get all vocabulary terms' JSONs
        all_jsons = get_term_jsons()
        # Generate the Vocabulary IDs
        id_t_locals = [x.get("id_t_local", "") for x in all_jsons if x["approved"]]
        id_t_locals = sorted([x for x in id_t_locals if x != ""])
        id_t_locals_str = ", ".join(x for x in id_t_locals)
        n_approved_str = f"{len(id_t_locals):06}"

        id_t_versions = sorted([x.get("id_t_version", "") for x in all_jsons])
        id_t_versions_str = ", ".join(x for x in id_t_versions)

        if id_t_locals_str != "":
            id_v_local = (
                n_approved_str
                + "-"
                + (hashlib.md5(id_t_locals_str.encode("utf-8")).hexdigest())
                + "-vp"
            )
        else:
            id_v_local = n_approved_str + "-" + "n/a" + "-vp"

        id_v_version = (
            hashlib.md5(id_t_versions_str.encode("utf-8")).hexdigest()
        ) + "-vp"
        id_v_global = vocab_tag + "-" + str(session.get("PROJECT_ID", "n/a")) + "-vp"

        desc = (
            f"This is the published version of the {project_name} vocabulary.  \n"
            f"The file `metadata.json` contains the vocabulary's metadata as per VocPopuli's schema (in progress).  \n"
            f" \n "
            f"The vocabulary was created using [https://vocpopuli.com](https://vocpopuli.com).  \n"
        )

        namespace_id = form.namespace.data
        serializer_namespace = "https://purls.helmholtz-metadaten.de/vp/"

        # Try to publish the vocabulary in the specified namespace
        publish_response = gitlab.post(
            "projects",
            data={
                "name": f"Vocabulary {project_name}",
                "namespace_id": namespace_id,
                "description": desc,
                "initialize_with_readme": False,
                "default_branch": "main",
                "remove_source_branch_after_merge": False,
                "visibility": "public",
            },
        )

        if (
            publish_response.json().get("message", {}).get("path", ["na"])[0]
            == "has already been taken"
        ):
            user_repos = gitlab.get("projects", params={"membership": True}).json()
            repos_in_ns = [
                x for x in user_repos if int(x["namespace"]["id"]) == int(namespace_id)
            ]
            repo = [
                x for x in repos_in_ns if x["name"] == f"Vocabulary {project_name}"
            ][0]
            repo_id = repo["id"]
            created_at = repo["created_at"]
            new_repo = False
            web_url = repo["web_url"]
        else:
            repo_id = publish_response.json()["id"]
            created_at = publish_response.json()["created_at"]
            new_repo = True
            web_url = publish_response.json()["web_url"]

        # Generate vocabulary metadata:
        repo_info = gitlab.get(ep.PROJECT_EP, params={"license": True}).json()
        publisher = db_user(session["USER_ID"])["name"]
        license = repo_info["license"]
        contributors = gitlab.get(f"{ep.REPO_EP}/contributors").json()

        metadata_dict = {
            "VOCABULARY_NAME": project_name,
            "VOCABULARY_PURL": f"{serializer_namespace}{id_v_global}",
            "VOCPOPULI_GLOBAL_ID": id_v_global,
            "VOCPOPULI_LOCAL_ID": id_v_local,
            "VOCPOPULI_VERSION_ID": id_v_version,
            "VOCABULARY_PUBLISHER": publisher,
            "VOCABULARY_PUBLISHED_ON_UTC": created_at,
            "VOCABULARY_UPDATED_ON_UTC": datetime.utcnow().isoformat(),
            "VOCABULARY_LICENSE": license,
            "VOCABUALRY_CONTRIBUTORS": contributors,
        }

        metadata_str = json.dumps(metadata_dict, indent=4)
        # Serialize the vocabulary using JSON-LD
        serializer = SKOSSerializer(
            namespace=serializer_namespace,
            term_jsons=all_jsons,
            id_v_global=id_v_global,
            vocab_name=project_name,
            publisher=metadata_dict["VOCABULARY_PUBLISHER"],
            created=metadata_dict["VOCABULARY_PUBLISHED_ON_UTC"],
            license_url=metadata_dict["VOCABULARY_LICENSE"]["html_url"],
            contributor_names=[
                x["name"] for x in metadata_dict["VOCABUALRY_CONTRIBUTORS"]
            ],
        )

        serialization = serializer.serialize_vocabulary(format="ttl")
        jsonld_serialization = serializer.serialize_vocabulary(format="json-ld")
        # Add a description of the vocabulary

        serialization_ep = (
            f"projects/{repo_id}/"
            f"repository/files/SKOS_{project_name.replace(' ', '_')}%2Ettl"
        )
        jsonld_serialization_ep = (
            f"projects/{repo_id}/"
            f"repository/files/SKOS_{project_name.replace(' ', '_')}%2Ejsonld"
        )
        readme_ep = f"projects/{repo_id}/" f"repository/files/README%2Emd"
        license_ep = f"projects/{repo_id}/" f"repository/files/LICENSE"
        metadata_ep = f"projects/{repo_id}/" f"repository/files/metadata%2Ejson"

        term_update_lists = []

        if new_repo:
            # Single term serializations:
            for x in all_jsons:
                term_serializazion = SKOSSerializer(
                    term_jsons=[x], id_v_global=id_v_global
                ).serialize_term(format="ttl")
                term_ep = (
                    f"projects/{repo_id}/"
                    f"repository/files/terms%2F{x['id_t_global']}%2Ettl"
                )
                term_post_resp = gitlab.post(
                    term_ep,
                    data={
                        "branch": "main",
                        "content": term_serializazion,
                        "commit_message": f"Add {x['id_t_global']}.ttl",
                    },
                )
                if term_post_resp.ok:
                    term_update_lists.append(
                        {
                            "issue_iid": x["issue_iid"],
                            "purl": f"{serializer.namespace}{id_v_global}/{x['id_t_global']}",
                            "gitlab_raw_url": f"{web_url}/-/raw/main/terms/{x['id_t_global']}.ttl",
                        }
                    )

            gitlab.post(
                readme_ep,
                data={
                    "branch": "main",
                    "content": desc,
                    "commit_message": f"Add README.md",
                },
            )
            gitlab.post(
                serialization_ep,
                data={
                    "branch": "main",
                    "content": serialization,
                    "commit_message": f"Add SKOS_{project_name.replace(' ', '_')}.ttl",
                },
            )
            gitlab.post(
                jsonld_serialization_ep,
                data={
                    "branch": "main",
                    "content": jsonld_serialization,
                    "commit_message": f"Add SKOS_{project_name.replace(' ', '_')}.jsonld",
                },
            )
            gitlab.post(
                metadata_ep,
                data={
                    "branch": "main",
                    "content": metadata_str,
                    "commit_message": "Add metadata.json",
                },
            )
            gitlab.post(
                license_ep,
                data={
                    "branch": "main",
                    "content": CC_BY_40,
                    "commit_message": "Add LICENSE",
                },
            )
            action = "created"
        else:
            commit_message = f"Update the {project_name} vocabulary."
            # Single term serializations:
            for x in all_jsons:
                term_serializazion = SKOSSerializer(
                    term_jsons=[x], id_v_global=id_v_global
                ).serialize_term(format="ttl")
                term_ep = (
                    f"projects/{repo_id}/"
                    f"repository/files/terms%2F{x['id_t_global']}%2Ettl"
                )
                try:
                    term_post_resp = gitlab.post(
                        term_ep,
                        data={
                            "branch": "main",
                            "content": term_serializazion,
                            "commit_message": commit_message,
                        },
                    )
                except:
                    term_post_resp = gitlab.put(
                        term_ep,
                        data={
                            "branch": "main",
                            "content": term_serializazion,
                            "commit_message": commit_message,
                        },
                    )
                term_update_lists.append(
                    {
                        "issue_iid": x["issue_iid"],
                        "purl": f"{serializer.namespace}{id_v_global}/{x['id_t_global']}",
                        "gitlab_raw_url": f"{web_url}/-/raw/main/terms/{x['id_t_global']}.ttl",
                    }
                )

            gitlab.put(
                readme_ep,
                data={
                    "branch": "main",
                    "content": desc,
                    "commit_message": commit_message,
                },
            )
            gitlab.put(
                serialization_ep,
                data={
                    "branch": "main",
                    "content": serialization,
                    "commit_message": commit_message,
                },
            )
            gitlab.put(
                jsonld_serialization_ep,
                data={
                    "branch": "main",
                    "content": jsonld_serialization,
                    "commit_message": commit_message,
                },
            )
            gitlab.put(
                metadata_ep,
                data={
                    "branch": "main",
                    "content": metadata_str,
                    "commit_message": commit_message,
                },
            )

            action = "updated"
        _update_issues(term_update_lists)
        vocab_url = web_url
        return render_template(
            "publish_success.html", vocab_url=vocab_url, action=action
        )

    return render_template("publish_vocabulary.html", form=form)


def _update_issues(term_update_list):
    raise NotImplementedError
    ep = Endpoints()
    for term in term_update_list:
        issue_iid = term["issue_iid"]
        purl = term["purl"]
        gitlab_raw_url = term["gitlab_raw_url"]

        issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}").json()
        issue_desc = issue["description"]
        if "<p><strong>PURL:</strong> " not in issue_desc:
            issue_desc += f"<p><strong>PURL:</strong> {purl}</p>"
        else:
            purl_start_idx = issue_desc.find("<p><strong>PURL:</strong> ") + len(
                "<p><strong>PURL:</strong> "
            )
            purl_end_idx = issue_desc.find("</p>", purl_start_idx)
            purl_old = issue_desc[purl_start_idx:purl_end_idx]
            issue_desc.replace(purl_old, purl)

        if "<p><strong>GitLab SKOS URL:</strong> " not in issue_desc:
            issue_desc += f"<p><strong>GitLab SKOS URL:</strong> {gitlab_raw_url}</p>"
        else:
            url_start_idx = issue_desc.find(
                "<p><strong>GitLab SKOS URL:</strong> "
            ) + len("<p><strong>GitLab SKOS URL:</strong> ")
            url_end_idx = issue_desc.find("</p>", url_start_idx)
            url_old = issue_desc[url_start_idx:url_end_idx]
            issue_desc = issue_desc.replace(url_old, gitlab_raw_url)

        resp = gitlab.put(
            f"{ep.ISSUES_EP}/{issue_iid}", params={"description": issue_desc}
        )
        if not resp.ok:
            resp = gitlab.put(
                f"{ep.ISSUES_EP}/{issue_iid}", params={"description": issue_desc}
            )
        if not resp.ok:
            resp = gitlab.put(
                f"{ep.ISSUES_EP}/{issue_iid}", params={"description": issue_desc}
            )


@main.route("/request_vocabulary", methods=["GET"])
def request_vocabulary():
    return render_template("request_vocabulary.html",
                           select_project_form=_get_select_project_form(),)


@main.route("/files/<file_id>", methods=["GET"])
def serve_file(file_id: str):
    # TODO: improve security
    file = db_file.get(file_id)

    if not file["local"]:
        redirect(file["url"])

    return send_file(file["url"])


@main.route("/load_repositories")
def force_load_repositories():
    load_repositories()
    return redirect(url_for(".index"))


@main.route("/relationship_types", methods=["GET", "POST"])
@selected_project_required
def relationship_types():
    project_id = session["PROJECT_ID"]
    types = db_vocabulary.get_relationship_types_order_and_instances(id=project_id)

    create_relationship_type_form = CreateRelationshipTypeForm()
    if create_relationship_type_form.validate_on_submit():
        if create_relationship_type_form.label.data in [t["label"] for t in types]:
            flash("Relationships must have unique names.")
        else:
            relationship_type = db_relationship_type.create(
                vocabulary_id=session["PROJECT_ID"],
                user_id=session["USER_ID"],
                label=create_relationship_type_form.label.data,
                definition=create_relationship_type_form.definition.data,
                related_iri=create_relationship_type_form.related_iri.data,
            )
            # git_new_relationship_type.delay(
            #     user_id=session["USER_ID"],
            #     type_id=relationship_type["id_t_global"],
            #     vocabulary_name=db_vocabulary.get(project_id)["name"],
            #     project_id=project_id,
            #     oauth_token=gitlab.token["access_token"],
            # )
            return redirect(url_for(".relationship_types"))

    return render_template(
        "relationship_types.html",
        types=types,
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        select_project_form=_get_select_project_form(),
        create_relationship_type_form=create_relationship_type_form,
    )


@main.route("/remove_relationship_type/<type_id>", methods=["POST"])
@selected_project_required
def remove_relationship_type(type_id: str):
    db_relationship_type.remove(id=type_id)
    return redirect(url_for(".relationship_types"))


@main.route("/edit_relationship_type", methods=["POST"])
@selected_project_required
def edit_relationship_type():
    
    relationship_id = request.form.get("relationship_id")
    definition = request.form.get("definition")
    related_iri = request.form.get("related_iri")

    db_relationship_type.edit(
        vocabulary_id=session["PROJECT_ID"],
        relationship_id=relationship_id,
        definition=definition,
        related_iri=related_iri,
    )

    return redirect(url_for(".relationship_types"))


@main.route("/reorder_relationshiptypes", methods=["POST"])
@selected_project_required
def reorder_relationshiptypes():
    """
    Function used to order the datatypes for displaying.
    """
    db_relationship_type.reorder(
        id=session["PROJECT_ID"],
        relationship_types=json.loads(request.form.get("new_order_input")),
    )
    return redirect(url_for(".relationship_types"))


@main.route("/vocpopuli_types", methods=["GET", "POST"])
@selected_project_required
def vocpopuli_types():
    project_id = session["PROJECT_ID"]
    vocpopuli_types = db_vocpopuli_type.all_with_instances(project_id)

    create_vocpopuli_type_form = CreateVocPopuliTypeForm()
    if create_vocpopuli_type_form.validate_on_submit():
        if create_vocpopuli_type_form.name.data in [t["name"] for t in vocpopuli_types]:
            flash("VocPopuli Types must have unique names.")
        else:
            color = determine_color_vocpopuli_type(create_vocpopuli_type_form.name.data)
            db_vocpopuli_type.create(
                vocabulary_id=project_id,
                name=create_vocpopuli_type_form.name.data,
            )
            return redirect(url_for(".vocpopuli_types"))

    return render_template(
        "vocpopuli_types.html",
        vocpopuli_types=vocpopuli_types,
        select_project_form=_get_select_project_form(),
        create_vocpopuli_type_form=create_vocpopuli_type_form,
    )


@main.route("/remove_vocpopuli_type/<type_name>", methods=["POST"])
@selected_project_required
def remove_vocpopuli_type(type_name: str):
    db_vocpopuli_type.delete(vocabulary_id=session["PROJECT_ID"], name=type_name)
    return redirect(url_for(".vocpopuli_types"))


@main.route("/edit_vocpopuli_type", methods=["POST"])
@selected_project_required
def edit_vocpopuli_type():
    name = request.form.get("name")
    old_name = request.form.get("old_name")
    project_id = session["PROJECT_ID"]

    if (
        name == ""
        or name is None
        or (
            name != old_name
            and name
            in [t["name"] for t in db_vocpopuli_type.all_with_instances(project_id)]
        )
    ):
        flash("Label of a VocPopuli Type can not be empty and must be unique.")

    else:
        db_vocpopuli_type.edit(vocabulary_id=project_id, name=name, old_name=old_name)

    return redirect(url_for(".vocpopuli_types"))


@main.route("/data_types", methods=["GET", "POST"])
@selected_project_required
def data_types():
    project_id = session["PROJECT_ID"]
    data_types = db_datatype.get_all_with_order_and_instances(project_id)
    placeholder_self = "<<itself>>"
    create_data_type_form = CreateDataTypeForm(all_types=[placeholder_self]+[t["name"] for t in data_types])
   
    if create_data_type_form.validate_on_submit():
        if create_data_type_form.name.data in [t["name"] for t in data_types]:
            flash("Datatype Names have to be unique.")
        else:
            db_datatype.create(
                vocabulary_id=project_id,
                name=create_data_type_form.name.data,
                color=create_data_type_form.color.data or "gray",
                icon=create_data_type_form.icon.data or "bi-question-diamond",
                numeric=create_data_type_form.numeric.data or False,
                toplevel=create_data_type_form.toplevel.data or False,
                parents = [create_data_type_form.name.data if item == placeholder_self else item for item in create_data_type_form.parent_types.data],
                help_text=create_data_type_form.help_text.data or "",
            )
            return redirect(url_for(".data_types"))

    # json of parents for the javascript
    for t in data_types:
        t.update({"parents_json": json.dumps(t.get("parent_types", []))}) 
        
    return render_template(
        "data_types.html",
        data_types=data_types,
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        select_project_form=_get_select_project_form(),
        create_data_type_form=create_data_type_form,
    )


@main.route("/delete_datatype/<datatype>", methods=["GET", "POST"])
@selected_project_required
def delete_datatype(datatype):
    """
    Function used to delete an unused datatype.
    """
    db_datatype.delete(vocabulary_id=session["PROJECT_ID"], datatype=datatype)
    return redirect(url_for(".data_types"))


@main.route("/edit_datatype", methods=["POST"])
@selected_project_required
def edit_datatype():
    name = request.form.get("name")
    old_name = request.form.get("old_name")
    numeric = "numeric" in request.form
    color = request.form.get("color")
    help_text = request.form.get("help_text")
    icon = request.form.get("icon")
    toplevel = "toplevel" in request.form
    parents = request.form.getlist("parent_types")
    project_id = session["PROJECT_ID"]

    if (
        name == ""
        or name is None
        or (
            name != old_name
            and name in [t["name"] for t in db_datatype.get_all(project_id)]
        )
    ):
        flash("Name of a Datatype must be unique.")

    else:
        db_datatype.edit(
            vocabulary_id=project_id,
            name=name,
            old_name=old_name,
            numeric=numeric,
            color=color,
            help_text=help_text,
            toplevel=toplevel,
            parents=parents,
            icon=icon,
        )

    return redirect(url_for(".data_types"))


@main.route("/reorder_datatypes", methods=["GET", "POST"])
@selected_project_required
def reorder_datatypes():
    """
    Function used to order the datatypes for displaying.
    """
    db_datatype.reorder(
        vocabulary_id=session["PROJECT_ID"],
        datatypes=json.loads(request.form.get("new_order_input")),
    )
    return redirect(url_for(".data_types"))


@main.route("/archive_term/<term_version_id>", methods=["GET"])
@selected_project_required
def archive_term(term_version_id):
    """
    The function used to archive a given term.
    """
    user_id = session["USER_ID"]
    term_version = db_term_version.get(id=term_version_id,
                                       vocabulary_id=session["PROJECT_ID"])
    broader_terms_ids = term_version["broader"]
    child_terms = db_term.get_latest_children(
        id=term_version["term_id"],
        vocabulary_id=session["PROJECT_ID"]
        )

    # get children terms, and detach them
    for child_term in child_terms:
        child_term_version = db_term_version.get(id=child_term["id"],
                                                 vocabulary_id=session["PROJECT_ID"])
        custom_relationships = db_term_version.get_custom_relationships(
            id=child_term["id"],
            vocabulary_id=session["PROJECT_ID"],
        )

        new_child_term_parents_ = [
            id_t_version
            for id_t_version in child_term_version["broader"]
            if id_t_version != term_version_id
        ]
        new_child_term_parents = []
        for parent in new_child_term_parents_:
            parent_term_id = db_term_version.get(id=parent,
                                                 vocabulary_id=session["PROJECT_ID"])["term_id"]
            new_child_term_parents.append(parent_term_id)

        new_child_term_related_ = [
            id_t_version
            for id_t_version in child_term_version["related"]
            if id_t_version != term_version_id
        ]
        new_child_term_related = []
        for related in new_child_term_related_:
            related_term_id = db_term_version.get(id=related,
                                                  vocabulary_id=session["PROJECT_ID"])["term_id"]
            new_child_term_related.append(related_term_id)

        if not new_child_term_parents:
            new_child_term_vp_type = "detached"
        else:
            new_child_term_vp_type = child_term_version["vocpopuli_type"]

        new_child_term_version = db_term_version.create(
            id=f"{generate_uid()}-vp",
            term_id=child_term_version["term_id"],
            user_id=user_id,
            label=child_term_version["label"],
            definition=child_term_version["definition"],
            reference=term_version.get("reference", ""),
            synonyms=child_term_version["synonyms"],
            translations=child_term_version.get("translations"),
            unit=child_term_version["unit"],
            datatype=child_term_version["datatype"],
            broader=new_child_term_parents,
            related=new_child_term_related,
            related_external_terms=child_term_version.get("related_external"),
            vocpopuli_type=new_child_term_vp_type,
            min_=child_term_version.get("min_"),
            max_=child_term_version.get("max_"),
            required=child_term_version["required"],
            vocabulary_id=session["PROJECT_ID"],
            generated_from=term_version_id
        )
        db_term_version.add_custom_relationships(
            id=new_child_term_version["id"],
            relationships=custom_relationships,
            vocabulary_id=session["PROJECT_ID"]
        )

    # create new latest term version which has no parents,
    # no related terms, and no custom relationships
    db_term_version.create(
        id=f"{generate_uid()}-vp",
        term_id=term_version["term_id"],
        user_id=user_id,
        label=term_version["label"],
        definition=term_version["definition"],
        reference=term_version.get("reference", ""),
        synonyms=term_version["synonyms"],
        translations=term_version.get("translations"),
        unit=term_version["unit"],
        datatype=term_version["datatype"],
        broader=[],  # detach from parents
        related=[],  # no related terms
        related_external_terms=term_version.get("related_external"),
        vocpopuli_type=term_version["vocpopuli_type"],
        min_=term_version.get("min_"),
        max_=term_version.get("max_"),
        required=term_version["required"],
        vocabulary_id=session["PROJECT_ID"],
        generated_from=term_version["id"]
    )

    # copy the parents' TermVersion nodes, making them unapproved
    for broader_term_id in broader_terms_ids:
        broader_term_version = db_term_version.get(id=broader_term_id,
                                                   vocabulary_id=session["PROJECT_ID"])
        custom_relationships = db_term_version.get_custom_relationships(
            id=broader_term_id,
            vocabulary_id=session["PROJECT_ID"]
        )

        new_broader_term_parents_ = [
            id_t_version
            for id_t_version in broader_term_version["broader"]
            if id_t_version != term_version_id
        ]
        new_broader_term_parents = []
        for parent in new_broader_term_parents_:
            parent_term_id = db_term_version.get(id=parent,
                                                 vocabulary_id=session["PROJECT_ID"])["term_id"]
            new_broader_term_parents.append(parent_term_id)

        new_broader_term_related_ = [
            id_t_version
            for id_t_version in broader_term_version["related"]
            if id_t_version != term_version_id
        ]
        new_broader_term_related = []
        for related in new_broader_term_related_:
            related_term_id = db_term_version.get(id=related,
                                                  vocabulary_id=session["PROJECT_ID"])["term_id"]
            new_broader_term_related.append(related_term_id)

        new_broader_term_version = db_term_version.create(
            id=f"{generate_uid()}-vp",
            term_id=broader_term_version["term_id"],
            user_id=user_id,
            label=broader_term_version["label"],
            definition=broader_term_version["definition"],
            reference=broader_term_version.get("reference", ""),
            synonyms=broader_term_version["synonyms"],
            translations=broader_term_version.get("translations"),
            unit=broader_term_version["unit"],
            datatype=broader_term_version["datatype"],
            broader=new_broader_term_parents,
            related=new_broader_term_related,
            related_external_terms=broader_term_version.get("related_external"),
            vocpopuli_type=broader_term_version["vocpopuli_type"],
            min_=broader_term_version.get("min_"),
            max_=broader_term_version.get("max_"),
            required=broader_term_version["required"],
            vocabulary_id=session["PROJECT_ID"],
            generated_from=term_version["id"]
        )
        db_term_version.add_custom_relationships(
            id=new_broader_term_version["id"],
            relationships=custom_relationships,
            vocabulary_id=session["PROJECT_ID"],
        )

    # set as archived
    db_user.set_as_archived(id=user_id,
                            term_id=term_version["term_id"],
                            project_id=session["PROJECT_ID"])
    return redirect(url_for(".list_terms"))


@main.route("/delete_term/<term_version_id>", methods=["GET"])
@selected_project_required
def delete_term(term_version_id):
    term_version = db_term_version.get(id=term_version_id,
                                       vocabulary_id=session["PROJECT_ID"])
    if db_term.is_archived(term_version["term_id"], session["PROJECT_ID"]):
        db_term.delete_detach(term_version["term_id"], session["PROJECT_ID"])

    return redirect(url_for(".list_terms"))

@main.route("/export_to_bank", methods=["GET", "POST"])
@selected_project_required
def export_to_bank():
    
    form = SelectProjectForm(project_id=session.get("PROJECT_ID"), user_id=session["USER_ID"])
    
    if form.validate_on_submit():
        vocabulary = db_vocabulary.get(request.form.get("project"))
        # central nodes
        all_terms = db_vocabulary.all_terms(vocabulary['id'])
        all_latest_versions = db_term_version.all_latest_versions(vocabulary_id=vocabulary['id'])
        unapproved = [x["label"] for x in all_latest_versions if not x.get("is_approved", False)]
        if unapproved:
            return redirect(url_for(".error_bank_export"))
    
        db_vocabulary.remove_terms_db_(id=vocabulary["id"])
        db_vocabulary.import_terms_db_(id=vocabulary["id"],
                                    name=vocabulary["name"],
                                    description=vocabulary.get("description"),
                                    domain=vocabulary.get("domain"),
                                    institution=vocabulary.get("institution"),
                                    institution_ror=vocabulary.get("institution_ror"),
                                    license_name=vocabulary.get("license_name"),
                                    license_url=vocabulary.get("license_url"),
                                    )
        all_vp_types = db_vocpopuli_type.all(vocabulary["id"])
        all_datatypes = db_datatype.get_all(vocabulary["id"])
        for vp_type in all_vp_types:
            db_vocpopuli_type.import_terms_db_(vp_type["name"], vp_type["color"])
        for dtype in all_datatypes:
            db_datatype.import_terms_db_(name=dtype["name"],
                                    color=dtype["color"],
                                    icon=dtype["icon"],
                                    numeric=dtype["numeric"],
                                    help_text=dtype["help_text"])
        for x in all_terms:
            db_term.import_terms_db_(id=x["id"])
        for v in all_latest_versions:
            db_term_version.import_terms_db_(
                vocabulary_id=vocabulary["id"],
                id=v["id"],
                id_t_local=v.get("id_t_local"),
                term_id=v["term_id"],
                label=v["label"],
                definition=v["definition"],
                translations=v.get("translations", []),
                synonyms=v.get("synonyms", []),
                unit=v.get("unit"),
                created_at=v["created_at"],
                datatype=v["datatype"],
                related_external_terms=v.get("related_external_terms", []),
                min_=v.get("min_"),
                max_=v.get("max_"),
                required=v.get("required"),
                purl=v.get("purl"),
                gitlab_skos_url=v.get("gitlab_skos_url"),
                vocpopuli_type=v.get("vocpopuli_type")
            )

        for v in all_latest_versions:
            broader_version_ids = []
            broader_global_ids = v["broader"]
            if broader_global_ids:
                for broader_global_id in broader_global_ids:
                        broader_version_ids.append([x["id"] for x in all_latest_versions
                                            if x["term_id"] == broader_global_id][0])
            db_term_version.attach_broader_terms_db_(id=v["id"],
                                                    broader=broader_version_ids,
                                                    vocabulary_id=vocabulary["id"])
        for v in all_latest_versions:
            try:
                related_version_ids = []
                related_global_ids = v["related"]
                if isinstance(related_global_ids, str) and related_global_ids:
                    related_global_id = related_global_ids
                    related_version_ids.append([x["id"] for x in all_latest_versions
                                        if x["term_id"] == related_global_id][0])
                elif isinstance(related_global_ids, list) and related_global_ids:
                    for related_global_id in related_global_ids:
                            related_version_ids.append([x["id"] for x in all_latest_versions
                                                if x["term_id"] == related_global_id][0])
                else:
                    continue
                db_term_version.attach_related_terms_db_(id=v["id"],
                                                        related=related_version_ids,
                                                        vocabulary_id=vocabulary["id"])
            except:
                print(v)
                continue
        all_custom_relationships = db_relationship_type.get_all_with_relationships(vocabulary['id'])
        for relationship_type in all_custom_relationships:
            db_relationship_type.import_terms_db_(id_t_global=relationship_type["id_t_global"],
                                                id_t_local=relationship_type["id_t_local"],
                                                id_t_version=relationship_type["id_t_version"],
                                                label=relationship_type["label"],
                                                definition=relationship_type["definition"])
            for (version_id, term_id, relationship_id) in zip(relationship_type["version_id"],
                                                            relationship_type["term_id"],
                                                            relationship_type["relationship_id"]):
                db_relationship_type.import_relationships_terms_db_(vocabulary_id=vocabulary['id'],
                                                                    version_id=version_id,
                                                                    term_id=term_id,
                                                                    id_t_global=relationship_type["id_t_global"],
                                                                    relationship_id=relationship_id)
            
        return redirect(url_for(".index"))
    
    return render_template(
        "export_bank.html",
        select_vocabulary_form=form,
    )


@main.route("/visualize_bank_vocabulary/<vocabulary_id>", methods=["GET", "POST"])
def visualize_bank_vocabulary(vocabulary_id):
    """
    The view function used to visualize the structure of
    the vocabulary in the 'List terms' view.
    """
    project_name = db_vocabulary.get_from_bank(vocabulary_id)["name"]
    tree_dict = term_tree_from_bank(vocabulary_id)
    return render_template(
        "list_terms_bank.html",
        vocabulary_id=vocabulary_id,
        project_name=project_name,
        tree_dict=tree_dict,
        datatypes_dict=db_datatype.get_all_dict_bank(vocabulary_id),
        vocpopuli_types=db_vocpopuli_type.all_bank(vocabulary_id),
        select_project_form=_get_select_project_form(),
        error_message=""
    )


@main.route("/import_existing_vocabulary_from_bank/<vocabulary_id>", methods=["GET", "POST"])
def import_existing_vocabulary_from_bank(vocabulary_id):
    user_id = session["USER_ID"]
    project_id = vocabulary_id
    vocabulary = db_vocabulary.get_from_bank(vocabulary_id)
    local_vocabulary = db_vocabulary.get(id=vocabulary_id)

    if local_vocabulary:
        project_name = db_vocabulary.get_from_bank(vocabulary_id)["name"]
        tree_dict = term_tree_from_bank(vocabulary_id)
        return render_template(
            "list_terms_bank.html",
            vocabulary_id=vocabulary_id,
            project_name=project_name,
            tree_dict=tree_dict,
            datatypes_dict=db_datatype.get_all_dict_bank(vocabulary_id),
            vocpopuli_types=db_vocpopuli_type.all_bank(vocabulary_id),
            select_project_form=_get_select_project_form(),
            error_message="Cannot import the selected vocabulary! It already exists in your VocPopuli instance!",
    )
        
    author_id = merge_user(user_id)
    db_vocabulary.create(project_id, 
                         vocabulary["name"], 
                         user_id, 
                         vocabulary_description=vocabulary.get('description', ''),
                         vocabulary_domain=vocabulary.get('domain', ''),
                         institution_name=vocabulary.get('institution', ''),
                         institution_ror=vocabulary.get('institution_ror', ''),
                         license_name=vocabulary.get('license_name', ''),
                         license_url=vocabulary.get('license_url', ''))
    db_datatype.create_default_types(project_id)
    db_vocpopuli_type.create_default_types(project_id)

    bank_terms = db_term.all_bank(vocabulary_id=vocabulary_id)
    bank_term_versions = db_term_version.get_all_terms_db(vocabulary_id=vocabulary_id)
    top_level_terms = db_vocabulary.top_level_ids_from_bank(id=vocabulary_id)
    top_level_terms_ids = []
    for vp_type, ids in top_level_terms.items():
        top_level_terms_ids += ids[1]
    top_level_terms_ids = list(set(top_level_terms_ids))

    for term in bank_terms:
        # use the data from the latest file to create the term node
        db_term.import_(
            id=term["id"],
            author_id=author_id,
            vocabulary_id=project_id,
            created_at=term["created_at"],
        )
        term_version = [x for x in bank_term_versions
                        if x["term_version"]["term_id"] == term["id"]]
        term_version = term_version[0]["term_version"]

        if term["id"] in top_level_terms_ids:
            is_top_level = True
        else:
            is_top_level = False

        latest_term_version_id = term_version["id"]
        vocpopuli_type = term_version["vocpopuli_type"]
        datatype = term_version["datatype"]
        # make sure the VocPopuliType exists
        if vocpopuli_type and vocpopuli_type != "relationship_type":
            db_vocpopuli_type.merge(project_id, vocpopuli_type)
        if datatype:
            db_datatype.merge(project_id, datatype)

        db_term_version.import_(
            id=term_version["id"],
            local_id=term_version.get("id_t_local"),
            issue_iid=-1,
            term_id=term["id"],
            author_id=author_id,
            issue_version=1,
            label=term_version["label"],
            definition=term_version["definition"],
            synonyms=term_version["synonyms"],
            translations=term_version.get("translations", [""]),
            unit=term_version.get("unit"),
            created_at=term_version["created_at"],
            datatype=datatype,
            related_external_terms=term_version["related_external_terms"],
            vocpopuli_type=vocpopuli_type,
            generated_from="",
            min_=term_version.get("min_"),
            max_=term_version.get("max_"),
            required=term_version.get("required", False),
            purl=term_version.get("purl"),
            gitlab_skos_url=term_version.get("gitlab_skos_url"),
            vocabulary_id=project_id,
        )

        db_term_version.set_latest(term_version["id"], project_id)
        db_user.approve(author_id, term_version["id"], project_id)

    for term in bank_terms:
        term_version = [x for x in bank_term_versions
                        if x["term_version"]["term_id"] == term["id"]]
        term_version = term_version[0]["term_version"]
        
        broader_terms_ids = term_version["broader"]
        broader = []
        for term_version_id in broader_terms_ids:
            term_version_ = [x for x in bank_term_versions
                            if x["term_version"]["id"] == term_version_id][0]["term_version"]
            broader.append(term_version_["term_id"])

        related_terms_ids = term_version["related"]
        related = []
        if isinstance(related_terms_ids, list):
            for term_version_id in related_terms_ids:
                term_version_ = [x for x in bank_term_versions
                                if x["term_version"]["id"] == term_version_id][0]["term_version"]
                related.append(term_version_["term_id"])
        elif isinstance(related_terms_ids, str):
            term_version_id = related_terms_ids
            term_version_ = [x for x in bank_term_versions
                            if x["term_version"]["id"] == term_version_id][0]["term_version"]
            related.append(term_version_["term_id"])

        db_term_version.set_broader(term_version["id"], broader)
        db_term_version.attach_related(term_version["id"], related)
        
    for term_id in top_level_terms_ids:
        if vocpopuli_type != "relationship_type":
            db_term.set_as_top_level_term(term_id, project_id)

    relationship_types = db_vocabulary.get_custom_relationships_from_bank(vocabulary_id)
    for relationship_type in relationship_types:
        db_relationship_type.import_from_terms_db_(
                            vocabulary_id=project_id,
                            user_id=author_id,
                            label=relationship_type["label"],
                            definition=relationship_type["definition"],
                            id_t_global=relationship_type["id_t_global"],
                            id_t_local=relationship_type["id_t_local"],
                            id_t_version=relationship_type["id_t_version"],
                            created_at=relationship_type["created_at"]
                        )
        for subject_version_id, relationship_id, object_term_id in zip(relationship_type["subject_version_ids"],
                                                                       relationship_type["relationships"],
                                                                       relationship_type["object_term_ids"]):
            db_relationship_type.create_relationship(subject_version_id=subject_version_id,
                                                     relationship_type_id=relationship_type["id_t_global"],
                                                     relationship_id=relationship_id,
                                                     object_term_id=object_term_id)
    
    session["PROJECT_ID"] = project_id
    return redirect(url_for(".list_terms"))

   
@main.route("/bank", methods=["GET", "POST"])
@selected_project_required
def explore_bank():
    terms_versions = db_term_version.get_all_terms_db_no_vocab()
    options = [(x["id"], f'{x["label"]} ({x["vocabulary_name"]})') for x in terms_versions]
    options = sorted(options, key=lambda x: x[1])
    select_term_form = SelectTermFromBankForm(terms=options)

    if select_term_form.validate_on_submit():
        return redirect(url_for(".view_term_bank", term_version_id=select_term_form.terms.data))

    data = db_vocabulary.get_all_from_bank()
    domains = []
    for x in data:
        if type(x["domain"]) is list:
            domains.extend(x["domain"])
        else:
            domains.append(x["domain"])
    domains = list(set(domains))
    vocabularies = [((x["id"], x["domain"]), x["name"]) for x in data]
    select_vocabulary_form = SelectVocabularyFromBankForm(domains=domains,
                                                          vocabularies=vocabularies)
    if select_vocabulary_form.validate_on_submit():
        data = select_vocabulary_form.data["vocabulary"]
        id_ = data[2:].split("', ")[0]        
        return redirect(url_for(".visualize_bank_vocabulary", vocabulary_id=id_))
   
    return render_template(
        "explore_bank.html",
        select_term_form=select_term_form,
        select_vocabulary_form=select_vocabulary_form,
        terms=terms_versions,
        select_project_form=_get_select_project_form(),
    )


@main.route("/view_term_bank/<term_version_id>", methods=["GET", "POST"])
def view_term_bank(term_version_id):
    project_id = session.get("PROJECT_ID")
    user_id = session["USER_ID"]
    term_version = db_term_version.get_terms_db(term_version_id)
    id_t_global = term_version["term_id"]

    tree_dict = term_tree(project_id)
    # set read session key
    project_name = db_vocabulary.get(project_id)["name"]

    if db_datatype.is_numeric(project_id, term_version["datatype"]):
        fields = (
            "definition",
            "min_",
            "max_",
            "synonyms",
            "translations",
            "datatype",
        )
    else:
        fields = (
            "definition",
            "synonyms",
            "translations",
            "datatype",
        )

    is_archived = False
    
    datatypes_dict = db_datatype.get_all_dict(project_id)
    if term_version['datatype'] not in datatypes_dict:
        datatypes_dict[term_version['datatype']] = {'color': 'black', 'icon': ''}

    return render_template(
        "view_term_bank.html",
        term_version=term_version,
        tree_dict=tree_dict,
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        datatypes_dict=datatypes_dict,
        select_project_form=_get_select_project_form(),
        fields=fields,
        is_archived=is_archived,
    )


def _import_term_from_bank(term_version: dict, term:dict, broader: list):
    # method used to import a term from the bank into the current vocabulary
    label = term_version["label"]
    related = []
    related_external = (
        ",".join(term_version["related_external_terms"])
        if term_version.get("related_external_terms")
        else ""
    )
    custom_relationships = []
    project_id = session["PROJECT_ID"]
    # fill the form for editing the term
    form = NewTermForm(
        project_id,
        edited_term_id=term["id"],
        label=label,
        datatype=term_version["datatype"],
        reference=term_version.get("reference", ""),
        unit=term_version.get("unit", ""),
        min_=term_version.get("min_", ""),
        max_=term_version.get("max_", ""),
        required_data=term_version.get("required", False),
        vocpopuli_type=term_version.get("vocpopuli_type"),
        broader=broader,
        related=related,
        related_external=related_external,
        custom_relationships=custom_relationships,
    )
    # form.label.data = form_data_dict['label']
    form.label.validators[1].values = []
    form.definition.data = term_version["definition"]
    form.synonym.data = ",".join(term_version["synonyms"])
    form.translations.data = ",".join(term_version.get("translations", []))

    # check if submission is valid and submit new term
    user_id = session["USER_ID"]
    project_id = session["PROJECT_ID"]
    formdata = pre_jsonify_form(form.data)

    approved = True
    id_t_version = term_version["id"]
    id_t_global = term_version["term_id"]

    if not db_datatype.is_numeric(project_id, formdata["datatype"]):
        for field_name in ("unit", "min_", "max_"):
            formdata[field_name] = ""
    
    term = db_term.create(
        user_id=user_id,
        term_id=id_t_global,
        term_version_id=id_t_version,
        label=formdata["label"],
        definition=formdata["definition"],
        reference=formdata["reference"],
        synonyms=formdata["synonyms"],
        translations=formdata["translations"],
        unit=formdata["unit"],
        datatype=formdata["datatype"],
        broader=formdata["broader"],
        related=formdata["related"],
        related_external_terms=formdata["related_external"],
        vocpopuli_type="detached" if len(broader) == 0 else None,
        # TODO change to an actual vocabulary id
        vocabulary_id=session.get("PROJECT_ID"),
        min_=formdata["min_"],
        max_=formdata["max_"],
        required=formdata["required"],
    )
    db_term_version.add_custom_relationships(
        id=term_version["id"],
        relationships=formdata["custom_relationships"],
        vocabulary_id=session.get("PROJECT_ID"),
    )
    if approved:
        db_user.approve(user_id, id_t_version, project_id)
        if "id_t_local" in term_version:
            db_term_version.set_id_t_local(id_t_version, term_version["id_t_local"])    


@main.route("/import_term/<term_version_id>", methods=["GET", "POST"])
@selected_project_required
def import_term(term_version_id):
    """
    The view function used to import a given term.
    """
    term_version = db_term_version.get_terms_db(term_version_id)
    term = db_term.get_terms_db(term_version["term_id"])
    
    _import_term_from_bank(term_version, term, [])
    
    return redirect(url_for(".list_terms"))

    return render_template(
        "import_term.html",
        form=form,
        _template=RelationshipForm(),
        term_version_id=term_version_id,
        edit_term=term_version["label"],
        tree_dict=tree_dict,
        vocpopuli_types=db_vocpopuli_type.all(project_id),
        select_project_form=_get_select_project_form(),
    )
    

@main.route("/import_branch_of_term/<term_version_id>", methods=["GET", "POST"])
@selected_project_required
def import_branch_of_term(term_version_id):
    
    top_branch_term_version = db_term_version.get_terms_db(term_version_id)
    terms_from_bank = db_term_version.get_term_branch_versions_db(term_version_id)
    
    next_to_import = [top_branch_term_version["term_id"]]
    already_imported = set()
    
    while len(next_to_import) > 0:
        
        # get first term of list
        term_version = terms_from_bank[next_to_import[0]]
        next_to_import.pop(0)
        next_to_import += term_version["narrower"]
        already_imported.add(term_version["term_id"])
        next_to_import = list(set(next_to_import).difference(already_imported))
        
        term = db_term.get_terms_db(term_version["term_id"])
        broader = [x for x in term_version["broader"] if x in already_imported]
        
        _import_term_from_bank(term_version, term, broader)
        
    return redirect(url_for(".list_terms"))


@main.route("/save_reordered_list_terms", methods=["POST"])
@selected_project_required
def save_reordered_list_terms():
    user_id = session["USER_ID"]
    vocabulary_id = session.get("PROJECT_ID")
    vocpopuli_types = set(db_vocpopuli_type.names(vocabulary_id))

    try:
        changes = json.loads(request.form["reordering"])
        new_vocpopuli_type_order = json.loads(request.form["vocpopuli_type_reordering"])
    except:
        flash("Vocabulary Structure could not be saved.")
        return redirect(url_for(".list_terms"))

    # save the reordering of the vocpopuli types
    if new_vocpopuli_type_order:
        db_vocpopuli_type.set_new_order(
            vocabulary_id=vocabulary_id, new_order=new_vocpopuli_type_order
        )

    # if the terms have not been moved, there is nothing left to do
    if not changes:
        return redirect(url_for(".list_terms"))

    # get the final set of parents for each moved term
    moved_term_ids = {change["moved_term_id"] for change in changes}
    parents = db_term.get_parents_and_vocpopuli_type_bulk(vocabulary_id, moved_term_ids)
    rearrange_children = {}
    for change in changes:
        moved_term = change["moved_term_id"]
        from_ = change["moved_from_term"]
        to = change["moved_to_term"]

        if from_ != to:
            if not change["is_a_copy"] or (to is None and moved_term not in change["new_list_order"]):
                if from_ in parents[moved_term]:
                    parents[moved_term].remove(from_)
            parents[moved_term].add(to)

        if to is not None:
            rearrange_children[to] = change["new_list_order"]

    set_vocpopuli_type = {}
    set_new_parents = {}
    # verify the new parents
    for term in parents:
        if "detached" in parents[term]:
            parents[term].remove("detached")
        elif None in parents[term]:
            parents[term].remove(None)

        parent_vocpopuli_types = parents[term] & vocpopuli_types
        parent_terms = parents[term] - vocpopuli_types

        # check the number of vocpopuli types and regular terms in parents
        if not parents[term]:
            flash("Moving terms to 'detached' is not a valid structural change.")
            return redirect(url_for(".list_terms"))
        elif not parent_terms:
            if len(parent_vocpopuli_types) == 1:
                # change vocpopuli_type
                set_vocpopuli_type[term] = parent_vocpopuli_types.pop()
            else:
                flash("A term cannot have multiple VocPopuli types.")
                return redirect(url_for(".list_terms"))
        elif not parent_vocpopuli_types:
            # change parents
            set_new_parents[term] = parent_terms
        else:
            flash("Terms can not be top level and lower level at the same time.")
            return redirect(url_for(".list_terms"))

    # set new vocpopuli types
    db_term.change_vocpopuli_type_bulk(
        terms=set_vocpopuli_type,
        user_id=user_id,
        vocabulary_id=vocabulary_id,
    )

    # set new parents
    db_term.change_parents_bulk(
        terms=set_new_parents,
        user_id=user_id,
        vocabulary_id=vocabulary_id,
    )

    # apply the new orders
    db_term.rearrange_children_bulk(
        terms=rearrange_children,
        vocabulary_id=vocabulary_id,
    )

    return redirect(url_for(".list_terms"))


@main.route("/user_settings", methods=["GET", "POST"])
@login_required
def user_settings():
    user_id = session["USER_ID"]
    user_settings_form = UserSettingsForm()
    if user_settings_form.validate_on_submit():
        db_user.set_orcid_id(
            id=user_id,
            orcid_id=user_settings_form.orcid_id.data,
        )
        return redirect(url_for(".user_settings"))

    user = db_user.get(user_id)
    user_settings_form.orcid_id.data = user.get("orcid_id")
    return render_template(
        "user_settings.html",
        select_project_form=_get_select_project_form(),
        user_settings_form=user_settings_form,
    )


@main.route("/vocabulary_metadata", methods=["GET", "POST"])
@login_required
def vocabulary_metadata():
    user_id = session["USER_ID"]
    vocabularies = db_user.get_vocabularies(user_id)
    vocabulary_stats = []

    for vocab in vocabularies:
        vocabulary_id = vocab["id"]
        vocabulary_stats.append(db_user.get_vocabulary_stats(
            id=user_id,
            vocabulary_id=vocabulary_id))
        if vocabulary_stats[-1]["domain"] is not None and type(vocabulary_stats[-1]["domain"]) is not list:
            vocabulary_stats[-1]["domain"] = [vocabulary_stats[-1]["domain"]]

    return render_template(
        "vocabulary_stats.html",
        select_project_form=_get_select_project_form(),
        vocabulary_stats=vocabulary_stats
    )


@main.route("/edit_vocabulary_metadata/<vocabulary_id>", methods=["GET", "POST"])
@login_required
def edit_vocabulary_metadata(vocabulary_id):
    user_id = session["USER_ID"]
    vocabulary_stats = db_user.get_vocabulary_stats(
        id=user_id,
        vocabulary_id=vocabulary_id)

    vocabulary_metadata_form = VocabularyMetadata()

    if request.method == "GET":
        vocabulary_metadata_form.vocabulary_name.data=vocabulary_stats.get("name")
        vocabulary_metadata_form.vocabulary_description.data=vocabulary_stats.get("description")
        if type(vocabulary_stats.get("domain")) == list:
            vocabulary_metadata_form.vocabulary_domain.data=", ".join(vocabulary_stats.get("domain"))
        else:
             vocabulary_metadata_form.vocabulary_domain.data=vocabulary_stats.get("domain")
        vocabulary_metadata_form.institution_name.data=vocabulary_stats.get("institution")
        vocabulary_metadata_form.institution_ror.data=vocabulary_stats.get("institution_ror")
        vocabulary_metadata_form.license_name.data=vocabulary_stats.get("license_name")
        vocabulary_metadata_form.license_url.data=vocabulary_stats.get("license_url")
        
    if vocabulary_metadata_form.validate_on_submit():

        db_vocabulary.update_metadata(id=vocabulary_id,
                                      name=vocabulary_metadata_form.vocabulary_name.data,
                                      description=vocabulary_metadata_form.vocabulary_description.data,
                                      domain=[x.strip() for x in vocabulary_metadata_form.vocabulary_domain.data.split(',')] if vocabulary_metadata_form.vocabulary_domain.data != "" else None,
                                      institution=vocabulary_metadata_form.institution_name.data,
                                      institution_ror=vocabulary_metadata_form.institution_ror.data,
                                      license_name=vocabulary_metadata_form.license_name.data,
                                      license_url=vocabulary_metadata_form.license_url.data)

        return redirect(url_for(".vocabulary_metadata"))

    return render_template(
        "edit_vocabulary_metadata.html",
        select_project_form=_get_select_project_form(),
        vocabulary_metadata_form=vocabulary_metadata_form
    )


@main.route("/error_bank_export", methods=["GET"])
@selected_project_required
def error_bank_export():
    # central nodes
    all_latest_versions = db_term_version.all_latest_versions(vocabulary_id=session["PROJECT_ID"])
    unapproved = [(x["label"], x["id"]) for x in all_latest_versions if not x.get("is_approved", False)]
    return render_template(
        "error_bank_export.html",
        unapproved=unapproved,
        select_project_form=_get_select_project_form(),
    )


@main.route("/restore_term/<term_version_id>", methods=["GET"])
@selected_project_required
def restore_term(term_version_id):
    """
    The function used to approve a given term.
    """
    db_term_version.restore_term(term_version_id)   
    return redirect(url_for(".edit_term", term_version_id=term_version_id))
