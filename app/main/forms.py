import os
import json
from flask import session
from flask_wtf import FlaskForm, Form
from wtforms import (
    BooleanField,
    FieldList,
    FormField,
    MultipleFileField,
    SelectField,
    SelectMultipleField,
    StringField,
    SubmitField,
    TextAreaField,
)
from wtforms.validators import DataRequired, NoneOf, ValidationError, Optional
from werkzeug.datastructures import FileStorage
from ..db import (
    datatype as db_datatype,
    user as db_user,
    vocabulary as db_vocabulary,
    vocpopuli_type as db_vocpopuli_type,
)

MAX_IMG_FILE_SIZE_MB = os.environ.get("MAX_IMG_FILE_SIZE_MB") or 2


def file_size(size_limit_mb=2):
    def _file_size(form, field):
        if not (
            all(isinstance(item, FileStorage) for item in field.data) and field.data
        ):
            return
        if len(field.data) == 1 and field.data[0].filename == "":
            return
        for x in field.data:
            if len(x.read()) > size_limit_mb * (1024**2):  # convert MB to byte
                raise ValidationError(f"File should be smaller than {size_limit_mb} MB")
            x.seek(0)

    return _file_size


def is_http_uri():
    def _is_http_uri(form, field):
        if field.data is not None:
            values = field.data.split(",")
            values = [x.lstrip() for x in values]
            all_start_with_http = all([x.startswith("http") for x in values])
            if not all_start_with_http:
                raise ValidationError("All URIs should start with http")
            else:
                return
        else:
            return

    return _is_http_uri


class SelectProjectForm(FlaskForm):
    project = SelectField(
        label=None,
        description=None,
    )

    def __init__(self, project_id: str | None, user_id: str | None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        vocabularies = {x["id"]: x["name"] for x in db_user.get_vocabularies(user_id)}

        if project_id:
            self.project.choices = sorted(
                [(id, name) for id, name in vocabularies.items()], key=lambda x: x[1]
            )
            self.project.default = project_id
        else:
            first_choice = (None, "Choose a vocabulary project:")
            self.project.choices = [first_choice] + sorted(
                [(id, name) for id, name in vocabularies.items() if id != project_id],
                key=lambda x: x[1],
            )
            self.project.default = None


class TermsJSONsUploadForm(FlaskForm):
    vocab_name = StringField("New vocabulary name (optional)")
    vocabulary_description = StringField("Vocabulary Description")
    vocabulary_domain = StringField(
        "Vocabulary Domain", render_kw={"placeholder": "Comma-separated values"}
    )

    institution_name = StringField("Institution Name")
    institution_ror = StringField("Institution ROR (URL)")

    license_name = StringField("License Name")
    license_url = StringField("License URL")

    files = MultipleFileField(
        "Select the JSON files of the new vocabulary's terms."
        " If left empty, an empty vocabulary repository will be created"
    )

    submit = SubmitField("Submit")


class GitlabImportForm(FlaskForm):
    project_id = StringField("GitLab project id:")
    submit = SubmitField("Import")


class AddNewCommentForm(FlaskForm):
    comment = TextAreaField(
        "Add a new comment:",
        validators=[DataRequired()],
        render_kw={"style": "height:150px;", "id": "comment-area"},
    )
    submit = SubmitField(
        "Submit Comment", render_kw={"class": "btn btn-colour", "id": "comment-btn"}
    )


class RelationshipForm(Form):
    type_ = SelectField("Type:")
    related = SelectMultipleField("Terms:")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        vocabulary_id = session["PROJECT_ID"]
        self.type_.choices = [
            (relationship_type["id_t_global"], relationship_type["label"])
            for relationship_type in db_vocabulary.get_relationship_types(vocabulary_id)
        ]
        self.related.choices = [
            (term["id"], term["label"])
            for term in db_vocabulary.all_terms(vocabulary_id)
        ]


class NewTermForm(FlaskForm):
    none_of_msg = "The term label is already present in the repository."

    label = StringField(
        "Term:",
        validators=[
            DataRequired("Term name field is required."),
            NoneOf(values=[], message=none_of_msg),
        ],
        render_kw={"placeholder": "Unique name", "autofocus": True},
    )

    definition = TextAreaField(
        "Definition:",
        validators=[DataRequired("Definition field is required.")],
        render_kw={"placeholder": "Term definition", "rows": 3},
    )
    
    reference = StringField(
        "Reference:",
        description="It is strongly encouraged for the reference to be an URL",
        render_kw={"placeholder": "Further Information on the term"},
        default=""
    )

    synonym = StringField(
        "Synonyms:",
        description="Comma-separated values.",
        render_kw={"placeholder": "E.g.: label, lbl"},
    )

    translations = StringField(
        "Translations:",
        description="Comma-separated values with optional language tags (@de, @fr).",
        render_kw={"placeholder": "E.g.: Tasse@de"},
    )

    datatype = SelectField(
        label="Data Type:",
        description="If applicable",
    )
    unit = StringField("Unit:", render_kw={"placeholder": "Term unit"}, default="")
    min_ = StringField("Min:", render_kw={"placeholder": "Min"})
    max_ = StringField("Max:", render_kw={"placeholder": "Max"})
    required = BooleanField("Required")
    vocpopuli_type = SelectField(
        label="VocPopuli Type:",
        validators=[Optional()],
    )
    broader = SelectMultipleField("Relatively Broader Terms:")
    related = SelectMultipleField("Related Terms (non-hierarchical):")
    r_e_descr = "Comma-separated URIs beginning with 'http'"
    related_external = StringField("Related External Terms:", description=r_e_descr)
    custom_relationships = FieldList(FormField(RelationshipForm))
    images = MultipleFileField(
        "Image Upload (optional)", validators=[file_size(MAX_IMG_FILE_SIZE_MB)]
    )
    keep_prev_imgs = BooleanField("Keep the previous version's images")

    submit_btn = SubmitField("Save Term")

    def __init__(
        self,
        vocabulary_id: str,
        edited_term_id: str = "",
        # TODO check if this can just be named `required`
        required_data: bool = False,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)

        all_terms = [
            x
            for x in db_vocabulary.all_terms(vocabulary_id)
            if x["id"] != edited_term_id
        ]
        all_terms_choices = [(x["id"], x["label"]) for x in all_terms]
        top_level_terms_choices = [
            (x["id"], x["label"]) for x in all_terms if x["is_top_level"]
        ]
        ontology_terms = []  # TODO: get from ontology
        broader_choices = ontology_terms + all_terms_choices
        related_choices = ontology_terms + top_level_terms_choices

        numeric_types = db_datatype.numeric_types(vocabulary_id)
        toplevel_types = db_datatype.toplevel_types(vocabulary_id)
        datatype_configuration = db_datatype.get_child_datatype_configuration(
            vocabulary_id
        )

        self.label.validators[1].values = all_terms
        self.broader.choices = broader_choices
        self.related.choices = related_choices
        self.vocpopuli_type.choices = db_vocpopuli_type.names(vocabulary_id)
        self.datatype.choices = db_datatype.names(vocabulary_id)
        self.datatype.render_kw = {
            "data-numeric-types": json.dumps(numeric_types),
            "data-toplevel-types": json.dumps(toplevel_types),
            "data-configuration": json.dumps(
                {x["name"]: x["child_types"] for x in datatype_configuration}
            ),
            "data-term-datatypes": json.dumps(
                {x["id"]: x["datatype"] for x in all_terms}
            ),
        }
        # TODO: check if this can be set using a bool directly
        checked = "checked" if required_data else "unchecked"
        self.required.render_kw = {checked: ""}


class KadiTemplateExport(FlaskForm):
    template = SelectField("Choose Template:")
    title_template = StringField("Title:", validators=[DataRequired()])
    tags = StringField(
        "Tags:", render_kw={"placeholder": "comma separated tags, e.g.,  one, two"}
    )
    description = TextAreaField("Description:", render_kw={"rows": 3})

    host = StringField("Kadi4Mat Host:", default="https://kadi4mat.iam-cms.kit.edu")
    pat = StringField("Kadi4Mat Personal Access Token:")

    submit = SubmitField("Export Template to Kadi4Mat")
    download = SubmitField("Download Template")

    def __init__(self, possible_templates: list[str] = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.template.choices = possible_templates


class PublishVocabularyForm(FlaskForm):
    namespace = SelectField("GitLab™ group/namespace:", validators=[DataRequired()])
    vocabulary_tag = StringField(
        "Vocabulary tag:",
        render_kw={"placeholder": "A tag of max. 7 characters. E.g., triboex"},
        validators=[DataRequired()],
    )

    submit = SubmitField("Publish to GitLab™")

    def __init__(self, namespaces, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.namespace.choices = namespaces


class CreateRelationshipTypeForm(FlaskForm):
    label = StringField(
        "Label:",
        validators=[DataRequired()],
    )
    definition = TextAreaField(
        "Definition:", validators=[DataRequired()], render_kw={"rows": "1"}
    )

    related_iri = TextAreaField(
        "Related IRI:",
    )

    submit = SubmitField("Add", render_kw={"class": "btn btn-colour"})


class CreateVocPopuliTypeForm(FlaskForm):
    name = StringField(
        "Name:",
        validators=[DataRequired()],
    )
    color = StringField("Color:", render_kw={"placeholder": "Optional"})
    submit = SubmitField("Add", render_kw={"class": "btn btn-colour"})


class CreateDataTypeForm(FlaskForm):
    name = StringField(
        "Name:",
        validators=[DataRequired()],
    )
    color = StringField("Color:")
    icon = StringField("Icon:")
    numeric = BooleanField("Numeric:")
    toplevel = BooleanField("Toplevel:")
    help_text = TextAreaField("Help text:")
    parent_types = SelectMultipleField("Parent Datatypes:")
    submit = SubmitField("Add", render_kw={"class": "btn btn-colour"})

    def __init__(self, all_types: list):
        super().__init__()
        self.parent_types.choices = all_types


class UserSettingsForm(FlaskForm):
    orcid_id = StringField("ORCID iD:")
    submit = SubmitField("Save settings", render_kw={"class": "btn btn-colour"})

    def validate_orcid_id(form, field):
        # let users remove the orcid_id
        if not field.data:
            return

        try:
            orcid.check(field.data)
        except ValueError as e:
            raise ValidationError(*e.args)


class VocabularyMetadata(FlaskForm):
    vocabulary_name = StringField("Vocabulary Name")
    vocabulary_description = StringField("Vocabulary Description")
    vocabulary_domain = StringField(
        "Vocabulary Domain", render_kw={"placeholder": "Comma-separated values"}
    )

    institution_name = StringField("Institution Name")
    institution_ror = StringField("Institution ROR (URL)")

    license_name = StringField("License Name")
    license_url = StringField("License URL")

    submit = SubmitField("Save Metadata", render_kw={"class": "btn btn-colour"})


class SelectVocabularyFromBankForm(FlaskForm):
    domain = SelectField(
        "Domain:",
        validators=[DataRequired()],
    )
    vocabulary = SelectField(
        "Vocabulary:",
        validators=[DataRequired()],
    )
    submit = SubmitField("Preview Vocabulary", render_kw={"class": "btn btn-colour"})

    def __init__(self, domains, vocabularies, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.domain.choices = domains
        self.vocabulary.choices = vocabularies


class SelectTermFromBankForm(FlaskForm):
    terms = SelectField(
        "Terms:",
        validators=[DataRequired()],
    )

    submit = SubmitField("Preview Term", render_kw={"class": "btn btn-colour"})

    def __init__(self, terms, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.terms.choices = terms
