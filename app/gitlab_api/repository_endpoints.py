"""
A wrapper for the URL's of relevant API endpoints
used throughout VocPopuli
"""

from flask import session


class Endpoints:
    def __init__(self, project_id=None) -> None:
        # the ID of the vocabulary's repository
        self.PROJECT_ID = project_id or session["PROJECT_ID"]
        # project endpoint
        self.PROJECT_EP = f"projects/{self.PROJECT_ID}"
        # issues endpoint
        self.ISSUES_EP = f"{self.PROJECT_EP}/issues"

        # repository endpoint
        self.REPO_EP = f"{self.PROJECT_EP}/repository"
        # repository tree endpoint
        self.REPO_TREE_EP = f"{self.REPO_EP}/tree"
        # repository commits endpoint
        self.REPO_COMMITS_EP = f"{self.REPO_EP}/commits"
        # repository branches endpoint
        self.REPO_BRANCHES_EP = f"{self.REPO_EP}/branches"
        # repository files endpoint
        self.REPO_FILES_EP = f"{self.REPO_EP}/files"
        # path of the approved_terms folder
        self.REPO_APPROVED_TERMS = f"{self.REPO_FILES_EP}/approved_terms"
        # path of the candidate_terms folder
        self.REPO_CANDIDATE_TERMS = f"{self.REPO_FILES_EP}/candidate_terms"
        # merg requests endpoint
        self.MERGE_REQUESTS = f"{self.PROJECT_EP}/merge_requests"
