"""
A module containing wrapper functions for the GitLab API.

Where needed, different methods for taking care of paginated
results are implemented. For more information, visit
https://docs.gitlab.com/ee/api/#pagination
"""

from collections.abc import Iterator
from .repository_endpoints import Endpoints
from ..utils.license import CC_BY_40
from .. import DOTENV_PATH
from flask_dance.contrib.gitlab import gitlab
import requests
from requests import HTTPError
import re
import dotenv
import os
import json
from time import sleep

dotenv.load_dotenv(DOTENV_PATH)
GITLAB = "https://gitlab.com/api/v4"


def get_branches(
    project_id: str | None = None,
    search: str | None = None,
    params: dict | None = None,
    oauth_token: str | None = None,
) -> Iterator[dict]:
    """
    Get all of the branches' dictionaries in a given repository and return them
    as an iterator
    """
    ep = Endpoints(project_id)
    default_params = {"per_page": 100}
    params = default_params | (params or {})

    if search:
        params["search"] = search

    if not oauth_token:
        oauth_token = gitlab.token["access_token"]
    headers = {"Authorization": f"Bearer {oauth_token}"}

    url = f"{GITLAB}/{ep.REPO_BRANCHES_EP}"
    while True:
        response = requests.get(
            url,
            params=params,
            headers=headers,
        )

        yield from response.json()

        try:
            new_url = response.links["next"]["url"]
        except KeyError:
            break

        if url == new_url:
            break

        url = new_url


def get_branch_names(
    drop_merged: bool = True,
    project_id: str | None = None,
    search: str | None = None,
    params: dict | None = None,
    oauth_token: str | None = None,
) -> Iterator[str]:
    """
    Get the names of all branches in a given repository
    """
    branches_dict_list = get_branches(
        project_id, search=search, params=params, oauth_token=oauth_token
    )
    if drop_merged:  # disregard already merged branches
        names = (
            branch_dict["name"]
            for branch_dict in branches_dict_list
            if not branch_dict["merged"]
        )
    else:
        names = (branch_dict["name"] for branch_dict in branches_dict_list)

    return names


def get_issue(issue_iid: int, project_id: str | None = None) -> dict:
    ep = Endpoints(project_id)
    issue = gitlab.get(f"{ep.ISSUES_EP}/{issue_iid}")
    if not issue.ok:
        raise HTTPError
    return issue.json()


def post_new_term_issue(
    title: str,
    description: str,
    labels: str = None,
    project_id: str | None = None,
    oauth_token: str | None = None,
) -> bool:
    """
    Post a new issue related to a given term.
    """
    ep = Endpoints(project_id)
    # the data passed to the POST request
    request_data = {
        "title": title,  # issue title
        "description": description,  # issue description
    }

    # add any issue labels, if applicable
    if labels:
        request_data["labels"] = labels

    # POST the new issue
    if oauth_token:
        response = requests.post(
            f"{GITLAB}/{ep.ISSUES_EP}",
            data=request_data,
            headers={"Authorization": f"Bearer {oauth_token}"},
        )
    else:
        response = gitlab.post(f"{ep.ISSUES_EP}", data=request_data)

    return response


def create_new_vocab_repository(repository_name: str) -> dict:
    """
    Attempts to create a new vocabulary repository.
    """
    repository_description = (
        f"This is the repository of the {repository_name} vocabulary.\n\n"
        "The vocabulary was created using [https://vocpopuli.com](https://vocpopuli.com)."
    )
    response = gitlab.post(
        "projects",
        data={
            "name": repository_name,
            "description": repository_description,
            "initialize_with_readme": False,
            "default_branch": "main",
            "remove_source_branch_after_merge": False,
            "visibility": "private",
        },
    ).json()

    if not response.get("id"):
        raise KeyError

    # Try adding the VocPopuli admin to the repository's members
    if os.getenv("VOCPOPULI_ADMIN_USER_ID"):
        admin_id = int(os.getenv("VOCPOPULI_ADMIN_USER_ID"))
        gitlab.post(
            f"projects/{response['id']}/members",
            data={"user_id": admin_id, "access_level": 50},  # owner
        )

    # Create an approved_terms, candidate_terms, and an images
    # directory in the new repository. Additionally, add
    # a vocabulary license, and metadata file.
    files_ep = f"projects/{response['id']}/repository/files"
    approved_terms_ep = f"{files_ep}/approved_terms%2F%2Egitkeep"
    candidate_terms_ep = f"{files_ep}/candidate_terms%2F%2Egitkeep"
    images_ep = f"{files_ep}/images%2F%2Egitkeep"
    license_ep = f"{files_ep}/LICENSE"
    metadata_ep = f"{files_ep}/metadata%2Ejson"
    readme_ep = f"{files_ep}/README%2Emd"

    # TODO: add IRI's
    metadata_str = json.dumps(
        {
            "VOCABULARY_NAME": repository_name,
            "VOCABULARY_PROV_PURL-IRI": "",
            "VOCABULARY_SKOS_PURL-IRI": "",
        },
        indent=4,
    )

    commits = (
        (license_ep, CC_BY_40, "Add LICENSE"),
        (readme_ep, repository_description, "Add README.md"),
        (metadata_ep, metadata_str, "Add metadata.json"),
        (approved_terms_ep, "", "Create approved_terms"),
        (candidate_terms_ep, "", "Create candidate_terms"),
        (images_ep, "", "Create images"),
    )
    for ep, content, commit_message in commits:
        gitlab.post(
            ep,
            data={
                "branch": "main",
                "content": content,
                "commit_message": commit_message,
            },
        )

    return response


def add_new_file(
    file: str,
    file_path: str,
    branch: str = "main",
    start_branch: str = "main",
    commit_message="New term definition",
    project_id: str | None = None,
) -> bool:
    """
    Add a new file to the repository.
    """
    ep = Endpoints(project_id)
    response = gitlab.post(
        f"{ep.REPO_FILES_EP}/{file_path}",
        data={
            "branch": branch,
            "start_branch": start_branch,
            "content": file,
            "commit_message": commit_message,
        },
    )
    return response.ok


def add_new_branch(branch: str, ref: str, project_id: str | None = None) -> bool:
    """
    Create a new branch. Optionally, from an existing one.

    branch --- Name of the new branch.

    ref --- Branch name or commit SHA to create the new branch from.
    """
    ep = Endpoints(project_id)
    response = gitlab.post(
        f"{ep.REPO_BRANCHES_EP}", data={"branch": branch, "ref": ref}
    )
    return response.ok


def get_issues_list(
    params: dict, project_id: str | None = None, oauth_token: str | None = None
) -> Iterator[dict]:
    """
    Get dictionaries of all issues in a given repository and return them as an
    iterator
    """
    ep = Endpoints(project_id)
    # set default params (which can be overwritten)
    params = {"order_by": "created_at", "sort": "desc", "per_page": 100} | params
    next_page = 1
    while next_page:
        params["page"] = next_page
        if oauth_token:
            response = requests.get(
                f"{GITLAB}/{ep.ISSUES_EP}",
                params=params,
                headers={"Authorization": f"Bearer {oauth_token}"},
            )
        else:
            response = gitlab.get(f"{ep.ISSUES_EP}", params=params)
        yield from response.json()
        # response.headers['X-Next-Page'] is an empty string if there are no
        # more pages
        next_page = response.headers["X-Next-Page"]


def get_branch_tree(branch: str = "main", project_id: str | None = None) -> list[dict]:
    """
    Get the tree of a given repository branch.
    The tree contains, among others, the paths of all files
    in the branch.
    """
    ep = Endpoints(project_id)
    branch_tree_dicts = []
    branch_files_response = gitlab.get(
        f"{ep.REPO_TREE_EP}",
        params={
            "recursive": "true",
            "ref": branch,
            "per_page": 100,
            "pagination": "keyset",
        },
    )

    branch_tree_dicts += branch_files_response.json()

    # handle pagination
    if 'rel="next"' in branch_files_response.headers["Link"]:
        more_pages_left = True
    else:
        more_pages_left = False

    while more_pages_left:
        # get all pagination links
        pagination_links = branch_files_response.headers["Link"].split(", ")
        # get the link to the next page
        next_page_link = [x for x in pagination_links if 'rel="next"' in x][0]
        # strip 'rel="next"'
        next_page_link = next_page_link.split(";")[0]
        # remove leading < and trailing >
        next_page_link = next_page_link[1:-1]
        # strip the API URL
        next_page_link = re.sub("https://gitlab.com/api/v4/", "", next_page_link)
        branch_files_response = gitlab.get(next_page_link)
        branch_tree_dicts += branch_files_response.json()

        # breaking condition
        if 'rel="next"' in branch_files_response.headers["Link"]:
            more_pages_left = True
        else:
            more_pages_left = False

    return branch_tree_dicts


def retry(function, *, max_tries=5, **kwargs):
    """
    Keep retrying an API call until it succeeds or max_tries is reached
    """
    for _ in range(max_tries):
        response = function(**kwargs)

        if response.ok:
            return response
        elif response.status_code == 404:
            response.raise_for_status()

        sleep(1)
