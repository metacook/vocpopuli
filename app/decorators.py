"""
View function wrappers used throughout VocPopuli
"""

from flask_dance.contrib.gitlab import gitlab
from flask import redirect, url_for, session
from functools import wraps
from .db import user as db_user


def login_required(f):
    """
    Checks whether the current user is logged in GitLab
    """

    @wraps(f)
    def decorated_function(*args, **kwargs):
        if not gitlab.authorized:
            return redirect(url_for("gitlab.login_prompt"))
        if not (user_id := session.get("USER_ID")) or not db_user.get(user_id):
            user = gitlab.get("user").json()
            session["USER_ID"] = user["id"]

            db_user.create(
                id=user["id"],
                name=user["name"],
                username=user["username"],
                email=user["email"],
                web_url=user["web_url"],
                avatar_url=user["avatar_url"],
            )
        return f(*args, **kwargs)

    return decorated_function


def selected_project_required(f):
    """
    Checks if user is logged in and has a project selected
    """

    @wraps(f)
    @login_required
    def decorated_function(*args, **kwargs):
        if not session.get("PROJECT_ID"):
            return redirect(url_for(".index"))
        return f(*args, **kwargs)

    return decorated_function
