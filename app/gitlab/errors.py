"""
Error handling for the gitlab blueprint
"""

from flask import redirect, url_for
from oauthlib.oauth2.rfc6749.errors import InvalidGrantError, TokenExpiredError
from . import gitlab_blueprint


@gitlab_blueprint.app_errorhandler(TokenExpiredError)
def token_expired(e):
    return redirect(url_for("gitlab.login"))


@gitlab_blueprint.app_errorhandler(InvalidGrantError)
def invalid_grant(e):
    return redirect(url_for("gitlab.login"))
