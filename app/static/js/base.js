// mark active tab in the navbar
document.querySelectorAll(".nav-link").forEach(item => {
    if (window.location.href == item.href) {
        item.classList.add("active")
    }
});

// enable tooltips
[...document.querySelectorAll('[data-bs-toggle="tooltip"]')]
    .map(tooltip => new bootstrap.Tooltip(tooltip));

// Show the timeoutModal shortly before the token expires
const timeoutModal = document.getElementById("timeoutModal")
if (window.location.pathname != "/login/login_prompt") {
    (async () => {
        let tokenExpiresIn = await fetch("/authorization_status")
            .then(response => response.json())
            .then(token => token['expires_in']);
        setTimeout(() => {
            timeoutModal.style.display = "block"
        }, (tokenExpiresIn - 30) * 5000)
    })()
}
// close the modal if clicked on the X or outside of the modal
timeoutModal.addEventListener("click", event => {
    if (
        event.target == timeoutModal
        || event.target == document.getElementById("timeout-x")
    ) {
        timeoutModal.style.display = "none";
    }
})

// hide the Alpha Version banner and keep it hidden
const hideAlphaVersionBanner = () => {
    document.getElementById('alpha-version-banner').style.display = "none"
    document.getElementById('navbar').style.marginTop = "0"
    document.querySelector('body').style.paddingTop = "64px"
}

if (localStorage.closeAlphaVersionBanner) {
    hideAlphaVersionBanner()
} else {
    document.getElementById('alpha-version-banner-x').addEventListener("click", () => {
        localStorage.closeAlphaVersionBanner = true
        hideAlphaVersionBanner()
    })
}

// show the Agree modals in order
const agreeModal1 = document.getElementById('agree-modal1')
const agreeModal2 = document.getElementById('agree-modal2')
const agreeModal3 = document.getElementById('agree-modal3')
if (!localStorage.agreeTermsApp) {
    agreeModal1.style.display = "block"
    document.getElementById("agree-btn1").addEventListener("click", () => {
        agreeModal1.style.display = "none"
        agreeModal2.style.display = "block"
    });
    document.getElementById("agree-btn2").addEventListener("click", () => {
        agreeModal2.style.display = "none"
        agreeModal3.style.display = "block"
    });
    document.getElementById("agree-btn3").addEventListener("click", () => {
        agreeModal3.style.display = "none"
        localStorage.agreeTermsApp = true
    });
}
