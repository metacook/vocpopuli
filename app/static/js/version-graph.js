const treeData = $(".version-tree").data("tree");
var pathname = window.location.pathname.split("/").pop();

const dx = 70;
const dy = 450;
var width = 0;
var	height = 0;
var	scaled_w = 0;
var	scaled_h = 0;
// x coordinate of root node
var root_x = 0;
const margin = ({top: 100, right: 120, bottom: 100, left: 40});

diagonal = d3.linkHorizontal().x(d => d.y).y(d => d.x);

tree = d3.tree().nodeSize([dx, dy]);

const zoom = d3.zoom()
.on('zoom', (event) => {
    gLink.attr('transform', event.transform);
    gNode.attr('transform', event.transform);
})
.scaleExtent([0.5, 30]);

const root = d3.hierarchy(treeData);
// nodes without children
var endNodes = [];
var endColors = [];

root.x0 = dy / 2;
root.y0 = 0;
root.descendants().forEach((d, i) => {
    d.id = i;
    d._children = null;
    if (! d.children) {
        endNodes.push(d.data);
    }
});
// Order end nodes by version number
endNodes.sort(compareVersions);

function compareVersions(a, b) {
    return parseInt(a.issue_version) - parseInt(b.issue_version);
}

var gradient = [
    [
        -1,
        [19, 80, 88]
    ],
    [
        95,
        [241, 242, 181]
    ],
    [
        100,
        [255,165,0]
    ]
    // [
    //     -1,
    //     [63,38,168]
    // ],
    // [
    //     40,
    //     [36,160,230]
    // ],
    // [
    //     70,
    //     [255,239,1]
    // ],
    // [
    //     100,
    //     [255,175,0]
    // ]
];

function getColorByPercent(percent) {
    var colorRange = [];
    for (let i = 0; i < gradient.length; i++) {
        if (percent <= gradient[i][0]) {
            colorRange = [i - 1, i];
            break;
        }
    }
    //Get the two nearest colors
    var leftcolor = gradient[colorRange[1]][1];
    var rightcolor = gradient[colorRange[0]][1];
    //Calculate ratio between the two nearest colors
    var leftcolor_x = gradient[colorRange[0]][0];
    var rightcolor_x = gradient[colorRange[1]][0] - leftcolor_x;
    var pointer = percent - leftcolor_x;
    var ratio = pointer / rightcolor_x;
    return "rgb(" + pickRgb(leftcolor, rightcolor, ratio) + ")";
}

// Pick rgb from two color with weight
function pickRgb(color1, color2, weight) {
    var p = weight;
    var w = p * 2 - 1;
    var w1 = (w/1+1) / 2;
    var w2 = 1 - w1;
    var rgb = [Math.round(color1[0] * w1 + color2[0] * w2),
        Math.round(color1[1] * w1 + color2[1] * w2),
        Math.round(color1[2] * w1 + color2[2] * w2)];
    return rgb;
}

endColors = generateEndColors();


// Generate a list of colors based on end node number
function generateEndColors() {
    if (endNodes.length == 1) return ["rgb(" + gradient[gradient.length - 1][1] + ")"];
    var largest = endNodes[endNodes.length - 1].issue_version;
    var smallest = endNodes[0].issue_version;
    interval = 100 / (largest - smallest);
    var result = [];
    for (let i = 0; i < endNodes.length; i++) {
        result.push(getColorByPercent(((endNodes[i].issue_version) - smallest) * interval));
    }
    return result;
}

function getNodeColor(vTag) {
    index = endNodes.findIndex((v) => v.issue_version === vTag);
    return index >= 0 ? endColors[index] : "#fff";
}

function getNodeLineColor(vTag) {
    index = endNodes.findIndex((v) => v.issue_version === vTag);
    return index >= 0 ? endColors[index] : "#555";
}

var svg = d3.select(".version-tree").append("svg")
    .style("font", "10px sans-serif")
    .style("user-select", "none")
    .call(zoom);

const gLink = svg.append("g")
    .attr("fill", "none")
    .attr("stroke", "#555")
    .attr("stroke-opacity", 0.4)
    .attr("stroke-width", 1.5);

const gNode = svg.append("g")
    .attr("cursor", "pointer")
    .attr("pointer-events", "all");

function zoomed() {
    gNode.attr("transform", d3.event.transform);
    gLink.attr("transform", d3.event.transform);
}

function update(source) {
const duration = d3.event && d3.event.altKey ? 2500 : 250;
const nodes = root.descendants().reverse();
const links = root.links();

// Compute the new tree layout.
tree(root);

let left = root;
let right = root;
root.eachBefore(node => {
    if (node.x < left.x) left = node;
    if (node.x > right.x) right = node;
});

const transition = svg.transition()
    .duration(duration)
    .tween("resize", window.ResizeObserver ? null : () => () => svg.dispatch("toggle"));

// Update the nodes…
const node = gNode.selectAll("g")
    .data(nodes, d => d.id);

// Enter any new nodes at the parent's previous position.
const nodeEnter = node.enter().append("g")
    .attr("transform", d => `translate(${source.y0},${source.x0})`)
    .attr("fill-opacity", 0)
    .attr("stroke-opacity", 0);

nodeEnter.append("circle")
    .on("click", (e, d) => {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        update(d);
    })
    .attr("r", 15)
    .attr("fill", d => d._children ? "lightsteelblue" : getNodeColor(d.data.issue_version))
    .attr("stroke-width", 10)
    .style("pointer-events","visible");

nodeEnter.append("svg:a").attr("xlink:href", function(d) { 
    return "/view_term/" + d.data.version_id;
})
    .append('foreignObject')
    .attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
    .attr("x", function(d) { return d.children || d._children ? 20 : 20; })
    .attr("y", function(d) { return d.children || d._children ? 5 : -10; })
    .attr("dy", ".35em")
    .style("font-size", "large")
    .attr("height", "1")
    .attr("width", "360")
    .html(function(d){ return "<t class= \"vocabulary-tree-tag\">" + makeTitle(d.data) + "</t>"; })
    .on("mouseover", function(d) {
        d3.select(this)
        .style("text-decoration","underline")
        .style("color", "#0093A1");
    })
    .on("mouseout",function(d) {
        d3.select(this)
        .style("text-decoration","none")
        .style("color", "black");
    });

// Split the version title after ]
function makeTitle(version) {
   return version.created_by + '<br>' + displayableDate(version.created_at)
}

function displayableDate(dateIsoformat) {
    const d = new Date(Date.parse(dateIsoformat));
    const options = {};
    return d.toLocaleString(undefined, undefined);
}

// Transition nodes to their new position.
const nodeUpdate = node.merge(nodeEnter).transition(transition)
    .attr("transform", d => `translate(${d.y},${d.x})`)
    .attr("fill-opacity", 1)
    .attr("stroke-opacity", 1);

    nodeUpdate.select("circle")
        .style("fill", function(d) { return d._children ? "lightsteelblue" : getNodeColor(d.data.issue_version); })
        .style("stroke", d => d.data.version_id == window.location.pathname.split("/")[2] ? "#0093A1" : getNodeLineColor(d.data.issue_version))
        .attr("class", d => d.data.version_id == window.location.pathname.split("/")[2] ? "current-node" : "")
        .style("stroke-width", "5");

    nodeUpdate.select("text")
        .style("fill-opacity", 1);

// Transition exiting nodes to the parent's new position.
const nodeExit = node.exit().transition(transition).remove()
    .attr("transform", d => `translate(${d.y},${d.x})`)
    .attr("fill-opacity", 0)
    .attr("stroke-opacity", 0);

// Update the links…
const link = gLink.selectAll("path")
    .data(links, d => d.target.id);

// Enter any new links at the parent's previous position.
const linkEnter = link.enter().append("path")
    .attr("d", d => {
        const o = {x: source.x0, y: source.y0};
        return diagonal({source: o, target: o});
    });

// Transition links to their new position.
link.merge(linkEnter).transition(transition)
    .attr("d", diagonal);

// Transition exiting nodes to the parent's new position.
link.exit().transition(transition).remove()
    .attr("d", d => {
        const o = {x: source.x, y: source.y};
        return diagonal({source: o, target: o});
    });

// Stash the old positions for transition.
root.eachBefore(d => {
    d.x0 = d.x;
    d.y0 = d.y;
});

width = 0;
var maxHeight = 0;
var minHeight = 0;
nodes.forEach(function(d) {
    //calculate new width and height needed from the node positions
    width = d.y + 300 > width ? d.y + 300 : width;
    minHeight = d.x - 30 < minHeight ? d.x - 30 : minHeight;
    maxHeight = d.x + 30 > maxHeight ? d.x + 30 : maxHeight;
});
height = maxHeight - minHeight;
root_x = minHeight;
// don't resize if only root node left
if (root_x != 0) resizeViewBox();
}

function resizeViewBox() {
    // resize svg viewBox according to window size and ratio
    var w_h_ratio = 2;
    if (w_h_ratio <= width / height) {
        scaled_w = width;
        scaled_h = width / w_h_ratio;
    } else {
        scaled_h = height;
        scaled_w = height * w_h_ratio;
        }
        //resize svg accordingly
        svg = svg.attr("viewBox", "-50 " + (-scaled_h/2) + " " + (scaled_w + 160) + " " + (scaled_h + 50));
}

function zoomIn() {
	d3.select('svg')
		.transition()
		.call(zoom.scaleBy, 2);
	height *= 2;
	width *= 2;
}

function zoomOut() {
	d3.select('svg')
		.transition()
		.call(zoom.scaleBy, 0.5);
	height *= 0.5;
	width *= 0.5;
}

function resetZoom() {
	d3.select('svg')
		.transition()
		.call(zoom.scaleTo, 1);
}
update(root);



function hideGraph() {
    document.getElementById("version-graph").setAttribute("hidden", "hidden");
}

function showGraph() {
    document.getElementById("version-graph").removeAttribute("hidden", "hidden");
}