const hideAll = query => {
    [...document.querySelectorAll(query)]
        .forEach(item => item.classList.add("d-none"))
}

const unhideAll = query => {
    [...document.querySelectorAll(query)]
        .forEach(item => item.classList.remove("d-none"))
}
