// dragable sidebar
const dragToResize = document.getElementById('dragholder')
const resizeSidebar = e => {
    if (e.pageX < window.innerWidth / 2 && e.pageX > window.innerWidth / 10) {
        document.querySelector('body').style.setProperty('--left-width', `${e.pageX}px`)
    }
}
const startResizing = e => {
    e.preventDefault()
    addEventListener("mousemove", resizeSidebar)
    addEventListener("mouseup", stopResizing)
}
const stopResizing = () => {
    removeEventListener("mousemove", resizeSidebar)
    removeEventListener("mouseup", stopResizing)
}
dragToResize.addEventListener("mousedown", startResizing)


// resize sidebar via touch
window.onload = function() {
    dragToResize.addEventListener('touchmove', function(e) {
      var touchLocation = e.targetTouches[0];
        if (touchLocation.pageX < window.innerWidth / 2 && touchLocation.pageX > window.innerWidth / 10) {
            document.querySelector('body').style.setProperty('--left-width', `${touchLocation.pageX}px`)
        }
    })
  }



/**
 * TODO: review everything below
 */
function showParentsAndChildren(current, origin=false) {
    if (current.parent('span').length > 0) {
        current.addClass('selected-term');
        if (origin) {
            current.addClass('origin-term');
        }
        current.parent('span').parent('div').parents('li').each(function () {
            $(this).children('.collapse').collapse('toggle');
        })
    } else {
        current.parent('div').addClass('selected-item');
        if (origin) {
            current.parent('div').addClass('origin-term');
        }
        current.parent('div').parents('li').each(function () {
            $(this).children('.collapse').collapse('toggle');
        })
    }
}

$(document).ready(function () {
    var pathname = window.location.pathname;
    var splitUrlArray = pathname.split('/');
    var treePath = splitUrlArray.pop();
    var issueID = splitUrlArray.pop();

    const urlParams = new URLSearchParams(window.location.search);
    const number = urlParams.get("number");
    let counter = 0;
    // open the parents and its children on the sidebar
    if (treePath == "-") {
        $("a[href*='" + issueID + '/-' + "']").each(function () {
            if (counter == number) {
                showParentsAndChildren($(this), origin=true);
            } else {
                showParentsAndChildren($(this));
            }
            counter++;
        });
    } else {
        $("a[href$='" + treePath + "']").each(function () {
            if (counter == number) {
                showParentsAndChildren($(this), origin=true);
            } else {
                showParentsAndChildren($(this));
            }
            counter++;
        });
    }
    $('#sidebar-tree>li>div>.term-name').addClass("parent");

    $('span.individual-term').hide();
    $('span.individual-term:nth-child(-n+6)').show();
});

// Show the first 6 options and show more button
$(".show-more").click(function () {
    if ($(this).text() == "...show more") {
        $(this).parent('div').children('span.individual-term').show();
        $(this).text("show less");
    } else {
        $(this).parent('div').children('span.individual-term:nth-child(n+7)').hide();
        $(this).text("...show more");
    }
});

// Shortcuts keys to expand or collaspe all child terms on sidebar
$('.collapse-btn').on("click", function (e) {
    if (e.ctrlKey || e.metaKey) {
        if ($(this).attr('aria-expanded') == 'true') {
            $(this).parents('li:eq(0)').find('.collapse').collapse('show');
        } else {
            $(this).parents('li:eq(0)').find('.collapse').collapse('hide');
        }
    }
});