// Shortcut key for comment field
const commentField = document.getElementById('comment-area')
commentField.addEventListener('keydown', e => {
    if (e.key == "Enter" && (e.ctrlKey || e.metaKey)) {
        if (commentField.value != "") {
            document.getElementById('comment-btn').click()
        }
    }
})

// TODO: document what it's supposed to do and check if it works
const carousel = document.getElementById('imageCarousel')
const modalElement = document.getElementById('imgModal')
modalElement?.addEventListener('show.bs.modal', event => {
    const trigger = event.relatedTarget
    let bsCarousel = bootstrap.Carousel.getInstance(carousel)
    bsCarousel.to(trigger.dataset.bsSlideTo)
})

const copyField = button => () => {
    // copy the text
    const field = button.previousSibling.previousSibling
    navigator.clipboard.writeText(field.innerText)

    // show success bubble for a moment
    const successBubble = button.lastChild
    successBubble.style.visibility = "visible"
    setTimeout(() => successBubble.style.visibility = "hidden", 800)
};
[...document.getElementsByClassName('copied-tip')].forEach(bubble => {
    bubble.style.visibility = "hidden"
});
[...document.getElementsByClassName('copy-button')].forEach(button => {
    button.addEventListener("click", copyField(button))
})

/**
 * TODO: add back breadcrumbs (as GET parameter)
 */

// var url = window.location.href;
// var pathname = window.location.pathname;

// var splitUrlArray = pathname.split('/');
// var treePath = splitUrlArray.pop();
// var issueID = splitUrlArray.pop();

// // Use class name to get hyper reference from tree list
// function getURL(className) {
//     // replace space with - to get the class name
//     var issue = $('.' + className.replace(/ /g, "-").replace(/(?=[()])/g, '\\')).children('div').children('.term-name').attr('href');
//     if (issue) {
//         var issueID = issue.split('/').slice(-2).reverse().pop();
//         return '/view_term/' + issueID + '/' + encodeURI(className);
//     }
//     return '/list_terms';
// }

// $(document).ready(function () {
//     // breadcrumb
//     var splitPathArray = decodeURI(treePath).split('-');
//     splitPathArray = splitPathArray.filter(item => item);
//     for (var i = 0; i < splitPathArray.length - 1; i++) {
//         $('<li class="breadcrumb-item"><a href="' + getURL(splitPathArray[i]) + '">' + splitPathArray[i] + '</a></li>').insertBefore('ol.breadcrumb li.active');
//     }
//     // keep the term version after selecting version on the view term page
//     $('#term-version option').attr("value", function () { return $(this).attr("value") + treePath });
//     $("#term-version").val(pathname);
//     $('.term-version-item').attr("href", function () { return $(this).attr("href") + treePath });
//     var version = $("a[href*='" + issueID + '/-' + "']").text();
//     $('.version-item').text(version)
//     $('.edit-button').attr("href", function () { return $(this).attr("href") + treePath });

//     $('.link').each(function () {
//         var className = $(this).text();
//         // set the href by searching the sidebar using class name
//         if (className) {
//             $(this).attr("href", getURL(className));
//         }
//     })
// });


function confirmDeletion(link) {
    if (window.confirm("Are you sure you want to permanently delete the term? This can lead to inconsistencies. Do you wish to continue?")) {
        location.href = link;
    }
}


function correctTimeZone(comment) {
    let utcTime = comment.getAttribute("value");
    console.log(utcTime);
    let stringParts = utcTime.split('T');
    let parts = stringParts[0].split('-')
    let time = stringParts[1].split(".")[0].split(':');
    let date = new Date(Date.UTC(parseInt(parts[0]), parseInt(parts[1])-1, parseInt(parts[2]), parseInt(time[0]), parseInt(time[1]), parseInt(time[2])))
    let formattedTime = date.toLocaleString();
    comment.innerHTML = "• updated on " + formattedTime.replace(", ", " at ");
}

// set timestamp in comments to local timezone
document.querySelectorAll(".comment_time_stamp").forEach(comment => correctTimeZone(comment));
