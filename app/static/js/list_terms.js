const tree_dict_task_status_url = "/tree_dict_status"
const reloadButton = document.getElementById('reloadAction')
let treeUpdateCheckInterval = window.setInterval(() => {
    fetch(tree_dict_task_status_url)
        .then(response => response.json())
        .then(data => {
            if (data["done"] === true) {
                reloadButton.style.display = 'block';
                reloadButton.removeAttribute("hidden");
                clearInterval(treeUpdateCheckInterval)
            }
            if (data["done"] === 'halt') {
                clearInterval(treeUpdateCheckInterval)
            }
        })
}, 1000)

// Show the first 6 options and show more button
$(document).ready(function () {
    $('span.individual-term').hide();
    $('span.individual-term:nth-child(-n+6)').show();
});
$(".show-more").click(function () {
    if ($(this).text() == "...show more") {
        $(this).parent('div').children('span.individual-term').show();
        $(this).text("show less");
    } else {
        $(this).parent('div').children('span.individual-term:nth-child(n+7)').hide();
        $(this).text("...show more");
    }
});

// term search
// const removeMarks = () => {
//     for (const mark of ['child-searched', 'searched-be', 'child-filtered', 'filtered-bg']) {
//         [...document.getElementsByClassName(mark)].forEach(element => element.classList.remove(mark))
//     }
// }
// const treeSearch = () => {
//     removeMarks()

// }

/**
 * TODO: verify everything below
 */

// Filters
$(".list-filter").on("keyup", function () {
    searchAndFilter();
});
$("#unapproved, #unread, #allTerms").on("click", function () {
    searchAndFilter();
});

// Menu buttons for all list
$('.collapse-all-btn').on("click", function () {
    collapseAll('#list-tree');
});
$('.expand-all-btn').on("click", function () {
    expandAll('#list-tree');
});
// Shortcuts keys to expand or collaspe all child terms
$('.collapse-btn').on("click", function (e) {
    if (e.ctrlKey || e.metaKey) {
        if ($(this).attr('aria-expanded') == 'true') {
            $(this).parents('li:eq(0)').find('.collapse').collapse('show');
        } else {
            $(this).parents('li:eq(0)').find('.collapse').collapse('hide');
        }
    }
});

// Menu buttons for each term
$('.collapse-children-btn').on("click", function () {
    $(this).parents('li:eq(1)').find('.collapse').collapse('hide');
});
$('.expand-children-btn').on("click", function () {
    $(this).parents('li:eq(1)').find('.collapse').collapse('show');
});

function markParents(current) {
    if (current.parent('span').hasClass('individual-term')) {
        current.parents('li').addClass('child-searched');
    } else {
        current.parent('div').parent('div').parent('div').parent('li').parents('li').addClass('child-searched');
    }
}

function collapseNotSearched() {
    $('li').each(function () {
        if (!$(this).hasClass('child-searched')) {
            $(this).children('.collapse').collapse('hide');
        }
    })
}
function showMarked() {
    $('.child-searched').children('.collapse').collapse('show');
}

function collapseFiltered() {
    $('li').each(function () {
        if (!$(this).hasClass('child-filtered')) {
            $(this).children('.collapse').collapse('hide');
        }
    })
}
function showFiltered() {
    $('.child-filtered').children('.collapse').collapse('show');
}

function showTerm(current) {
    if (current.children('div').children('div').children('div').children('.term-name').hasClass('searched-bg') || current.children('.term-name').hasClass('searched-bg')) {
        current.show();
        current.parents('li').show();
    }
}

function collapseAll(parent) {
    $(parent).find('.collapse').collapse('hide');
}
function expandAll(parent) {
    $(parent).find('.collapse').collapse('show');
}

function removeMarks() {
    $('.child-searched').removeClass('child-searched');
    $('.searched-bg').removeClass('searched-bg');
    $('.child-filtered').removeClass('child-filtered');
    $('.filtered-bg').removeClass('filtered-bg');
}

function isFilterAdded() {
    return ($('#unapproved').is(':checked') || $('#unread').is(':checked'))
}

function resetShowMore() {
    $(".show-more").show();
    $(".show-more").parent('div').children('span.individual-term:nth-child(n+7)').hide();
    $(".show-more").text("...show more");
}

function removeBadge() {
    // TODO: this seems to assume the old fixed VP types are used
    if ($('.list-term-tree .bg-primary').nextUntil('.list-term-tree .bg-info', 'li:visible').length == 0) {
        $('.list-term-tree .bg-primary').hide();
    } else {
        $('.list-term-tree .bg-primary').show();
    }
    if ($('.list-term-tree .bg-info').nextUntil('.list-term-tree .bg-warning', 'li:visible').length == 0) {
        $('.list-term-tree .bg-info').hide();
    } else {
        $('.list-term-tree .bg-info').show();
    }
    if ($('.list-term-tree .bg-warning').nextAll('li:visible').length == 0) {
        $('.list-term-tree .bg-warning').hide();
    } else {
        $('.list-term-tree .bg-warning').show();
    }
}

function showNoResultPage() {
    if ($('.badge:visible').length == 0) {
        $('.alert-info').show();
    }
}

function searchAndFilter() {
    $('.list-term-tree').hide();
    $('.show-on-load').show();

    $('.alert-info').hide();
    $(".show-more").hide();
    removeMarks();
    if ($('.list-filter').val() == '') {
        if (!isFilterAdded()) {
            collapseAll('#list-tree');
            $('.list-term-tree li').show();
            $('.unapproved-icon').show();
            $('.notRead').show();
            $('.show-more').show();
            $('span.individual-term').hide();
            $('span.individual-term:nth-child(-n+6)').show();
            $('.show-more').text("...show more");
        } else {
            if ($('#unapproved').is(':checked')) {
                $('.approved-icon').parents('li').hide();
                $('.unapproved-icon').parents('li').show();
                $('.unapproved-icon').parents('li').parents('li').show();
                $('.unapproved-icon').parents('li').parents('li').addClass('child-filtered');
                $('.unapproved-icon').parents('li').children('div').children('div').children('.unapproved-icon').addClass('filtered-bg');
                $('.unapproved-icon').parents('li').children('.term-name').addClass('filtered-bg');
            } else if ($('#unread').is(':checked')) {
                $('.read').hide();
                $('.unread-icon').parents('li').show();
                $('.unread-icon').parents('li').parents('li').show();
                $('.unread-icon').parents('li').parents('li').addClass('child-filtered');
                $('.unread-icon').parents('li').children('div').children('div').children('.unread-icon').addClass('filtered-bg');
                $('.unread-icon').parents('li').children('.term-name').addClass('filtered-bg');
            }
            collapseFiltered();
            showFiltered();
        }
    } else {
        var termName = $('.list-filter').val().toLowerCase();
        $(".list-term-tree li, .individual span").hide();
        $(".list-term-tree .term-name").each(function () {
            if ($(this).text().toLowerCase().indexOf(termName) > -1) {
                if (!isFilterAdded()) {
                    $(this).addClass('searched-bg');
                    markParents($(this));
                } else if ($('#unapproved').is(':checked')) {
                    if ($(this).closest('div').find('i').hasClass('unapproved-icon')) {
                        $(this).addClass('searched-bg');
                        $(this).parents('li').children('div').children('div').children('.unapproved-icon').addClass('filtered-bg');
                        markParents($(this));
                    }
                } else if ($('#unread').is(':checked')) {
                    if ($(this).closest('div').find('i').hasClass('unread-icon')) {
                        $(this).addClass('searched-bg');
                        $(this).parents('li').children('div').children('div').children('.unread-icon').addClass('filtered-bg');
                        markParents($(this));
                    }
                }
            }
        });
        $('.list-term-tree li, .individual span').each(function () {
            showTerm($(this));
        })
        collapseNotSearched();
        showMarked();
    }
    setTimeout(function () {
        $('.show-on-load').hide();
        $('.list-term-tree').show();
        removeBadge();
        showNoResultPage();
    }, 1000)
}
$(".list-filter").on("keyup", function () {
    searchAndFilter();
});
function removeMarks() {
    $('.child-searched').removeClass('child-searched');
    $('.searched-bg').removeClass('searched-bg');
    $('.child-filtered').removeClass('child-filtered');
    $('.filtered-bg').removeClass('filtered-bg');
}
function isFilterAdded() {
    return $('#unapproved').is(':checked') || $('#unread').is(':checked');
}
function collapseAll() {
    $('.collapse').collapse('hide');
}
function showTerm(current) {
    if (current.children('div').children('div').children('.term-name').hasClass('searched-bg') || current.children('.term-name').hasClass('searched-bg')) {
        current.show();
        current.parents('li').show();
    }
}

function unfoldTerm(termVersionId) {
    $(".term_" + termVersionId).each(function () {
        $(this).parents('li').each(function () {
            let narrowerTermsList = document.querySelector("#collapse" + this.id);
            if (narrowerTermsList != null) {
                let chevron = document.querySelector("#chevron_" + this.id);
                let isExpanded = chevron.getAttribute('aria-expanded') === 'true';
                if (!narrowerTermsList.classList.contains('show')) {
                    narrowerTermsList.classList.add('show');
                }
                chevron.setAttribute('aria-expanded', true);
            }
        })
    })
}

function collapseFiltered() {
    $('li').each(function () {
        if (!$(this).hasClass('child-filtered')) {
            $(this).children('.collapse').collapse('hide');
        }
    })
}
function showFiltered() {
    $('.child-filtered').children('.collapse').collapse('show');
}

function collapseNotSearched() {
    $('li').each(function () {
        if (!$(this).hasClass('child-searched')) {
            $(this).children('.collapse').collapse('hide');
        }
    })
}
function showMarked() {
    $('.child-searched').children('.collapse').collapse('show');
}

function markParents(current) {
    if (current.parent('span').hasClass('individual-term')) {
        current.parents('li').addClass('child-searched');
    } else {
        current.parent('div').parent('div').parent('li').parents('li').addClass('child-searched');
    }
}
function removeBadge() {
    if ($('.list-term-tree .bg-primary').nextUntil('.list-term-tree .bg-info', 'li:visible').length == 0) {
        $('.list-term-tree .bg-primary').hide();
    } else {
        $('.list-term-tree .bg-primary').show();
    }
    if ($('.list-term-tree .bg-info').nextUntil('.list-term-tree .bg-warning', 'li:visible').length == 0) {
        $('.list-term-tree .bg-info').hide();
    } else {
        $('.list-term-tree .bg-info').show();
    }
    if ($('.list-term-tree .bg-warning').nextAll('li:visible').length == 0) {
        $('.list-term-tree .bg-warning').hide();
    } else {
        $('.list-term-tree .bg-warning').show();
    }
}

function showNoResultPage() {
    if ($('.badge:visible').length == 0) {
        $('.alert-info').show();
    }
}



/**
 * Marking terms as (un)read
 */
$('.read-all-btn').on("click", function () {
    var issueIids = getAllTermsIssueIid('.list-term-tree');
    readAllTerms(issueIids);
    $('.bi-envelope-fill').hide();
    $('.unread-field').css("display", "inline-block");
});
$('.unread-all-btn').on("click", function () {
    var issueIids = getAllTermsIssueIid('.list-term-tree');
    unreadAllTerms(issueIids);
    $('.bi-envelope-fill').show();
});

$('.read-children-btn').on("click", function () {
    $(this).parents('li:eq(1)').addClass('read-all-children');
    var issueIids = getAllTermsIssueIid('.read-all-children');
    readAllTerms(issueIids);
    $('.read-all-children .bi-envelope-fill').hide();
    $('.read-all-children .unread-field').css("display", "inline-block");
    $('.read-all-children').removeClass('read-all-children');
});
$('.unread-children-btn').on("click", function () {
    $(this).parents('li:eq(1)').addClass('unread-all-children');
    var issueIids = getAllTermsIssueIid('.unread-all-children');
    unreadAllTerms(issueIids);
    $('.unread-all-children .bi-envelope-fill').show();
    $('.unread-all-children').removeClass('unread-all-children');
});


function getAllTermsIssueIid(parent) {
    var issueIids = [];
    $(parent).find(".term-name").each(function () {
        var href = $(this).attr('href');
        var issueIid = href.split('/')[2];
        if (jQuery.inArray(issueIid, issueIids) === -1) {
            issueIids.push(issueIid);
        }
    });
    return issueIids;
}

function readAllTerms(issueIids) {
    $.ajax({
        type: "POST",
        url: 'read_all_terms',
        contentType: "application/json",
        dataType: "JSON",
        data: JSON.stringify(issueIids)
    });
}

function unreadAllTerms(issueIids) {
    $.ajax({
        type: "POST",
        url: 'unread_all_terms',
        contentType: "application/json",
        dataType: "JSON",
        data: JSON.stringify(issueIids)
    });
}

// quick addition of terms
const unsavedTerm = () => document.getElementById('new_term_list_element') != null
let currentTermAddition = null;
let currentBroaderVersionId = null;
const newTermListItem = () => {
    const newNode = document
        .getElementById("new_term_list_element_blueprint")
        .cloneNode(true)
    newNode.id = "new_term_list_element"
    return newNode
}


function new_child_term(broaderVersionId, broaderTermId, datatype) {
    // only one new term opened at a time
    if (unsavedTerm()) {
        return;
    }

    // set correct datatype options
    const dataTypeField = document.getElementById("datatype");
    dataTypeField.innerHTML = '';
    const datatypeConfiguration = JSON.parse(dataTypeField.dataset.configuration);
    const newOptions = datatypeConfiguration[datatype];
    newOptions.forEach(o => {
        let newOption = new Option(o,o);
        dataTypeField.appendChild(newOption);
    })
    
    currentBroaderVersionId = broaderVersionId;
    let broaderTerm = document.getElementById(broaderVersionId);
    let hasList = false;

    // make input node
    let newTermInput = newTermListItem();

    //find existing list of narrower terms and append the input node
    broaderTerm.querySelectorAll(':scope > ul').forEach(child => {
        child.insertBefore(newTermInput, child.childNodes[1]);
        hasList = true;
        currentTermAddition = newTermInput;
    })

    // term does not have broader terms
    if (!hasList) {
        makeTermABroaderTerm(broaderVersionId, broaderTerm)
    }

    //add hidden broader term ids
    let broaderIdInputField = document.getElementById('broader_term_id_input');
    broaderIdInputField.value = broaderTermId;

    // expand the list to be visible if its not expanded yet
    let narrowerTermsListId = "#collapse" + broaderVersionId;
    let narrowerTermsList = document.querySelector(narrowerTermsListId);
    let chevron = document.querySelector("#chevron_" + broaderVersionId);
    let isExpanded = chevron.getAttribute('aria-expanded') === 'true';

    // if one list has individuals the broader term contains two separate lists
    if (!isExpanded && broaderTerm.querySelectorAll(':scope > ul').length == 1) {
        narrowerTermsList.classList.toggle('show');
        chevron.setAttribute('aria-expanded', !isExpanded);
    } else if (!isExpanded) {
        broaderTerm.querySelectorAll(':scope > ul').forEach(narrowerList => {
            narrowerList.classList.toggle('show');
        })
        chevron.setAttribute('aria-expanded', !isExpanded);
    }
}

// deletes the input fields and allows different addition
function deleteCurrentAddition(event) {
    event.preventDefault();
    currentTermAddition.remove();
}

// adds a sublist to terms that dont have one when term input is added
function makeTermABroaderTerm(broaderVersionId, listElement) {
    // new list for narrower terms
    let newNarrowerTermsList = document.createElement("ul");
    newNarrowerTermsList.setAttribute("class", "list-group list-group-flush bg-transparent collapse");
    newNarrowerTermsList.setAttribute("id", "collapse" + broaderVersionId);

    // new input field
    let newTermInput = newTermListItem();
    currentTermAddition = newTermInput;
    newNarrowerTermsList.appendChild(newTermInput);
    newNarrowerTermsList.setAttribute("class", "list-group list-group-flush bg-transparent collapse");
    listElement.appendChild(newNarrowerTermsList);

    // create the chevron to open sublist
    let chevron = create_chevron(broaderVersionId);
    let placeholder = document.getElementById("placeholder_" + broaderVersionId);
    parent = placeholder.parentElement;
    parent.replaceChild(chevron, placeholder);
    return;
}


function create_chevron(broaderId) {
    // Create the <a> element to open, close list
    let chevron = document.createElement('a');
    chevron.setAttribute('class', 'btn item-collapsed collapse-btn');
    chevron.setAttribute('type', 'button');
    chevron.setAttribute('data-bs-toggle', 'collapse');
    chevron.setAttribute('data-bs-target', '#collapse' + broaderId);
    chevron.setAttribute('id', 'chevron_' + broaderId);
    chevron.setAttribute('aria-expanded', 'false');

    let chevronRightIcon = document.createElement('i');
    chevronRightIcon.setAttribute('class', 'bi bi-chevron-right collapsed chevron_icon');
    let chevronDownIcon = document.createElement('i');
    chevronDownIcon.setAttribute('class', 'bi bi-chevron-down expanded chevron_icon');
    chevron.appendChild(chevronRightIcon);
    chevron.appendChild(chevronDownIcon);

    return chevron;
}


function create_placeholder(broaderId) {
    // create placeholder <a> element to replace the chevron
    let placeholder = document.createElement('a');
    placeholder.setAttribute('class', 'btn collapse-btn');
    placeholder.setAttribute('type', 'button');
    placeholder.setAttribute('id', 'placeholder_' + broaderId);
    let icon = document.createElement('i');
    icon.setAttribute('style', 'font-size: 15px; opacity: 0;');
    icon.setAttribute('class', 'bi bi-chevron-down expanded');
    placeholder.appendChild(icon);
    return placeholder;
}


function redirect_child_term_edit(event) {
    event.preventDefault();
    // redirects to new child term page with input
    let termName = encodeURIComponent(document.getElementById("term_name").value);
    let datatype = encodeURIComponent(document.getElementById("datatype").value);
    let description = encodeURIComponent(document.getElementById("description").value);
    let broaderVersionId = currentBroaderVersionId;

    window.location.href = "/child_term/" + broaderVersionId + "?" + "label=" + termName + "&definition=" + description + "&datatype=" + datatype;
}

// Restructuring of the terms
const changesMade = new Array();
const vocpopuliTypeReordering = new Array();
const enableReorderingBtn = document.getElementById("enable_reordering_button")
const disableReorderingBtn = document.getElementById("disable_reordering_button")
const saveReorderingBtn = document.getElementById("save_reordering_button")
let nestedSortables = new Array();
let copy = false;
let saving = false;

const setBeforeunloadWarning = () => {
    window.addEventListener("beforeunload", e => {
        if (saving) {
            return null;
        }
        const confirmationMessage = 'If you leave the page without saving the new structure will be lost.';
        e.returnValue = confirmationMessage;
        return confirmationMessage;
    });
}

const setSortableOptionDisabled = newValue => {
    nestedSortables.forEach(sortable => sortable.option("disabled", newValue))
}
const enableSorting = () => setSortableOptionDisabled(false)
const disableSorting = () => setSortableOptionDisabled(true)

enableReorderingBtn.addEventListener("click", () => {
    enableReorderingBtn.setAttribute("hidden", "hidden");
    const warningReordering = document.getElementById("warning_save_reordering");
    if (warningReordering != null) {
        warningReordering.removeAttribute("hidden");
    }
    disableReorderingBtn.removeAttribute("hidden");
    if (nestedSortables.length == 0) {
        initializeSortables();
        setBeforeunloadWarning()
    } else {
        enableSorting()
    }
});

disableReorderingBtn.addEventListener("click", () => {
    disableReorderingBtn.setAttribute("hidden", "hidden");
    enableReorderingBtn.removeAttribute("hidden");
    disableSorting()
});

function addRemoveButton(listItem) {
        // adding button if not there already with x icon to delete the copy
        var button = document.createElement('a');
        button.setAttribute('onclick', 'removeCopy("' + listItem.id + '", '+ changesMade.length +');');
        button.setAttribute('class', 'hide remove_copy_button');
        
        var icon = document.createElement('i');
        icon.setAttribute('style', 'font-size: 20px; padding-left: 8px');
        icon.setAttribute('class', 'bi bi-x');
        icon.setAttribute('type', 'button');
        icon.setAttribute('data-bs-toggle', 'tooltip');
        icon.setAttribute('data-bs-placement', 'right');
        icon.setAttribute('data-bs-custom-class', 'custom-tooltip');
        icon.setAttribute('data-bs-title', 'Delete Copy');
        
        button.appendChild(icon);
    if (listItem.querySelector('.remove_copy_button') == null) {
        termName = listItem.querySelector(".term-name");
        termName.insertAdjacentHTML('afterend', button.outerHTML);
    } else {
        listItem.querySelector('.remove_copy_button').replaceWith(button);
    }
}

function removeCopy(id, index) {
    // removing the copy of a term by adding the deletion to the changes
    listItem = document.querySelector(".copy" + index);
    const changedList = listItem.parentElement;
    $(listItem.querySelector('.bi-x')).tooltip('hide');
    listItem.remove()

    // removing the chevron if list is empty then
    if (changedList.childNodes.length == 0) {
        chevron = changedList.parentElement.querySelector("#chevron_"+changedList.parentElement.id);
        if (chevron) {
            chevron.replaceWith(create_placeholder(changedList.parentElement.id));
        }
    }

    const childrenIds = getListItemChildrenIds(changedList);
    const move = {
        "moved_term_id": listItem.title,
        "moved_from_term": changedList.title,
        "moved_to_term": null,
        "new_list_order": childrenIds,
        "is_a_copy": true
    };
    changesMade.push(move);
}

let timerOpeningTerms = null;
const termSortableOptions = {
    group: 'nested',
    animation: 150,
    fallbackOnBody: false,
    swapThreshold: 0.2,
    onEnd: function (/**Event*/evt) {
        stopOpeningLastTerm();
        saveReorderingBtn.removeAttribute("hidden");
        const changedList = evt.to;
        const childrenIds = getListItemChildrenIds(changedList);
        const move = {
            "moved_term_id": evt.item.title,
            "moved_from_term": evt.from.title,
            "moved_to_term": evt.to.title,
            "new_list_order": childrenIds,
            "is_a_copy": copy
        };
        changesMade.push(move);
        evt.item.style.cursor = "default";

        // temporary list with new term is not temporary anymore
        if (changedList.classList.contains("temporary_drag_list")) {
            changedList.classList.remove("temporary_drag_list");
        }

        // remove temporary chevrons
        document.querySelectorAll(".temporary_drag_list").forEach(list => {
            term = list.parentElement;
            chevron = term.querySelector("#chevron_"+term.id);
            if (chevron) {
                chevron.replaceWith(create_placeholder(term.id));
            }
        });

        // if list where term was removed is empty after the chevron needs to be removed
        if (evt.from.childNodes.length == 0) {
            let chevron =  evt.from.parentElement.querySelector("#chevron_" + evt.from.parentElement.id);
            if (chevron != null) {
                chevron.replaceWith(create_placeholder(evt.from.parentElement.id));
            }
        }
    },
    onStart: function (/**Event*/evt) {
        if (evt.originalEvent.metaKey || evt.originalEvent.ctrlKey) {
            const draggedTerm = evt.item;
            const clone = draggedTerm.cloneNode(true);
            evt.from.insertBefore(clone, draggedTerm);
            addRemoveButton(draggedTerm);
            draggedTerm.classList.add("copy"+changesMade.length);
            clone.style.cursor = "default";
            $('[data-bs-toggle="tooltip"]').tooltip()
            copy = true;
        } else {
            copy = false;
        }
    },
    onChoose: function (/**Event*/evt) {
        evt.item.style.cursor = evt.originalEvent.metaKey ? "copy" : "move";
    },
    onUnchoose: function (/**Event*/evt) {
        evt.item.style.cursor = "default";
    },
    onChange: function(/**Event*/evt) {
        // exiting empty lists need chevron to work
        let placeholder = evt.to.parentElement.querySelector("#placeholder_" + evt.to.parentElement.id);
        if (placeholder) {
            let chevron = create_chevron(evt.to.parentElement.id);
            placeholder.replaceWith(chevron);
            chevron.setAttribute('aria-expanded', true);
        }
        console.log("Change");
	},
    onMove: function (/**Event*/evt) {
        stopOpeningLastTerm();
        timerOpeningTerms = window.setTimeout(function() {
            if (evt.related.classList.contains('item') && evt.related.getElementsByTagName("ul").length == 0) {
                // new list to sort a term under this broader term
                let newNarrowerTermsList = document.createElement("ul");
                newNarrowerTermsList.setAttribute("class", "list-group list-group-flush bg-transparent collapse sortable-list temporary_drag_list");
                newNarrowerTermsList.setAttribute("id", "collapse" + evt.related.id);
                newNarrowerTermsList.setAttribute("title", evt.related.title)
                new Sortable(newNarrowerTermsList, termSortableOptions)
                evt.related.appendChild(newNarrowerTermsList);
                
                // create the chevron to open sublist
                let chevron = create_chevron(evt.related.id);
                let placeholder = document.getElementById("placeholder_" + evt.related.id);
                let parent = placeholder.parentElement;
                parent.replaceChild(chevron, placeholder);
                let isExpanded = chevron.getAttribute('aria-expanded') === 'true';
                if (!isExpanded) {
                    newNarrowerTermsList.classList.toggle('show');
                    chevron.setAttribute('aria-expanded', !isExpanded);
                }
            } else if (document.querySelector("#chevron_" + evt.related.id) != null) {
                // expand the list to be visible if its not expanded yet
                let narrowerTermsListId = "#collapse" + evt.related.id;
                let narrowerTermsList = document.querySelector(narrowerTermsListId);
                let chevron = document.querySelector("#chevron_" + evt.related.id);
                let isExpanded = chevron.getAttribute('aria-expanded') === 'true';
                if (!isExpanded) {
                    narrowerTermsList.classList.toggle('show');
                    chevron.setAttribute('aria-expanded', !isExpanded);
                }
            } 
        },1000);
    }
}

function stopOpeningLastTerm() {
    if (timerOpeningTerms != null) {
        clearTimeout(timerOpeningTerms);
    }
}

const vocpopuliTypeSortableOptions = {
    group: 'shared',
    handle: '.vocpopuli_type_name_handle',
    animation: 150,
    onEnd: function (/**Event*/evt) {
        saveReorderingBtn.removeAttribute("hidden");
        const move = {
            movedType: evt.item.title,
            newListIndex: evt.newIndex,
            newOrder: getListItemChildrenIds(evt.to)
        };
        vocpopuliTypeReordering.push(move);
    }
}

function initializeSortables() {
    const nestedLists = [...document.querySelectorAll(".sortable-list")]
    const vocpopuli_types_list = document.getElementById("vocpopuli_types_list")

    nestedSortables = [
        // makes the term lists sortable
        ...nestedLists.map(list => new Sortable(list, termSortableOptions)),
        // separate sortable list for vocpopuli types
        new Sortable(vocpopuli_types_list, vocpopuliTypeSortableOptions),
    ]
}


// gets the ids of the terms in the list in the correct order
const getListItemChildrenIds = listElement => (
    [...listElement.children]
        .filter(child => child.nodeName == "LI")
        .map(child => child.title)
)


document.getElementById('reordering_form').addEventListener('submit', event => {
    saving = true;
    const formInput = document.getElementById("reordering");
    formInput.value = JSON.stringify(changesMade);
    const vocpopuliTypeInput = document.getElementById("vocpopuli_type_reordering");
    vocpopuliTypeInput.value = JSON.stringify(vocpopuliTypeReordering.pop()?.newOrder || null)
    return true;
});


function scrollingWhileDragging(event) {
    // Get the vertical position of the dragged element
    let draggedElementPosition = event.clientY;
    let height = window.innerHeight
    // Scrolling when dragged in the bottom or top 100 pixel (excluding navbar)
    if (draggedElementPosition <= 164) {
      // Scroll the page up
      window.scrollBy(0, -200, {behavior: 'smooth'});
    }
    else if (draggedElementPosition >= height - 100 ) {
        // Scroll the page up
        window.scrollBy(0, 200,  {behavior: 'smooth'}); 
      }
  }
  
  // Add an event listener for the 'dragover' event on the document
  document.addEventListener('dragover', scrollingWhileDragging);


function addNumbersToInstances() {
    // counts the instance of each term and adds it to the href
    const terms = document.querySelectorAll(".term-name");
    const instance_dict = {};
    terms.forEach(t => {
        if (!(t.id in instance_dict)) {
            instance_dict[t.id] = 0;
        } else {
            instance_dict[t.id] = instance_dict[t.id] + 1;
        }
        t.href = t.href + "?number=" + encodeURIComponent(instance_dict[t.id]);
    })
}
addNumbersToInstances();


function copyTerm(link) {
    if (window.confirm("This creates a new term with the existing information. Do you wish to continue?")) {
        location.href = link;
    }
}