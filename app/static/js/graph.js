//switching tabs
function switchToHierarchicalGraph() {
    if (!$('#hierarchical-graph-tab').hasClass('graph-tab-active')) {
        $('#relation-graph-tab').removeClass('graph-tab-active');
        $('#hierarchical-graph-tab').addClass('graph-tab-active');
        $('#relation-graph').css('display', 'none');
        $('#hierarchical-graph').css('display', 'block');
    }
}
document.getElementById('hierarchical-graph-tab')
    .addEventListener("click", switchToHierarchicalGraph)

function switchToRelationGraph() {
    if (!$('#relation-graph-tab').hasClass('graph-tab-active')) {
        $('#hierarchical-graph-tab').removeClass('graph-tab-active');
        $('#relation-graph-tab').addClass('graph-tab-active');
        $('#hierarchical-graph').css('display', 'none');
        $('#relation-graph').css('display', 'block');
    }
}


let top_level_showing = true

function switchToToplevelGraph() {
    var div = document.getElementById('relation-graph');
    var relationGraph = div.querySelector('svg'); 
    if (relationGraph) {
      div.removeChild(relationGraph); 
    }
    if (top_level_showing) {
        makeRelationshipGraph("related");
        var button = document.getElementById("show_only_record_level");
        button.innerText = "Show only Top-Level Nodes";
        top_level_showing = false;
    } else {
        makeRelationshipGraph("toplevel");
        var button = document.getElementById("show_only_record_level");
        button.innerText = "Show All Nodes";
        top_level_showing = true;
    }
}

document.getElementById('relation-graph-tab')
    .addEventListener("click", switchToRelationGraph)

//hierarchical graph
const treeData = $("#hierarchical-graph").data("tree");
const datatypes = $("#hierarchical-graph").data("map");

const nodeWidth = 70;
const nodeHeight = 300;
let width = 0;
let height = 0;
let scaledWidth = 0;
let scaledHeight = 0;
// x coordinate of root node
let rootNodeX = 0;
const margin = ({ top: 100, right: 120, bottom: 100, left: 40 });

let diagonal = d3.linkHorizontal().x(d => d.y).y(d => d.x);

const tree = d3.tree().nodeSize([nodeWidth, nodeHeight]);

const root = d3.hierarchy(treeData);
// TODO: why x uses height?
root.x0 = nodeHeight / 2;
root.y0 = 0;
root.descendants().forEach((d, i) => {
    d.id = i;
    // only open the first layer by default
    // d.children is the list of children that are shown
    // d._children is used to store them while they're hidden
    if (d.depth == 0) {
        // TODO: this seems unnecessary
        d._children = null;
    } else {
        d._children = d.children;
        d.children = null;
    }
});

const zoom = d3.zoom()
    .on('zoom', event => {
        gLink.attr('transform', event.transform);
        gNode.attr('transform', event.transform);
    })
    .scaleExtent([0.25, 30]);

let svg = d3.select("#hierarchical-graph").append("svg")
    .style("font", "10px sans-serif")
    .style("user-select", "none")
    .call(zoom);

const gLink = svg.append("g")
    .attr("fill", "none")
    .attr("stroke", "#555")
    .attr("stroke-opacity", 0.4)
    .attr("stroke-width", 1.5);

const gNode = svg.append("g")
    .attr("cursor", "pointer")
    .attr("pointer-events", "all");

/**
 * TODO: continue review from here
 */
function zoomed() {
    gNode.attr("transform", d3.event.transform);
    gLink.attr("transform", d3.event.transform);
}

function update(source) {
    const duration = d3.event && d3.event.altKey ? 2500 : 250;
    const nodes = root.descendants().reverse();
    const links = root.links();

    // Compute the new tree layout.
    tree(root);

    let left = root;
    let right = root;
    root.eachBefore(node => {
        if (node.x < left.x) left = node;
        if (node.x > right.x) right = node;
    });

    const transition = svg.transition()
        .duration(duration)
        .tween("resize", window.ResizeObserver ? null : () => () => svg.dispatch("toggle"));

    // Update the nodes…
    const node = gNode.selectAll("g")
        .data(nodes, d => d.id);

    // Enter any new nodes at the parent's previous position.
    const nodeEnter = node.enter().append("g")
        .attr("transform", d => `translate(${source.y0},${source.x0})`)
        .attr("fill-opacity", 0)
        .attr("stroke-opacity", 0);

    nodeEnter.append("circle")
        .on("click", (e, d) => {
            if (d.children) {
                d._children = d.children;
                d.children = null;
            } else {
                d.children = d._children;
                d._children = null;
            }
            update(d);
        })
        .attr("r", 15)
        .attr("fill", d => d._children ? "lightsteelblue" : "#fff")
        .attr("stroke-width", 10)
        .style("pointer-events", "visible");

    nodeEnter.append("svg:a").attr("xlink:href", function (d) {
        if (d.depth == 0) {
            return "/list_terms"
        } else {
            return `/view_term/${d.data.id_t_version}`
        }
    })
        .append('foreignObject')
        .attr("text-anchor", function (d) { return d.children || d._children ? "end" : "start"; })
        .attr("x", function (d) { return d.children || d._children ? 20 : 20; })
        .attr("y", function (d) { return d.children || d._children ? 5 : -10; })
        .attr("dy", ".35em")
        .style("font-size", "large")
        .attr("height", "1")
        .attr("width", "260")
        .html(function (d) { return "<i class= \"hierarchical-graph-tag " + datatypes[d.data.datatype] + " tree-tag-icon \" ></i> <t class= \"hierarchical-graph-tag\">" + d.data.name + "</t>"; })
        .on("mouseover", function (d) {
            d3.select(this)
                .style("text-decoration", "underline")
                .style("color", "#0093A1");
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style("text-decoration", "none")
                .style("color", "black");
        });

    // Transition nodes to their new position.
    const nodeUpdate = node.merge(nodeEnter).transition(transition)
        .attr("transform", d => `translate(${d.y},${d.x})`)
        .attr("fill-opacity", 1)
        .attr("stroke-opacity", 1);

    nodeUpdate.select("circle")
        .attr("r", 15)
        .style("fill", function (d) { return d._children ? "lightsteelblue" : "#fff"; })
        .style("stroke", d => d.data.color ? d.data.color : "SteelBlue")
        .style("stroke-width", "5")
        .style("stroke-dasharray", function (d) { return d.data.color == 'red' ? ("4, 4") : null; });

    nodeUpdate.select("text")
        .style("fill-opacity", 1);

    // Transition exiting nodes to the parent's new position.
    const nodeExit = node.exit().transition(transition).remove()
        .attr("transform", d => `translate(${d.y},${d.x})`)
        .attr("fill-opacity", 0)
        .attr("stroke-opacity", 0);

    // Update the links…
    const link = gLink.selectAll("path")
        .data(links, d => d.target.id);

    // Enter any new links at the parent's previous position.
    const linkEnter = link.enter().append("path")
        .attr("d", d => {
            const o = { x: source.x0, y: source.y0 };
            return diagonal({ source: o, target: o });
        });

    // Transition links to their new position.
    link.merge(linkEnter).transition(transition)
        .attr("d", diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition(transition).remove()
        .attr("d", d => {
            const o = { x: source.x, y: source.y };
            return diagonal({ source: o, target: o });
        });

    // Stash the old positions for transition.
    root.eachBefore(d => {
        d.x0 = d.x;
        d.y0 = d.y;
    });


    var maxHeight = 0;
    var minHeight = 0;
    nodes.forEach(function (d) {
        //calculate new width and height needed from the node positions
        width = d.y + 300 > width ? d.y + 300 : width;
        minHeight = d.x - 30 < minHeight ? d.x - 30 : minHeight;
        maxHeight = d.x + 30 > maxHeight ? d.x + 30 : maxHeight;
    });
    height = maxHeight - minHeight;
    rootNodeX = minHeight;
    // don't resize if only root node left
    if (rootNodeX != 0) resizeViewBox();
}

function resizeViewBox() {
    // resize svg viewBox according to window size and ratio
    var w_h_ratio = (window.innerWidth - 50) / (window.innerHeight - 64 - 30 - 60);
    if (w_h_ratio <= width / height) {
        scaledWidth = width;
        scaledHeight = width / w_h_ratio;
    } else {
        scaledHeight = height;
        scaledWidth = height * w_h_ratio;
    }
    //rescale rootNodeX
    rootNodeX = (rootNodeX * scaledHeight) / height;
    //resize svg accordingly
    svg = svg.attr("viewBox", "-50 " + (rootNodeX) + " " + (scaledWidth + 30) + " " + (scaledHeight));
}

function zoomIn() {
    d3.select('svg')
        .transition()
        .call(zoom.scaleBy, 2);
    height *= 2;
    width *= 2;
}
document.querySelector("#hierarchical-graph .zoom-in-btn").addEventListener("click", zoomIn)

function zoomOut() {
    d3.select('svg')
        .transition()
        .call(zoom.scaleBy, 0.5);
    height *= 0.5;
    width *= 0.5;
}
document.querySelector("#hierarchical-graph .zoom-out-btn").addEventListener("click", zoomOut)

function resetZoom() {
    d3.select('svg')
        .transition()
        .call(zoom.scaleTo, 1);
}
update(root);
document.querySelector("#hierarchical-graph .reset-zoom-btn").addEventListener("click", resetZoom)

function linkExists(node1, node2) {
    linked = false;
    rLinks.each(function (d) {
        //if a link between two nodes exists
        if ((d.source.id === node1 && d.target.id === node2) ||
            (d.source.id === node2 && d.target.id === node1)) {
            linked = true;
        }
    })
    return linked;
}

function dragstarted(event) {
    if (!event.active) rSimulation.alphaTarget(0.3).restart();
    event.subject.fx = event.subject.x;
    event.subject.fy = event.subject.y;
}

function dragged(event) {
    event.subject.fx = event.x;
    event.subject.fy = event.y;
}

function dragended(event) {
    if (!event.active) rSimulation.alphaTarget(0);
    event.subject.fx = null;
    event.subject.fy = null;
}

function rZoomIn() {
    rsvg
        .transition()
        .call(relatedZoom.scaleBy, 2);
    relatedHeight *= 2;
    relatedWidth *= 2;
}
document.querySelector("#relation-graph .zoom-in-btn").addEventListener("click", rZoomIn)

function rZoomOut() {
    rsvg
        .transition()
        .call(relatedZoom.scaleBy, 0.5);
    relatedHeight *= 0.5;
    relatedWidth *= 0.5;
}
document.querySelector("#relation-graph .zoom-out-btn").addEventListener("click", rZoomOut)

function rResetZoom() {
    rTextElems
        .attr("opacity", 1)
    rsvg
        .transition()
        .call(relatedZoom.scaleTo, 1);
    relatedWidth = window.innerWidth / 3;
    relatedHeight = (window.innerHeight - 100) / 3;
}
document.querySelector("#relation-graph .reset-zoom-btn").addEventListener("click", rResetZoom)

function draggedBackground(event) {
    rLinkGroup.attr("transform", event.transform);
    rNodeGroup.attr("transform", event.transform);
    rLinkNames.attr("x", d => (d.source.x + d.target.x) / 2)
    .attr("y", d => (d.source.y + d.target.y) / 2)
    .attr("transform", function(d) {
      const angle = Math.atan2(d.target.y - d.source.y, d.target.x - d.source.x) * (180 / Math.PI);
      return `translate(${(d.source.x + d.target.x) / 2}, ${(d.source.y + d.target.y) / 2}) rotate(${angle})`;
    });
    rTextElems.attr("transform", event.transform);
}

// the variables of the related graph
let rData;
let relatedWidth;
let relatedHeight;
let rsvg;
let rLinkGroup;
let rNodeGroup;
let rSimulation;
let rLine;
let rLinks;
let rLinkNames;
let rNodes;
let relatedZoom;
let rTextElems;
let rLinkNameGroup;


function makeRelationshipGraph(data_source) {

    const colorLinks = "#999";
    const colorSelectedLinks = "#444";
    const colorFadedLinks = "#ddd";
    const fontSizeLinkLabel = 10;

    // getting the correct data from specified source
    rData = $("#relation-graph").data(data_source);

    relatedWidth = window.innerWidth;
    relatedHeight = window.innerHeight;

    relatedZoom = d3.zoom()
        .on('zoom', (event) => {
            rLinks.attr('transform', event.transform);
            rNodes.attr('transform', event.transform);
            rLinkNames.attr("transform", event.transform);
            rLinkNames.style("font-size", String(fontSizeLinkLabel * event.transform.k) + "px");
            rTextElems.attr('transform', event.transform);
        })
        .scaleExtent([0.25, 20]);

    rsvg = d3.select("#relation-graph").append("svg")
        .attr("viewBox", [0, 0, relatedWidth, relatedHeight])
        .call(relatedZoom);

    // append links, link names and nodes to the svg
    rLinkGroup = rsvg.append("g").attr("class", "links");
    rLinkNameGroup = rsvg.append("g").attr("class", "link-name");
    rNodeGroup = rsvg.append("g").attr("class", "nodes");

    // arrow head to indicate custom relationsships
    function makeArrowMarkers(id, color) {
        return rsvg
        .data(rData.links)
        .append("defs")
        .append("marker")
        .attr("id", id)
        .attr("markerWidth", 5) 
        .attr("markerHeight", 5) 
        .attr("refX", 9.3)
        .attr("refY", 2.5) 
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M0,0 L0,5 L5,2.5 z")
        .attr("class", "arrow")
        .style("fill", color);
    }

    // make all different styles of arrowmarkes to be appended when hovered over
    const rArrowMarker = makeArrowMarkers("arrowhead", colorLinks);
    const rArrowMarkerSelected = makeArrowMarkers("arrowhead_selected", colorSelectedLinks);
    const rArrowMarkerFaded = makeArrowMarkers("arrowhead_faded", colorFadedLinks);
    
    rSimulation = d3.forceSimulation(rData.nodes)
        .force("link", d3.forceLink(rData.links).id(d => d.id).distance(200))
        .force("charge", d3.forceManyBody().strength(-200))
        .force("center", d3.forceCenter(relatedWidth / 2, relatedHeight / 2))

    rLine = d3.line().curve(d3.curveBasis);

    // custom relationships have their names on the links
    rLinkNames = rLinkNameGroup.selectAll(".link-name")
        .data(rData.links)
        .enter()
        .insert('text')
        .attr("dy", -4)
        .insert('textPath')
        .attr("class", "link-name")
        .style('text-anchor', 'middle')
        .attr('startOffset', '50%')
        .style("font-size", String(fontSizeLinkLabel) + "px")
        .style("fill", colorLinks)
        .attr("stroke", "white")
        .attr("stroke-width", 3)
        .attr("stroke-linecap", "butt")
        .attr("stroke-linejoin", "miter")
        .attr("stroke-opacity", 0.75)
        .attr("paint-order", "stroke")
        .style("display", d => d.name ? "block" : "none")
        .attr("xlink:href", (d, i) => `#link-${i}`)
        .text(d => d.name);

    // all links, those with names are custom relationships
    rLinks = rLinkGroup.selectAll("path")
        .data(rData.links)
        .enter().append("path")
        .attr("class", d => d.name ? "arrow" : "line") 
        .attr("stroke", colorLinks)
        .attr("stroke-opacity", 1.0)
        .style("stroke-width", d => d.name ? 2 : 2)
        .attr("marker-end", d => d.name ? "url(#arrowhead)" : "")
        .attr("id", (d, i) => `link-${i}`);
    
    // the terms 
    rNodes = rNodeGroup
        .selectAll("circle")
        .data(rData.nodes)
        .enter()
        .append("circle")
        .attr("r", 10)
        .attr("fill", d => d.vocpopuli_type_color || "#888")
        .attr("stroke", "white")
        .attr("stroke-width", 1.5)
        .call(d3.drag()
            .on("start", dragstarted)
            .on("drag", dragged)
            .on("end", dragended))
        .on("mouseenter", (evt, d) => {
            rLinks
                .attr("stroke", colorFadedLinks)
                .attr("marker-end", d => d.name ? "url(#arrowhead_faded)" : "")
                .filter(l => l.source.id === d.id || l.target.id === d.id)
                .attr("marker-end", d => d.name ? "url(#arrowhead_selected)" : "")
                .attr("stroke", colorSelectedLinks)
            rNodes
                .attr("opacity", 0.2)
                .filter(n => n.id === d.id || linkExists(d.id, n.id))
                .attr("opacity", 1.0);
            rLinkNames
                .attr("opacity", 0)
                .filter(l => l.source.id === d.id || l.target.id === d.id)
                .attr("opacity", 1.0)
                .style("fill", colorSelectedLinks);
            rTextElems
                .attr("opacity", 0)
                .filter(n => n.id === d.id || linkExists(d.id, n.id))
                .attr("opacity", 1.0);
        })
        .on("mouseleave", evt => {
            rLinks.attr("stroke", colorLinks)
                .attr("stroke-opacity", 1.0)
                .attr("marker-end", d => d.name ? "url(#arrowhead)" : "");
            rNodes.attr("opacity", 1.0);
            rLinkNames.attr("opacity", 1.0)
                .style("fill", colorLinks)
            rTextElems.attr("opacity", 1.0);
        });

    // names of the terms
    rTextElems = rsvg.append('g')
        .selectAll('text')
        .data(rData.nodes)
        .join('text')
        .text(d => d.label || "")
        .attr("paint-order", "stroke")
        .attr("stroke", "white")
        .attr("stroke-width", "3px")
        .attr("stroke-linecap", "butt")
        .attr("stroke-linejoin", "miter")
        .attr("stroke-opacity", 0.75)
        .attr('font-size', 14)
        .attr('dy', -11)
        .on("mouseover", function (d) {
            d3.select(this)
                .style("text-decoration", "underline")
                .style("cursor", "pointer")
                .style("fill", "#0093A1")
                .attr("stroke-width", "1.5px");
        })
        .on("mouseout", function (d) {
            d3.select(this)
                .style("text-decoration", "none")
                .style("fill", "black")
                .attr("stroke-width", "3px");
        })
        .on("click", function (evt, d) {
            window.open(`view_term/${d.term_version_id}`, "_self");
        });

    rSimulation.on("tick", () => {
        rLinks.attr("d", d => {
            if (d.name) {
                const x1 = d.source.x;
                const y1 = d.source.y;
                const x2 = d.target.x;
                const y2 = d.target.y;
                const dx = x2 - x1;
                const dy = y2 - y1;
                const angle = Math.atan2(dy, dx);
                const arrowX = x2 - length * Math.cos(angle);
                const arrowY = y2 - length * Math.sin(angle);
                return rLine([[x1, y1], [arrowX, arrowY]]);
            } else {
                return rLine([[d.source.x, d.source.y], [d.target.x, d.target.y]]);
            }
        });

        rLinkNames.attr("x", d => (d.source.x + d.target.x) / 2)
            .attr("y", d => (d.source.y + d.target.y) / 2)
            .attr("transform", function (d) {
                const angle = Math.atan2(d.target.y - d.source.y, d.target.x - d.source.x) * (180 / Math.PI);
                return `translate(${(d.source.x + d.target.x) / 2}, ${(d.source.y + d.target.y) / 2}) rotate(${angle})`;
            });

        rNodes.attr("cx", d => d.x)
            .attr("cy", d => d.y);
        rTextElems.attr("x", d => d.x)
            .attr("y", d => d.y);
    });
}

makeRelationshipGraph("toplevel");

// calculates current length of a link
function calculateCurrentLength(link) {
    const dx = link.target.x - link.source.x;
    const dy = link.target.y - link.source.y;
    return Math.sqrt(dx * dx + dy * dy);
}

// sets strentgh to 0 so the force is disabled and graph is frozen
let frozen_graph = false;
function freezeGraph(button) {
    if (frozen_graph) {
        rSimulation.force("charge", d3.forceManyBody().strength(-200));
        rSimulation.force("link", d3.forceLink(rData.links).id(d => d.id).distance(d => calculateCurrentLength(d)).strength(d => 1));
        $(button).find('i').toggleClass('bi-pin bi-pin-angle');
        frozen_graph = false;
    } else {
        rSimulation.force("charge", d3.forceManyBody().strength(0));
        rSimulation.force("link", d3.forceLink(rData.links).id(d => d.id).distance(100).strength(0));
        $(button).find('i').toggleClass('bi-pin-angle bi-pin');
        frozen_graph = true;
    }
}
