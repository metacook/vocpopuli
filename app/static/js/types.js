
const save_button = document.getElementById("save-structure-button");
const table = document.getElementById("sortable-table");
const form = document.getElementById("save-structure-form");
const inputOrder = document.getElementById("new_order_input");

// allow sorting of the types if the table is supposed to be sortable
if (table != null) {
    const sortable = new Sortable(table, {
        handle: '.handle',
        animation: 150,
        onEnd: function (event) {
            save_button.style.visibility = "visible";
        }
    });
    
    save_button.addEventListener("click", () => {
        let values = [];
        for (var i = 0; i < table.rows.length; i++) {
            let row = table.rows[i];
            let value = row.getAttribute("value");
            values.push(value);
        }
        inputOrder.value = JSON.stringify(values);
        form.submit();
    });
}


function submitForm(form_id) {
    const form = document.getElementById(form_id);
    form.submit();
}

// editing the types, currently edited type
let hiddenRow = null;

function editType(row) {
    if (hiddenRow != null) {
        hiddenRow.removeAttribute("hidden");
    }
    row.setAttribute("hidden", "hidden");
    hiddenRow = row;
    const editFormRow = document.getElementById("edit_form_row");
    row.after(editFormRow);
}

function editRelationshipType(row, label, definition, related_iri, id) {
    editType(row);
    document.getElementById("edit_label").innerHTML = label;
    document.getElementById("edit_definition").value = definition;
    document.getElementById("edit_related_iri").value = related_iri;
    document.getElementById("relationship_id").value = id;
}

function editDataType(row, name, color, icon, numeric, help_text, toplevel, parentDatatypes) {
    editType(row);
    console.log(parentDatatypes)
    document.getElementById("edit_name").value = name;
    document.getElementById("edit_color").value = color;
    document.getElementById("edit_icon").value = icon;
    document.getElementById("edit_numeric").checked = (numeric == "True");
    document.getElementById("edit_toplevel").checked = (toplevel == "True");
    document.getElementById("edit_help_text").value = help_text;
    document.getElementById("old_name").value = name;

    const parentSelector = document.getElementById("edit_parent_types");
    const parents = JSON.parse(parentDatatypes);
    for (let i = 0; i < parentSelector.options.length; i++) {
        if (parentSelector.options[i].value == "<<itself>>") {
            parentSelector.options[i].setAttribute("hidden", "hidden")
        } else {
            parentSelector.options[i].selected = parents.indexOf(parentSelector.options[i].value) >= 0;
        }
    }
}

function editVocPopuliType(row, name) {
    editType(row);
    document.getElementById("edit_name").value = name;
    document.getElementById("old_name").value = name;
}

// stop editing button
document.getElementById("stop_editing_button").addEventListener("click", evt => {
    evt.preventDefault();
    const editFormRow = document.getElementById("edit_form_row");
    document.getElementById("form-table").appendChild(editFormRow);
    hiddenRow.removeAttribute("hidden");
})


// inhibit that datatypes are numeric and toplevel
let numeric = document.getElementById("numeric");
let toplevel = document.getElementById("toplevel");
let editNumeric = document.getElementById("edit_numeric");
let editToplevel = document.getElementById("edit_toplevel");

if (numeric != null && toplevel != null) {
    numeric.addEventListener("change", function() {
        if (this.checked) {
            toplevel.checked = false;
        }
    });
    toplevel.addEventListener("change", function() {
        if (this.checked) {
            numeric.checked = false;
        }
    });
    editNumeric.addEventListener("change", function() {
        if (this.checked) {
            editToplevel.checked = false;
        }
    });
    editToplevel.addEventListener("change", function() {
        if (this.checked) {
            editNumeric.checked = false;
        }
    });
}

 // allow selection and deselection of parent datatypes without ctrl
 document.querySelectorAll(".easy-select").forEach(item => {
    item.addEventListener("mousedown", function (event) {
        event.preventDefault();    
        let option = event.target;
        if (option.tagName === "OPTION") {
            option.selected = !option.selected;
        }
    })
});

