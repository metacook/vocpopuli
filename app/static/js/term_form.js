const warnUnsavedChanges = event => event.returnValue = "Your unsaved data will be lost.";
document.querySelectorAll("select,input,textarea").forEach(item => {
    item.addEventListener("change", () => {
        addEventListener("beforeunload", warnUnsavedChanges)
    })
});
document.getElementById("termForm").addEventListener("submit", () => {
    removeEventListener("beforeunload", warnUnsavedChanges)
});

const broaderTerms = document.getElementById("broader")
const topLevelSign = document.getElementById("toplevel_sign")
const isTopLevel = () => broaderTerms.value.length == 0

// show or hide numeric fields
const dataTypeField = document.getElementById('datatype-field')
const numericTypes = JSON.parse(dataTypeField.dataset.numericTypes)
const toplevelTypes = JSON.parse(dataTypeField.dataset.toplevelTypes)
const datatypeConfiguration = JSON.parse(dataTypeField.dataset.configuration)
const termDatatypes = JSON.parse(dataTypeField.dataset.termDatatypes)

const showNumericFields = () => {
    const selectedDataType = dataTypeField.value

    if (selectedDataType && numericTypes.includes(selectedDataType)) {
        unhideAll('.numeric-field-metadata')
    } else {
        hideAll('.numeric-field-metadata')
    }
}
showNumericFields()
dataTypeField.addEventListener("change", showNumericFields)

const originalDatatype = dataTypeField.value;

function changeDatatypeOptions(newOptions) {
    // remember selected datatype
    let selection = null;
    if (dataTypeField.selectedIndex != -1) {
        selection = dataTypeField.options[dataTypeField.selectedIndex].value;
    } else {
        selection = originalDatatype;
    }

    // clear all options
    dataTypeField.innerHTML = '';

    newOptions.forEach(o => {
        let newOption = new Option(o,o);
        dataTypeField.appendChild(newOption)
    })
    // set back datatype
    if (selection) {
        dataTypeField.value = selection;
    }
}

function determineDatatypeOptions() {
    let intersection = Object.keys(datatypeConfiguration);
    for (let i = 0; i < broaderTerms.options.length; i++) {
        let option = broaderTerms.options[i];
        if (option.selected) {
            let selectedDatatype = termDatatypes[option.value];
            intersection = intersection.filter(x => new Set(datatypeConfiguration[selectedDatatype]).has(x));
        }
    }
    return intersection;
}

// show or hide relation field
const showTopLevelTermFields = () => {
    const vocPopuliTypeField = document.getElementById("contexttype")
    if (isTopLevel()) {
        unhideAll("#related-div")
        unhideAll("#contexttype-div")
        vocPopuliTypeField.required = true
        topLevelSign.removeAttribute("hidden")
        changeDatatypeOptions(toplevelTypes)
    } else {
        hideAll("#related-div")
        hideAll("#contexttype-div")
        vocPopuliTypeField.required = false
        topLevelSign.setAttribute("hidden", "hidden")
        changeDatatypeOptions(determineDatatypeOptions())
    }
}
showTopLevelTermFields()
broaderTerms.addEventListener("change", showTopLevelTermFields)

// set selected badges
const showSelectedBadges = optionsField => () => {
    const badgesDiv = optionsField.parentNode.getElementsByClassName('selected-options')[0]
    badgesDiv.innerHTML = [...optionsField.selectedOptions]
        .map(option => option.innerText)
        .map(label => `<span class='badge bg-secondary'>${label}</span>`)
        .join()
};
function showBadges() {
    [...document.getElementsByClassName('options')].forEach(optionsField => {
        showSelectedBadges(optionsField)()
        optionsField.addEventListener("input", showSelectedBadges(optionsField))
    })
}
showBadges();

/**
 * Term name validation (?) and other stuff (?)
 */

// search field for multiple selection
$(".form-filter").on("input", function () {
    var value = $(this).val().toLowerCase();
    $(this).siblings(".options").children().filter(function () {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});


// Get all data types of terms in broader selector
// function getAllDataTypes() {
//     var broaderTerms = $('.broader option:selected').toArray().map(item => item.text);
//     var dataTypes = [];
//     const unique = arr => [...new Set(arr)];
//     for (let i = 0; i < broaderTerms.length; ++i) {
//         var broaderTerm = broaderTerms[i].split(" ").join("-");
//         var dataType = $('#sidebar-tree').find('.' + broaderTerm).attr('termType');
//         dataTypes.push(dataType);
//     }
//     return unique(dataTypes);
// }


// // Validations when datatype is changed
// var selectedOption = $(".datatype option").filter(':selected').val();
// $(".datatype").on('change', function () {
//     $(".datatype-error").empty();
//     $(".datatype").removeClass("is-invalid");
//     if ($(this).val() != "record") {
//         selectedOption = $(".datatype option").filter(':selected').val();
//         validateMin();
//         validateMax();
//     };
//     dataTypes = getAllDataTypes();
//     if ($(this).val() === "option") {
//         var type = dataTypes[0];
//         if (dataTypes.length === 1 && (type === "record" || type === "option")) {
//             $(".datatype-error").text("Data type could not be option.");
//             $(".datatype").addClass("is-invalid");
//         }
//     }
//     unitAndRangeField();
// });


// Validations when inputs are changed
// $('#termForm :input').on('change input', function () {
//     if ($('.definition').val()) {
//         $(".definition").removeClass("is-invalid");
//     }
//     if ($('#contexttype').is(":visible") && $('#contexttype').val()) {
//         $("#contexttype").removeClass("is-invalid");
//     }
//     if ($('.term-name-input').val()) {
//         $(".term-name-input").removeClass("is-invalid");
//     }
// });


// Validations when submit the form
// (function () {
//     'use strict'
//     // Fetch all the forms which are needed to be validated
//     var forms = document.querySelectorAll('.needs-validation')
//     // Loop over them and prevent submission
//     Array.prototype.slice.call(forms)
//         .forEach(function (form) {
//             form.addEventListener('submit', function (event) {
//                 // Each time remove validation errors from backend
//                 $('.from-errors').remove();
//                 // Check required fields
//                 if (!$('.term-name-input').val()) {
//                     $(".term-name-input").addClass("is-invalid");
//                 }
//                 if (!$('.definition').val()) {
//                     $(".definition").addClass("is-invalid");
//                 }
//                 if ($('#contexttype').is(":visible") && !$('#contexttype').val()) {
//                     $("#contexttype").addClass("is-invalid");
//                 } else {
//                     $("#contexttype").removeClass("is-invalid");
//                 }
//                 var invalidFields = document.getElementsByClassName('is-invalid');
//                 if (invalidFields.length > 0) {
//                     event.preventDefault()
//                     event.stopPropagation()
//                 }
//                 // Release all disabled fields for submission
//                 if (invalidFields.length == 0) {
//                     $(this).find('.broader option:selected').prop('disabled', false);
//                     $(this).find(".datatype").prop('disabled', false);
//                     if ($('.custom-relation-label').text() != 'Custom relation:') {
//                         $('.custom-relation-name').val($('.custom-relation-label').text());
//                     }
//                 }
//             }, false)
//         })
// })()

// TODO: check if used and review
// const resetForm = () => {
//     $(".term-name-error").empty();
//     $(".broader-error").empty();
//     $(".datatype-error").empty();
//     $(".term-name-input").prop('type', 'text');
//     if (!$('.datatype').val() || $('.datatype').val() == 'option') {
//         $(".datatype").val(' ').change();
//     }
//     $(".term-name-input").removeClass("is-invalid");
//     $(".datatype").removeClass("is-invalid");
//     $(".broader").removeClass("is-invalid");
// }

// TODO: check if used and review
// Term name validator
// function validateTermName() {
//     resetForm();
//     var allTypes = ['string', 'boolean', 'integer', 'float', 'date'];
//     dataTypes = getAllDataTypes();
//     if (dataTypes.length === 1 && jQuery.inArray(dataTypes[0], allTypes) !== -1) {
//         $(".datatype").val('option').change();
//         $(".datatype").prop('disabled', true);
//         var type = dataTypes[0];
//         var termName = $(".term-name-input").val();
//         if (type === "float") {
//             if ($.isNumeric(termName)) {
//                 if (Math.floor(termName) == termName) {
//                     $(".term-name-input").val(parseFloat($(".term-name-input").val()).toFixed(1));
//                 }
//             } else {
//                 $(".term-name-error").text("Term name must be float.");
//                 $(".term-name-input").addClass("is-invalid");
//             }
//         }
//         if (type === "integer") {
//             if ($.isNumeric(termName) && Math.floor(termName) == termName) {
//                 $(".term-name-input").val(Math.floor(termName));
//             } else {
//                 $(".term-name-error").text("Term name must be integer.");
//                 $(".term-name-input").addClass("is-invalid");
//             }
//         }
//         if (type === "date") {
//             $(".term-name-input").prop('type', 'date');
//         }
//     } else if (dataTypes.length === 1 && jQuery.inArray(dataTypes[0], allTypes) === -1) {
//         var type = dataTypes[0];
//         if (type === "option") {
//             $(".broader-error").text("This term cannot be broader term.");
//             $(".broader").addClass("is-invalid");
//         }
//         if ($(".datatype").val() === "option" && (type === "record" || type === "option")) {
//             $(".datatype-error").text("Data type could not be option.");
//             $(".datatype").addClass("is-invalid");
//         }
//     } else if (dataTypes.length > 1) {
//         $(".broader-error").text("Broader temrs hava different data type.");
//         $(".broader").addClass("is-invalid");
//     }
// }

// TODO: check if used and review
// Range validator (Min and Max value)
// function validateMax() {
//     var max = $(".max").val();
//     $(".max").removeClass("is-invalid");
//     $(".max-error").empty();
//     if ($(".datatype").val() === "float") {
//         if ($.isNumeric(max)) {
//             if (Math.floor(max) == max) {
//                 $(".max").val(parseFloat($(".max").val()).toFixed(1));
//             }
//             if ($(".min").val() && !$(".min").hasClass('is-invalid')) {
//                 if (parseFloat($(".max").val()) < parseFloat($(".min").val())) {
//                     $(".max-error").text("Max value must be larger than min.");
//                     $(".max").addClass("is-invalid");
//                 }
//             }
//         } else {
//             if (max) {
//                 $(".max-error").text("Max value must be float.");
//                 $(".max").addClass("is-invalid");
//             }
//         }
//     }
//     if ($(".datatype").val() === "integer") {
//         if ($.isNumeric(max) && Math.floor(max) == max) {
//             $(".max").val(Math.floor(max));
//             if ($(".min").val() && !$(".min").hasClass('is-invalid')) {
//                 if (parseInt($(".max").val()) < parseInt($(".min").val())) {
//                     $(".max-error").text("Max value must be larger than min.");
//                     $(".max").addClass("is-invalid");
//                 }
//             }
//         } else {
//             if (max) {
//                 $(".max-error").text("Max value must be integer.");
//                 $(".max").addClass("is-invalid");
//             }
//         }
//     }
// }
// function validateMin() {
//     var min = $(".min").val();
//     $(".min").removeClass("is-invalid");
//     $(".min-error").empty();
//     if ($(".datatype").val() === "float") {
//         if ($.isNumeric(min)) {
//             if (Math.floor(min) == min) {
//                 $(".min").val(parseFloat($(".min").val()).toFixed(1));
//             }
//             if ($(".max").val() && !$(".max").hasClass('is-invalid')) {
//                 if (parseFloat($(".max").val()) < parseFloat($(".min").val())) {
//                     $(".min-error").text("Min value must be smaller than max.");
//                     $(".min").addClass("is-invalid");
//                 }
//             }
//         } else {
//             if (min) {
//                 $(".min-error").text("Min value must be float.");
//                 $(".min").addClass("is-invalid");
//             }
//         }
//     }
//     if ($(".datatype").val() === "integer") {
//         if ($.isNumeric(min) && Math.floor(min) == min) {
//             $(".min").val(Math.floor(min));
//             if ($(".max").val() && !$(".max").hasClass('is-invalid')) {
//                 if (parseInt($(".max").val()) < parseInt($(".min").val())) {
//                     $(".min-error").text("Min value must be smaller than max.");
//                     $(".min").addClass("is-invalid");
//                 }
//             }
//         } else {
//             if (min) {
//                 $(".min-error").text("Min value must be integer.");
//                 $(".min").addClass("is-invalid");
//             }
//         }
//     }
// }


// TODO: check if used and review
// Page contents initializations
// unitAndRangeField();
if (window.location.pathname.indexOf("edit_term") == 0) {
    $("#broader-div #collapse-control").removeAttr('data-bs-toggle');
    $("#broader-div #collapse-control").removeAttr('data-bs-target');
    $("#broader-div #collapse-control").removeAttr('aria-expanded');
    $("collapseBroader").removeAttr('class');

    $("#broader-div #collapse-control i").remove();
}

// TODO: check if used and review
// Term validation
// var typingTimer;                //timer identifier
// var doneTypingInterval = 1000;  //time in ms
// $('.term-name-input').keyup(function () {
//     clearTimeout(typingTimer);
//     typingTimer = setTimeout(validateTermName, doneTypingInterval);
// });
// $('.min, .max').keyup(function () {
//     clearTimeout(typingTimer);
//     if ($('.min').val()) {
//         typingTimer = setTimeout(validateMin, doneTypingInterval);
//     }
//     if ($('.max').val()) {
//         typingTimer = setTimeout(validateMax, doneTypingInterval);
//     }
//     if (!$('.min').val() || !$('.max').val()) {
//         $(".min").removeClass("is-invalid");
//         $(".min-error").empty();
//         $(".max").removeClass("is-invalid");
//         $(".max-error").empty();
//     }
// });

const removeParent = event => {
    event.preventDefault()
    event.target.parentNode.parentNode.remove()
    setIndexes()
}

const setIndexes = () => {
    document.getElementById("relationship-forms").querySelectorAll(".relationship-form").forEach((node, index) => {
        node.id = `custom_relationships-${index}-form`
        node.querySelectorAll("*[id]").forEach(item => {
            item.id = item.id.replaceAll(/.*-/g, "")
            item.id = `custom_relationships-${index}-${item.id}`
            item.name = item.name.replaceAll(/.*-/g, "")
            item.name = `custom_relationships-${index}-${item.name}`
        })
    })
}

document.getElementById("add-relationship-form").addEventListener("click", event => {
    event.preventDefault()
    const forms = document.getElementById("relationship-forms")
    const template = document.getElementById("custom_relationships-_-form")
    const newNode = template.cloneNode(deep = true)
    newNode.style = ""
    newNode.querySelector(".remove-relationship-form").addEventListener("click", removeParent)
    forms.appendChild(newNode)
    setIndexes()

    $(".relationship-filter").on("input", function () {
        var value = $(this).val().toLowerCase();
        $(this).siblings(".form-control").children().filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
})

document.querySelectorAll(".remove-relationship-form").forEach(item => {
    item.addEventListener("click", removeParent)
})

// allow selection and deselection without ctrl
document.querySelectorAll(".easy-select").forEach(item => {
    item.addEventListener("mousedown", function (event) {
        event.preventDefault();    
        let option = event.target;
        if (option.tagName === "OPTION") {
            option.selected = !option.selected;
        }
        showBadges();
        showTopLevelTermFields();
    })
});

document.body.addEventListener('keydown', (event) => {
    if(event.key === "Enter" && (event.metaKey || event.ctrlKey)) {
        let form = document.getElementById("termForm");
        form.submit();
    }
});
