from ..db import user as db_user
from flask_dance.contrib.gitlab import gitlab
from ..gitlab_api import api

SLASH = "%2F"
DOT = "%2E"


def get_user_id(search_term):
    # search_term can be an email or name or username
    results = gitlab.get(f"users?search={search_term}").json()
    user_id = results[0]["id"]
    return merge_user(user_id)


def merge_user(user_id) -> int:
    if not db_user.get(user_id):
        user = gitlab.get(f"users/{user_id}").json()
        db_user.create(
            id=user_id,
            name=user["name"],
            username=user["username"],
            email=user["public_email"],
            web_url=user["web_url"],
            avatar_url=user["avatar_url"],
        )
    return user_id


def get_commits(project_id):
    return yield_from_pages(f"projects/{project_id}/repository/commits?all=true")


def get_issues(project_id):
    return yield_from_pages(f"projects/{project_id}/issues")


def get_comments(project_id, issue_id):
    return yield_from_pages(f"projects/{project_id}/issues/{issue_id}/notes")


def get_votes(project_id, issue_id):
    return yield_from_pages(f"projects/{project_id}/issues/{issue_id}/award_emoji")


def yield_from_pages(starting_url):
    url = starting_url
    while True:
        response = gitlab.get(url)
        response.raise_for_status()
        yield from response.json()
        try:
            new_url = response.links["next"]["url"]
        except KeyError:
            break

        if url == new_url:
            break

        url = new_url


def get_file_raw(project_id, file_path, branch_name):
    response = gitlab.get(
        f"projects/{project_id}/repository/files/{file_path}/raw?ref={branch_name}"
    )

    if not response.ok:
        raise ValueError(response.status_code)

    return response.json()


def get_term_versions(project_id, term_id, branch_name):
    i = 0
    while True:
        edit = "" if i == 0 else f"_edit_{i}"
        filename = f"{term_id}{edit}{DOT}json"
        file_path = f"candidate_terms{SLASH}{filename}"

        try:
            response = get_file_raw(project_id, file_path, branch_name)
        except ValueError:
            break

        yield (filename.replace(DOT, "."), response)
        i += 1

    filename = f"{term_id}{DOT}json"
    file_path = f"approved_terms{SLASH}{filename}"
    try:
        response = get_file_raw(project_id, file_path, branch_name)
        yield (filename.replace(DOT, "."), response)
    except ValueError:
        pass


def get_approved_versions(project_id):
    for file in yield_from_pages(
        f"projects/{project_id}/repository/tree?path=approved_terms"
    ):
        if file["path"].endswith(".json"):
            yield get_file_raw(
                project_id, file["path"].replace("/", SLASH).replace(".", DOT), "main"
            )


def get_latest_branches(project_id) -> dict[str, tuple[int, dict]]:
    latest_branches: dict[str, tuple[int, dict]] = {}
    for branch in api.get_branches(project_id=project_id):
        name = branch["name"]
        if name == "main":
            continue

        # possible branch names:
        # 'new_term_xxx_xxx_xxx-averylongid-vp'
        # 'term_reopening_1_xxx_xxx_xxx-averylongid-vp
        name_parts = name.split("_")
        order_id = 0 if name.startswith("new_term") else int(name_parts[2])
        term_id = "_".join(name_parts[-3:])

        if term_id not in latest_branches or latest_branches[term_id][0] < order_id:
            latest_branches[term_id] = (order_id, branch)
    return latest_branches
