import dotenv
import os
import git
import requests
from .. import DOTENV_PATH
from ..gitlab_api.api import get_branch_names

# load the acces token from the .env file
dotenv.load_dotenv(DOTENV_PATH)


def load_repositories(oauth_token: str | None = None):
    # the main directory of all vocabularies
    VOCABULARIES_DIR = os.environ.get("VOCABULARIES_DIR") or os.path.abspath(
        "vocabulary_repositories"
    )

    # create the vocab directory if it doesn't exists
    if not os.path.exists(VOCABULARIES_DIR):
        os.mkdir(VOCABULARIES_DIR)

    # parameters used in the API calls
    # TODO: check what is needed for which call
    params = {
        "private_token": os.environ.get("VOCPOPULI_TOKEN"),
        "merged": False,
        "per_page": 100,
    }
    headers = {"Authorization": f"Bearer {oauth_token}"} if oauth_token else {}

    projects_dict_list = requests.get(
        "https://gitlab.com/api/v4/projects", params=params | {"membership": True}
    ).json()

    # load all repositories which the admin user has access to
    # and put their relevant information as a dict in vocabulary_repositories
    for repo in projects_dict_list:
        repo_id = repo["id"]
        repository_path = f"https://gitlab.com/api/v4/projects/{repo_id}/repository"
        repo_tree_response = requests.get(
            f"{repository_path}/tree", params=params, headers=headers
        )
        if not repo_tree_response.ok:
            continue
        repo_tree_response = repo_tree_response.json()

        # check if it's a vocabulary (has 'approved_terms' and 'candidate_terms'
        # folders)
        try:
            top_level_names = {x["name"] for x in repo_tree_response}
        except:
            raise NotImplementedError(repo_tree_response)

        if not {"approved_terms", "candidate_terms"} <= top_level_names:
            continue

        http_url_to_repo = repo["http_url_to_repo"].replace(
            "://gitlab.",
            f'://{os.environ.get("VOCPOPULI_ADMIN_USER_ID")}:{os.environ.get("VOCPOPULI_TOKEN")}@gitlab.',
        )
        repo_name = repo["name"].replace(" ", "-")

        # Clone all repository branches,
        # which have not already been added to the directory

        # the directory of the given repository
        # it contains subdirectories for each non-merged repository branch
        repository_dir = os.path.join(VOCABULARIES_DIR, repo_name)
        if not os.path.exists(repository_dir):
            os.mkdir(repository_dir)

        # clone each non-merged branch
        for branch in get_branch_names(
            drop_merged=False,
            project_id=repo_id,
            params=params,
            oauth_token=oauth_token,
        ):
            branch_dir = os.path.join(repository_dir, f"{repo_name}-{branch}")
            if not os.path.exists(branch_dir):
                os.mkdir(branch_dir)
                cloned_repo = git.Repo.clone_from(
                    url=http_url_to_repo, to_path=branch_dir
                )
                cloned_repo.git.checkout(branch)
