"""
Utility functions for parsing data about terms
in the vocabulary.
"""

from markdown import markdown
from uuid import UUID
from flask_dance.contrib.gitlab import gitlab
from ..gitlab_api.api import Endpoints
from ..db import (
    relationship_type as db_relationship_type,
    vocpopuli_type as db_vocpopuli_type,
)
import re
import base64
from os.path import splitext
from os import environ
import requests


def parse_issue_description(term_definition: str, vocabulary_id: str) -> dict:
    """
    Converts the HTML definition of a given term,
    taken from its respective GitLab issue, into a
    Python dictionary.
    """
    # group all paragraphs under appropriate labels
    paragraphs = re.finditer("<p>(?P<content>.*?)</p>", term_definition, re.DOTALL)
    definition_parts = {}
    current_label = ""
    for paragraph in paragraphs:
        content = paragraph["content"]
        # Check if the paragraph contains a section header. If it does, create
        # a new list of paragraphs for that header. Otherwise add the paragraph
        # to the same list as the previous paragraph.
        match = re.fullmatch(
            "<strong>(?P<label>.*?):</strong> (?P<value>.*)", content, re.DOTALL
        )
        if match:
            current_label = match["label"]
            content = match["value"]
            definition_parts[current_label] = []

        definition_parts[current_label].append(content)

    # labels corresponding to form_data_dict keys
    labels_for_keys = {
        "label": "Label",
        "definition": "Definition",
        "synonyms": "Synonyms",
        "translations": "Translations (with language tags)",
        "datatype": "Data type",
        "vocpopuli_type": "Vocpopuli type",
        "unit": "Unit",
        "min_": "Min",
        "max_": "Max",
        "required": "Required",
        "broader": "Relatively broader terms",
        "broader_labels": "Relatively broader terms (labels)",
        "related": "Related terms (non-hierarchical)",
        "related_labels": "Related terms (non-hierarchical) (labels)",
        "related_external": "Related external terms",
        "Image files": "Term images",
        "id_t_global": "Global Term ID",
        "id_t_local": "Local Term ID",
        "id_t_version": "Term Version ID",
        "generated_from": "Generated From",
    }

    # labels that appear in issues, but don't have any meaningful data
    ignored_labels = {
        "Custom relation",
        "Range",
    }

    # labels that are handled separately later
    handled_separately = {
        "Synonyms (with language tags)",
        "PURL",
        "GitLab SKOS URL",
    }

    # for each key get the corresponding paragraphs and join them
    form_data_dict = {
        key: "<br>".join(definition_parts.get(label, []))
        for key, label in labels_for_keys.items()
    }

    # old version for vocpopuli type
    for name in ("Contextual type", "VocPopuli type"):
        type_content = definition_parts.get(name)
        if type_content:
            form_data_dict["vocpopuli_type"] = "<br>".join(type_content)

    # get possible custom relationships
    possible_custom_relationships_raw = {
        key: "<br>".join(paragraphs)
        for key, paragraphs in definition_parts.items()
        if key not in ("Contextual type", "VocPopuli type")
        and key not in labels_for_keys.values()
        and key not in ignored_labels
        and key not in handled_separately
    }

    # convert <ul>s into lists of items
    for key in (
        "synonyms",
        "translations",
        "broader",
        "broader_labels",
        "related",
        "related_labels",
        "related_external",
    ):
        form_data_dict[key] = [
            content
            for item in re.finditer(
                "<li>(?P<content>.*?)</li>", form_data_dict[key], re.DOTALL
            )
            if (content := item["content"]) != ""
        ]

    # convert <ul>s into lists of items
    possible_custom_relationships = {}
    for key, value in possible_custom_relationships_raw.items():
        try:
            possible_custom_relationships[key] = [
                content
                for item in re.finditer("<li>(?P<content>.*?)</li>", value, re.DOTALL)
                if (content := item["content"]) != ""
            ]
        except:
            continue

    # old version for synonyms and translation
    synonyms_translations_content = definition_parts.get(
        "Synonyms (with language tags)"
    )
    if synonyms_translations_content:
        synonyms_translations = "<br>".join(synonyms_translations_content)
        for item in re.finditer(
            "<li>(?P<content>.*?)</li>", synonyms_translations, re.DOTALL
        ):
            if "@" in item["content"]:
                form_data_dict["translations"].append(item["content"])
            else:
                form_data_dict["synonyms"].append(item["content"])

    # convert to img_tags
    form_data_dict["Image files"] = [
        img_tag
        for i, x in enumerate(
            re.finditer(
                "<li>(?P<url>.*?)</li>", form_data_dict["Image files"], re.DOTALL
            )
        )
        if (img_tag := url_to_img_tag(i, x["url"])) is not None
    ]

    # validate vocpopuli_type
    if form_data_dict["vocpopuli_type"] not in db_vocpopuli_type.names(vocabulary_id):
        form_data_dict["vocpopuli_type"] = ""

    # validate Local Term ID
    try:
        UUID(form_data_dict["id_t_local"][:-3], version=4)
    except ValueError:
        form_data_dict["id_t_local"] = ""

    # get purl and gitlab_skos_url if these exist
    purl = "".join(definition_parts.get("PURL", []))
    if "http" in purl:
        form_data_dict["purl"] = purl

    url = "".join(definition_parts.get("GitLab SKOS URL", []))
    if "http" in url:
        form_data_dict["gitlab_skos_url"] = url

    form_data_dict["custom"] = possible_custom_relationships

    return form_data_dict


def section(label, value):
    return markdown(f"**{label}:** {value}")


def new_term_to_markdown(term_dict: dict) -> str:
    """
    Convert the data from a term's JSON into a
    markdown string which is used as the description
    of the term's GitLab issue.
    """

    def ul(items):
        lis = "".join(
            f"<li>{stripped_item}</li>"
            for item in items
            if (stripped_item := item.strip()) != ""
        )
        return f"<ul>{lis}</ul>"

    term_data = term_dict.get("term")
    term_metadata = term_dict.get("metadata")
    image_files = term_dict.get("image_files", [])

    sections = [
        ("Label", term_data["label"]),
        ("Definition", term_data["definition"]),
        ("Synonyms", ul(term_data["synonyms"])),
        ("Translations (with language tags)", ul(term_data["translations"])),
        ("Data type", term_data["datatype"]),
        ("Vocpopuli type", term_data.get("vocpopuli_type", "")),
        ("Unit", term_data.get("unit", "")),
        ("Min", term_data.get("min_", "")),
        ("Max", term_data.get("max_", "")),
        ("Required", term_data.get("required", "")),
        ("Relatively broader terms", ul(term_data["broader"])),
        ("Relatively broader terms (labels)", ul(term_data.get("broader_labels", []))),
        ("Related terms (non-hierarchical)", ul(term_data["related"])),
        (
            "Related terms (non-hierarchical) (labels)",
            ul(term_data.get("related_labels", [])),
        ),
        ("Related external terms", ul(term_data["related_external"])),
        ("Term images", ul(image_files)),
        ("Global Term ID", term_metadata.get("id_t_global", "")),
        ("Local Term ID", term_metadata.get("id_t_local", "")),
        ("Term Version ID", term_metadata.get("id_t_version", "")),
    ] + [
        (relationship["type_label"], ul(relationship["related"]))
        for relationship in term_data["custom_relationships"]
    ]

    return "".join(section(label, value) for label, value in sections)


def pre_jsonify_form(form_data: dict) -> dict:
    """
    Some light preprocessing of the submitted
    form data for a new or edited term.
    """
    processed_dict = {}
    for k, v in form_data.items():
        # skip hidden/unneeded fields
        if k in ["submit", "csrf_token", "images", "keep_prev_imgs"]:
            continue

        if k == "custom_relationships":
            processed_dict[k] = [
                {
                    "type_id": item["type_"],
                    "type_label": db_relationship_type.get(item["type_"])["label"],
                    "related": item["related"],
                }
                for item in v
            ]

        elif isinstance(v, list):
            try:
                # remove empty list elements
                processed_dict[k] = [x for x in v if x.strip()]
            except AttributeError:
                # if items don't have .strip()
                processed_dict[k] = v

        # turn a string of comma-separated values into a list
        elif k in ["synonym", "related_external", "translations"]:
            if k == "synonym":
                k_new = "synonyms"
            else:
                k_new = k
            processed_dict[k_new] = [x.strip() for x in form_data[k].split(",")]
        # take the field's value as-is
        else:
            processed_dict[k] = v

    return processed_dict


def url_to_img_tag(i: int, img_url: str):
    """
    Converts an img_url into an image tag which can be
    embedded using the view_term view function.

    img_url should be of the form:
    'https://gitlab.com/NAMESPACE/PROJ_NAME/-/raw/BRANCH_NAME/images/IMG_FILE.XXX'
    """
    try:
        img_url_split = img_url.split("-/raw/")[1]
        branch_name, file_name = img_url_split.split("/images/")
        file_name = "%2E".join(file_name.rsplit(".", 1))
        extension = splitext(file_name)[1].removeprefix(".")

        # try-except as a workaround for using the celery
        # background task
        try:
            ep = Endpoints()
            img = gitlab.get(
                f"{ep.REPO_FILES_EP}/" f"images%2F{file_name}/raw?ref={branch_name}"
            )
        except (RuntimeError, AttributeError):
            img = requests.get(
                f"https://gitlab.com/api/v4/{environ.get('REPO_FILES_EP')}/"
                f"images%2F{file_name}/raw?ref={branch_name}",
                params={"private_token": environ.get("VOCPOPULI_TOKEN")},
            )
        img = base64.b64encode(img.content).decode("ascii")

        img_tag = (i, img, extension)
    except IndexError:
        img_tag = None

    return img_tag
