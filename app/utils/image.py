from flask import current_app, url_for
from typing import Iterator, Tuple
from .parsing import url_to_img_tag
from ..models.file import File
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename
import base64
import os

# Allowed image file extensions for image upload
img_filetypes = [".png", ".jpg", ".jpeg"]


def _has_valid_img_filetype(file) -> bool:
    return os.path.splitext(file.filename)[-1] in img_filetypes


def filter_valid_images(files) -> Iterator[FileStorage]:
    return (
        x for x in files if isinstance(x, FileStorage) and _has_valid_img_filetype(x)
    )


def save_in_uploads(file: FileStorage, file_id: str):
    folder = os.path.join(
        current_app.config["UPLOADS_DIR"],
        file_id,
    )
    os.makedirs(folder)
    path = os.path.join(folder, secure_filename(file.filename))
    file.save(path)
    return path


def delete_image(path):
    os.remove(path)

    # remove the folder if empty
    folder = os.path.dirname(path)
    if not os.listdir(folder):
        os.rmdir(folder)


def load_image(image: File) -> Tuple[int, str, str] | None:
    if image["local"]:
        return url_for(".serve_file", file_id=image["id"])

    return image["url"]
