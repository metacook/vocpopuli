from functools import cache

@cache
def determine_color_vocpopuli_type(vocpopuli_type: str):
    STANDARD_TYPES = {
        "object": "DeepSkyBlue",
        "data": "Gold",
        "process": "RoyalBlue",
        "detached": "Gray"
    }
    if vocpopuli_type in STANDARD_TYPES:
        return STANDARD_TYPES[vocpopuli_type]
    
    COLOR_CHOICES = [
        "Black",  # 000000
        "Blue",  # 0000FF
        "BlueViolet",  # 8A2BE2
        "Brown",  # A52A2A
        "Crimson",  # DC143C
        "DarkBlue",  # 00008B
        "DarkGreen",  # 006400
        "DarkMagenta",  # 8B008B
        "DarkOliveGreen",  # 556B2F
        "DarkOrchid",  # 9932CC
        "DarkRed",  # 8B0000
        "DarkSlateBlue",  # 483D8B
        "DarkSlateGray",  # 2F4F4F
        "DarkViolet",  # 9400D3
        "DimGray",  # 696969
        "FireBrick",  # B22222
        "Green",  # 008000
        "Indigo",  # 4B0082
        "Maroon",  # 800000
        "MediumBlue",  # 0000CD
        "MediumVioletRed",  # C71585
        "MidnightBlue",  # 191970
        "Navy",  # 000080
        "Olive",  # 808000
        "Purple",  # 800080
        "RebeccaPurple",  # 663399
        "RoyalBlue",  # 4169E1
        "SaddleBrown",  # 8B4513
        "Sienna",  # A0522D
        "SlateBlue",  # 6A5ACD
        "Teal",  # 008080
    ]
    vt = vocpopuli_type.lower()
    x = 0
    for i, l in enumerate(vt):
        x += ord(l) * (i + 1)

    return COLOR_CHOICES[(x >> 1) % len(COLOR_CHOICES)]


def add_color_vocpopuli_types(vocpopuli_types: list[dict]):
    for vt in vocpopuli_types:
        vt['color'] = determine_color_vocpopuli_type(vt['name'])
    return vocpopuli_types