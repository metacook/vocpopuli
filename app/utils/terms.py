"""
Functions related to the definition and/or extraction
of vocabulary terms.
"""

from .parsing import parse_issue_description
from uuid import uuid4
from ..gitlab_api import api
import re


def term_tag(term_label: str) -> str:
    """
    Converts the term's label into a
    3-part string which has the following structure:

    'xxx_xxx_xxx'

    Each triple of x's is replaced by the
    first three letters of each of the three
    first words of the term's label (if these exist).

    If the term's label is too short, the x's
    act as placehodlers in the string.

    Non-alphanumeric characters are disregarded.

    Examples:
    term_tag('Cup Grinding Process') = 'cup_gri_pro'
    term_tag('Specimen Cleaning') = 'spe_cle_xxx'
    term_tag('IAM') = 'iam_xxx_xxx'
    term_tag('EM') = 'emx_xxx_xxx'
    term_tag('V^(-1)/%') = 'v1x_xxx_xxx'
    """
    if term_label is None:
        return "xxx_xxx_xxx"

    term_label = term_label.lower()
    # consider only the first three words:
    term_label_parts = term_label.split()[:3]
    term_label_parts = [
        "".join([i for i in x if i.isalnum()]) for x in term_label_parts
    ]
    term_label_parts = [re.sub(r"[^\x00-\x7f]", r"", x) for x in term_label_parts]
    term_label_parts = [f"{part}xxx"[:3] for part in term_label_parts]

    return "_".join((term_label_parts + ["xxx"] * 3)[:3])


def generate_uid() -> str:
    "Generates an UUID uid for a given term"

    uid = uuid4()
    return uid.hex


def get_term_jsons(vocabulary_id) -> list[dict]:
    """
    Extracts a list of the JSONs of all vocabulary terms.
    """

    # get all repository issues
    issues = list(api.get_issues_list(params={"scope": "all"}))

    all_jsons = []
    candidate_terms_dicts = {}

    for issue in issues:
        approved = "approved" in issue["labels"]

        # keep if approved
        if issue["state"] == "closed" and approved:
            all_jsons.append(issue | {"approved": True})

        # keep the last edit if not approved
        elif issue["state"] == "opened" and not approved:
            id_t_global = issue["title"].rsplit(" | ")[-1]
            if id_t_global not in candidate_terms_dicts:
                candidate_terms_dicts[issue["id_t_global"]] = issue | {
                    "approved": False
                }

    all_jsons.extend(candidate_terms_dicts.values())

    return [
        {
            **parse_issue_description(x["description"], vocabulary_id),
            "issue_iid": x["iid"],
            "approved": x["approved"],
        }
        for x in all_jsons
    ]
