"""
Utility functions for generating a related json 
that fits d3 requirement from a list of terms.
"""
from anytree import Node, search
from anytree.exporter import JsonExporter


def make_node(term: dict, parent: Node) -> Node:
    return Node(
        term["id"],
        label=term["label"],
        version_id=term["id"],
        issue_version=term["issue_version"],
        created_by=term["created_by"]["name"],
        created_at=str(term["created_at"]),
        parent=parent,
    )


def generate_version_json(all_versions: list[dict]):
    # initial term version
    tree_node = make_node(all_versions[-1], parent=None)

    # if term versions have a term version they were generated from they get them as parents, the rest are ordered numerically
    current_last_Node = tree_node
    for term in reversed(all_versions[:-1]):
        if term.get("generated_from", None) is not None and search.find_by_attr(tree_node, term["generated_from"]):
            current_last_Node = make_node(term, parent=search.find_by_attr(tree_node, term["generated_from"]))
        else:
            current_last_Node = make_node(term, current_last_Node)

    return JsonExporter(indent=4).export(tree_node)
