"""
Contains functions related to the generation
of the vocabulary tree from the individual term jsons
"""

import hashlib
import json
from anytree import Node
from anytree.exporter import JsonExporter
from flask import session
from ..db import vocabulary as db_vocabulary, term_version as db_term_version


def term_tree(project_id: str, *, archived_only: bool = False):
    """
    A function used to create a tree represantion of the vocabulary.
    """
    if archived_only:
        vocabulary_terms = db_vocabulary.get_archived_term_tree(project_id)
        top_level_term_ids = {
            "archived": db_vocabulary.top_level_ids_archived(project_id)
        }
    else:
        vocabulary_terms = db_vocabulary.get_term_tree(project_id)
        top_level_term_ids = db_vocabulary.top_level_ids(project_id)

    nodes = {}
    for term_id, term in vocabulary_terms.items():
        nodes[term_id] = {
            "name": term["label"],
            "id_t_version": term["id"],
            "id_t_global": term_id,
            "approved": term["is_approved"],
            "datatype": term.get("datatype", "").strip(),
            "vocpopuli_type": term.get("vocpopuli_type", ""),
            "is_to_level": term["is_top_level"],
        }

    for id, node in nodes.items():
        narrower = vocabulary_terms[id].get("narrower")
        if narrower:
            node["children"] = [nodes[child] for child in narrower]

    forest = {
        vp_type_name: {
            "name": vp_type_name,
            "children": [nodes[child] for child in children],
        }
        for vp_type_name, children in top_level_term_ids.items()
    }

    return forest


def term_tree_from_bank(vocabulary_id: str):
    """
    A function used to create a tree represantion of a vocabulary
    contained in the term bank.
    """
    # create the tree root
    project_name = db_vocabulary.get_from_bank(vocabulary_id)["name"]
    tree_node = Node(project_name)

    vocabulary_terms = db_vocabulary.get_term_tree_from_bank(id=vocabulary_id)

    top_level_term_ids = db_vocabulary.top_level_ids_from_bank(id=vocabulary_id)
    
    forest = {}
    for vp_type_name in top_level_term_ids:
        _, children = top_level_term_ids[vp_type_name]
        root_node = Node(vp_type_name)
        adding_children_stack = [(root_node, children)]

        while adding_children_stack:
            # append children nodes to the parent node
            parent, children = adding_children_stack.pop()
                
            parent.children = [
                Node(
                    name=vocabulary_terms[child]["label"],
                    id_t_version=vocabulary_terms[child]["id"],
                    id_t_global=child,
                    datatype=vocabulary_terms[child].get("datatype", "").strip(),
                    vocpopuli_type=vocabulary_terms[child].get("vocpopuli_type", ""),
                    is_to_level=vocabulary_terms[child].get("vocpopuli_type", False),
                )
                for child in children
            ]
            # add children to the queue if they have children
            adding_children_stack.extend(
                (child, narrower)
                for child in parent.children
                if (narrower := vocabulary_terms[child.id_t_global]["narrower"])
            )

        forest[vp_type_name] = root_node
    return forest


def full_tree(
    vocabulary_terms: dict[str, dict],
    top_level_term_ids: list[str],
    project_id: str,
) -> str:
    """
    TODO: catch any loops

    A function used to create a tree represantion of the vocabulary.

    vocabulary_terms --- A list of the dictionaries of all terms
    in the vocabulary.
    """
    id_t_locals = sorted(
        id_t_local
        for term in vocabulary_terms.values()
        if term["is_approved"] and (id_t_local := term.get("id_t_local"))
    )
    n_approved_str = f"{len(id_t_locals):06}"
    id_v_local = db_vocabulary.get_latest_id_v_local(project_id)

    if not id_v_local:
        id_v_local = f"{n_approved_str}-n/a-vp"


    id_t_versions = sorted(x["id"] for x in vocabulary_terms.values())
    id_t_versions_str = ", ".join(x for x in id_t_versions)

    version_hash = hashlib.md5(id_t_versions_str.encode("utf-8")).hexdigest()
    id_v_version = f"{version_hash}-vp"

    # create the tree root
    project_name = db_vocabulary.get(project_id)["name"]
    id_v_global = f"vocab-{project_id}-vp"
    tree_node = {
        "name": project_name,
        "id_v_global": id_v_global,
        "id_v_local": id_v_local,
        "id_v_version": id_v_version,
        "approved": True,
        "issue_iid": -1,
    }

    nodes = {}
    for id, data in vocabulary_terms.items():
        approved = data["is_approved"]
        if data["datatype"] != " ":
            datatype = data["datatype"]
        else:
            datatype = False

        nodes[id] = {
            "name": data["label"],
            "definition": data["definition"],
            "id_t_global": data["term_id"],
            "id_t_local": data.get("id_t_local", ""),
            "id_t_version": data["id"],
            "approved": approved,
            "broader": data["broader"],
            "narrower": data["narrower"],
            "related": data["related"],
            "related_external": data["related_external"],
            "issue_iid": data.get("issue_iid", -1),
            "color": "green" if approved else "red",  # TODO: move to the view?
            "datatype": datatype,
            "vocpopuli_type": data.get("vocpopuli_type", ""),
            "purl": data.get("purl"),
            "unit": data.get("unit"),
            "required": data.get("required"),
            "min_": data.get("min_"),
            "max_": data.get("max_"),
            "generated_from": data.get("generated_from"),
            "gitlab_skos_url": data.get("gitlab_skos_url"),
            "custom_relationships": data.get("relationships"),
        }

    tree_node["children"] = [nodes[id] for id in top_level_term_ids]
    for node in nodes.values():
        if node["narrower"]:
            node["children"] = [nodes[child] for child in node["narrower"]]

    # Add relationship types
    relationship_types = db_vocabulary.get_relationship_types(project_id)
    tree_node["children"].extend(
        {
            "name": relationship_type["label"],
            "definition": relationship_type["definition"],
            "id_t_global": relationship_type["id_t_global"],
            "id_t_local": relationship_type["id_t_local"],
            "id_t_version": relationship_type["id_t_version"],
            "issue_iid": relationship_type.get("issue_iid", -1),
            "vocpopuli_type": "relationship_type",
            "approved": True,
        }
        for relationship_type in relationship_types
    )

    return json.dumps(tree_node, indent=4)
