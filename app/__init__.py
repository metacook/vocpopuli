"""
Definition of the app factory function used for VocPopuli
"""

from flask import Flask, g
from flask_bootstrap import Bootstrap5
from config import config, Config
from flask_session import Session
from celery import Celery
from .db import teardown_db
import os

DOTENV_PATH = os.path.abspath("../vocpopuli/.env")

bootstrap = Bootstrap5()
sess = Session()
celery = Celery(
    __name__,
    broker=Config.CELERY_BROKER_URL,
    backend=Config.result_backend,
)
# broker_use_ssl=Config.broker_use_ssl,
# redis_backend_use_ssl=Config.redis_backend_use_ssl)


def create_app(config_name: str):
    """
    The app factory function.
    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app()

    bootstrap.init_app(app)
    sess.init_app(app)
    celery.conf.update(app.config)
    app.teardown_appcontext(teardown_db)

    # register all available blueprints
    from .gitlab import gitlab_blueprint

    app.register_blueprint(gitlab_blueprint, url_prefix="/login")

    from .main import main as main_blueprint

    app.register_blueprint(main_blueprint)

    return app
