import os
from app import create_app

app = create_app(os.environ.get("FLASK_CONFIG") or "default")

if not os.path.exists(".env"):
    with open(".env", "w"):
        pass


@app.cli.command()
def test():
    """Run the unit tests in the 'tests' package"""
    import unittest

    tests = unittest.TestLoader().discover("tests")
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command("migrate")
def migrate():
    """Applies the migrations"""
    import subprocess

    try:
        # run neo4j-migrations migrate
        subprocess.run(
            [
                os.environ.get("NEO4J_MIGRATIONS_EXECUTABLE", "neo4j-migrations"),
                f"--location=file:{os.getcwd()}/app/db/migrations",
                "migrate",
            ]
        )
    except FileNotFoundError as e:
        # if neo4j-migrations is not found, print a custom error message
        if str(e) == "[Errno 2] No such file or directory: 'neo4j-migrations'":
            import sys

            print(
                "Could not find 'neo4j-migrations'. "
                "Are you sure it is installed and included in the path?",
                file=sys.stderr,
            )
        else:
            raise e
