# VocPopuli
VocPopuli is a Python-based software platform for collaborative development of controlled vocabularies (glossaries, taxonomies, or thesauri). VocPopuli's vocabularies are designed to support the collection of FAIR research data.

_VocPopuli was recently voted "Most Innovative RDM Solution From External Communities or other NFDI-Consortia" at the 2023 NFDI4Ing Conference._

# Basic Functionality
From a technical perspective, VocPopuli is a frontend for a graph database with a GitLab backup which enables people to collaboratively define, version, and organize terms in controlled vocabularies. Once a vocabulary repository has been initialized, new terms can be added to the vocabulary by filling out the relevant form in VocPopuli.

**-Recent Developments!-**
The SKOS Export and GitLab backup options are currently suspended and being redeveloped, as the VocPopuli codebase was switched to rely on a Neo4j graph database. Expect another release that includes these features in April 2023. Until then, VocPopuli can be set up as described below, and used without these two features.

# Setup
Currently, VocPopuli can be installed locally, while the connection to the database and the GitLab synchronization can be shared. It is recommended to use a virtual environment when running VocPopuli. This can be done using tools such as [conda](https://anaconda.org/anaconda/conda).

## Required Software
VocPopuli requires Python 3.9+ to run.

The Python packages needed to run the tool must first be installed in the chosen environment.
This can be done by running the following command inside the ```vocpopuli``` directory: ```pip install -r requirements.txt```

<details><summary>Infrastructure Details</summary>
[Redis](https://redis.io/docs/getting-started/installation/) is an in-memory database.

It is used to store some of the necessary user-specific data on the server running VocPopuli with the help of [Flask-Session](https://flask-session.readthedocs.io/).

Flask-Session also offers support for [additional storage](https://flask-session.readthedocs.io/en/latest/#built-in-session-interfaces). These have not yet been tested.


Redis is also used as a results backend and task broker by [Celery](https://docs.celeryq.dev/en/stable/).

This is needed for VocPopuli to be able to run some of its functions in the background, without disturbing the user experience.

Celery offers support for additional [brokers, and backends](https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/index.html). These have not yet been tested.


Vocabularies are stored in a [Neo4j](https://neo4j.com/) database and synchronized with GitLab. To run migrations, you need to download [neo4j-migrations](https://michael-simons.github.io/neo4j-migrations/current/#download).

To set up a local database using docker, set `$PATH_TO_DB` to point to the folder where you want to keep the database data, and run the following command:
```bash
docker run \
    -p 7474:7474 -p 7687:7687 \
    --volume $PATH_TO_DB:/data \
    -e apoc.export.file.enabled=true \
    -e apoc.import.file.enabled=true \
    -e apoc.import.file.use_neo4j_config=true \
    -e NEO4J_PLUGINS=\[\"apoc\"\] \
    --restart=unless-stopped \
    -d neo4j
```
</details>




## GitLab Setup
VocPopuli uses GitLab as the backup database for vocabulary entries. The advantages to that are that the vocabulary owner always has full control over vocabulary access and visibility rights (an administrator can even block VocPopuli's access to it, if desired). In order for VocPopuli to be able to integrate with GitLab, the specific VocPopuli installation needs to be registered as a GitLab application, first. For this, consult the [following guide](https://docs.gitlab.com/ee/integration/oauth_provider.html).

The following Redirect URI's should be defined during the application's registration:

https://your-domain-name.com/login/gitlab \
https://your-domain-name.com/login/gitlab/authorized

https://your-domain-name.com is a placeholder for the domain of the server hosting VocPopuli.
For development purposes, it can be set to http://localhost:5000.


After the app has been registered in GitLab, its Application ID and Secret should be stored on the machine running VocPopuli as environment variables.
These should be called ```OAUTH_CLIENT_ID```, and ```OAUTH_CLIENT_SECRET```, respectively.
Alternatively, instead of storing the variables as environment variables, they can be hard-coded directly in the `Config` class inside `config.py`.

Next, GitLab's user management is used for VocPopuli's user management as well. For that, a GitLab user needs to be selected, who will act as the administrator of all vocabularies managed by a given VocPopuli instance.
In the case of a single person using the tool by themselves, this user will become the administrator. The chosen user needs to generate an [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token) in GitLab which will be used to make some of the API calls used by the tool. The token's scope should be set to ```api```.
The access token should be copied, and stored in a manually created file called ```.env```(w/o file name) which should reside in the main vocpopuli directory. The file should contain two text lines which look the following way:
```
VOCPOPULI_TOKEN = 'your-token-here'
VOCPOPULI_ADMIN_USER_ID = '***'
```
The value of `VOCPOPULI_ADMIN` is set to the admin user's User ID, which can be found on their profile page (represented only by numbers).
The `.env` file can also be created by copying the `.env_template` file, and removing the `_template` suffix.

## Neo4j Setup
VocPopuli relies on a Neo4j graph database instance for fast querying of the vocabularies, and their terms. A Neo4j instance can be set up either in the cloud or on a dedicated server. Detailed information on the topic can be found on the [Neo4j website](https://neo4j.com/).

After an instance has been set up, its credentials need to be imported into VocPopuli's configuration. Additional information can be found below.

## Configuration
The configuration variables defining the behaviour of a given VocPopuli instance are stored inside the various `Config` classes in `config.py`.
Their values can either be hard-coded in the file (not recommended for non-development purposes), or stored as environment variables.

An overview of the configuration variables can be found below:
| Variable name | Description |
|----------------|---------|
|  OAUTH_CLIENT_ID | See above.  |
| OAUTH_CLIENT_SECRET  | See above.   |
|  SECRET_KEY | The application's secret key. It is recommended to use a randomly generated string as its value. |
|  VOCABULARIES_DIR | The path of the directory used for locally storing vocabulary repositories.   |
| SESSION_TYPE  |  The storage method used by [Flask-Session](https://flask-session.readthedocs.io/en/latest/#configuration) |
| SESSION_PERMANENT  |  See [Flask-Session](https://flask-session.readthedocs.io/en/latest/#configuration). |
|  SESSION_USE_SIGNER | See [Flask-Session](https://flask-session.readthedocs.io/en/latest/#configuration).  |
|  CELERY_BROKER_URL | The URL of the instance running the Celery broker.  |
|  result_backend |  The URL of the used Celery results backend. |
|  NEO4J_URI |  The URI of the Neo4j database. |
|  NEO4J_USERNAME |  The username of the Neo4j database (default: neo4j). |
|  NEO4J_PASSWORD |  The password for the Neo4j database. |


| Additional variables | Description |
|-------------------|---------|
| VOCPOPULI_TOKEN | See above.  |
| VOCPOPULI_ADMIN_USER_ID  | See above.   |

# Starting VocPopuli
Before VocPopuli can be started, the script ```load_repositories.py``` inside the main VocPopuli directory needs to be ran. This will ensure that the vocabularies, to which the current `VOCPOPULI_ADMIN` has access to, are loaded properly.

Afterwards, the Redis instance specified in `SESSION_TYPE`, `CELERY_BROKER_URL`, and `result_backend` needs to be started.
This is usually done by running the command `redis-server` (with added arguments if needed) in a new console windows.

Next, the following environment variables need to be set, so that the Flask application containing VocPopuli can be started:
```FLASK_APP=vocpopuli.py```, ```FLASK_DEBUG=1```.

After that, a celery instance needs to be started from inside the main vocpopuli directory. This is done by running the following command in a new console window:
```
celery -A celery_worker.celery worker --loglevel=info
```
If running VocPopuli on a Windows machine, the previous command needs to be changed as follows:
```
celery -A celery_worker.celery worker --loglevel=info --pool=solo
```

Next, make sure that the Neo4j database is running. For local development you can use [Neo4j Desktop](https://neo4j.com/download-center/#desktop) or the [neo4j Docker image](https://hub.docker.com/_/neo4j).
To apply any outstanding migrations make sure that the environment variables starting with `NEO4J_` are exported and run
```
flask migrate
```

VocPopuli can now then be started by running the ```flask run``` command inside the ```vocpopuli``` directory, and opening http://localhost:5000.


## Setting up an initial vocabulary
Currently, an initial empty vocabulary can be created from inside VocPopuli by clicking on 'Import/Create vocabulary' in the navigation bar. After a name for the vocabulary has been entered, and the form submitted, the vocabulary will be created automatically and exported to GitLab. There is no need for any JSON files to be uploaded when creating an empty new vocabulary.

Afterwards, new terms can be added using the 'New term' option in the navigation bar.

# Development setup using docker and docker compose

To run a local instance of VocPopuli using docker and docker compose,
you have to do the following:

- Create a new Application in GitLab (click `Add new application` on
  https://gitlab.com/-/profile/applications) with these settings:

  * do not enable `Confidential`
  * only enable the scope `api`
  * as redirect URIs you have to provide
    `http://localhost:5000/login/gitlab` and
    `http://localhost:5000/login/gitlab/authorized`

  * click "Save Application"
  Take note of (or copy) the **Application ID** and the **Secret**.

- Create an Access Token  (click `Add new token` on
  https://gitlab.com/-/profile/personal_access_tokens).  The only
  required scope is `api`. Take note of (or copy) the **generated
  token**.

- Go to https://gitlab.com/-/profile and take note of (or copy) your
  **User ID**

- Create a file `.env` in the root folder of the repository with the
  following content where you replace the `<...>` parts with the
  collected values, respectively:

```
OAUTH_CLIENT_ID="<Application ID of the GitLab Application>"
OAUTH_CLIENT_SECRET="<Secret of the GitLab Application>"
VOCPOPULI_TOKEN="<Access Token>"
VOCPOPULI_ADMIN_USER_ID="<User ID>"
```

Once all of this is done, you can start a local VocPopuli instance (assuming you have installed docker and docker-compose already) by
running

```bash
$ docker compose up
```
or if you have an older version of docker-compose

```bash
$ docker-compose up
```

You can reach this instance at `http://localhost:5000/`.  To access the
Neo4J web interface, you have to go to `http://localhost:7474/`.


# Functionality of VocPopuli
In the following paragraphs the functionalities of VocPopuli are listed.

## List Terms
List Terms gives an overview of the whole vocabulary by showing all its terms with their data type, name and approval status. It offers functionalities like searching for terms, downloading a json of the (approved) vocabulary and making the structural changes in the vocabulary. The term names redirect the user to view the information of the term.

### Adding new Terms
On list terms new terms can be created by clicking 'New Term' and filling out the form with all the information. If an existing term should serve as a template the clipboard icon next to it can be clicked. The user gets redirected to a new term form with the information from the selected term prefilled.

There is also a quick addition of child terms by clicking on the '+'-button next to a term. There a term can be created with a name, description and datatype. If clicked on 'Edit' it can be edited further (see Create and Edit Terms) or saved with only thos three attributes.

### Restructuring the Vocabulary
When the 'Enable Restructure' Button is clicked the terms and vocpopuli types can be dragged and dropped to change the existing structure. This changes the broader terms and vocpopuli types of the terms. Invalid moves include sorting terms into 'detached', into multiple different VocPopuli types or beneath data types that are not set as parent data types (see Data Types, VocPopuli Types, Relationship Types). Important to note is that terms can either have broader terms or a vocpopuli type, so it is not possible to have instances of the same term as a top level and lower level term. It is also possible to create another instance of a term by clicking 'ctrl' or 'cmd' when starting to drag. This adds another broader term to the dragged term. To save the new structure the 'Save Vocabulary Structure'-button needs to be clicked, else the changes made are lost when leaving the page.

### Graphs
Through the button 'Vocabulary Graphs' the Hierarchical Graph and Relation Graph can be viewed. The hierarchical graph shows the structure of the terms in a tree. Their approval statuses are also shown.

In the relation graph the terms are shown with their (custom) relationships. Custom relationships are represented by named and directed edges while the relations defined by the attribute 'related terms' are shown as unnamed and not directed edges. To move the nodes independently from each other the pin icon can be clicked.

## View Term
The view term page shows the information of a term. From there the terms can be archived, deleted and approved. When a term is archived it does not show up in list terms anymore and its children become detached. The archived terms can be viewed under Terms > Archived Terms.

The page also gives an overview of the versions of the term through the graph that shows what version was created at what time and from what previous version. In the dropdown menu 'Version' the different versions can be viewed. The Ids of this version can be seen in the dropdown 'ID'.It is also possible to leave comments on the version of the term and give it an up or downvote.

When clicking on the button 'Edit Term' the edit term form opens with the information of the selected term version.

## Create and Edit Terms
The Form to create and edit new terms contains the following information:
- Term: The unique name of the term.
- Required: If the information of the term is required.
- Definition: The definition of the term. 
- Reference: A reference leading to further information on the term.
- Relatively Broader Terms: The terms this term is a child of. This is the information that creates the hierarchical structure of the vocabulary as seen on list terms. When none is selected the term is a top level term.
- VocPopuli Type: A type that organizes the top level terms on list terms. This can only be given to top level terms.
- Data Type: The data type of the term. Depending on the (existence of) broader terms the options in the select menu change according to the definition of the data types (see Data Types, VocPopuli Types, Relationship Types).
- Synonyms: Synonyms to the term name.
- Translations: Translations of the term name.
- Unit: The possible units for this term as comma separated values. Units are only available for terms with a numeric datatype.
- Range: A minimum and maximum value for the term. Only available for terms with numeric data types.
- Related Terms: Other top level terms that are related to the current term, only available to top level terms. These relations are represented in the relation graph (see Graphs).
- Related External Terms: URI of related terms that are not in the vocabulary.
- Add Custom Relationship: When custom relationships are defined for this vocabulary (see Data Types, VocPopuli Types, Relationship Types) those can be added to the term by clicking the button. Per row one custom relationship type with multiple related terms can be created or removed.
- Image Upload: optional images can be added here.

## Data Types, VocPopuli Types, Relationship Types
In the navbar under the menu point 'Types' the overview and edit menus can be found. There the types can be created through the 'Add' button, edited by double clicking on them and deleted through clicking the garbage can icon on the selected term. Only unused types can be deleted. Data types and relationship types can also be reordered by dragging the arrow icon on the right and saving with the 'Save Order'-button. The order of the vocpopuli types is the same as the one created on list terms.

Data types have the following attributes:
- Name: A unique name of the data type.
- Color, Icon: The icon and its color will be shown next to the data type on view term. On list terms it is the only indicator to what data type a term has.
- Numeric: Numeric types allow their terms to have ranges and units.
- Top-Level: If the terms of this data type are allowed to be top level terms the checkbox should be selected.
- Parent Data Types: The data types of the terms that terms of this data type can be children of. Necessary to select if top-level term is not selected, else this data type will not show up in the edit term form.
- Help Text: Text to help distinguish data types and to choose the correct one for a term.

VocPopuli Types have a name. The color is automatically assigned through the name.
Relationship types have a name, a definition and optionally a related IRI to point to an external reference.
All types need a unique name when created.

## Vocabularies
The vocabularies can be created through the navigation under the point 'Vocabularies'. When creating a new vocabulary a repository on gitlab is created with the entered name and terms if provided. Each Vocabulary can have the following attributes:
- Name: The name of the vocabulary (and the repository).
- Vocabulary Description: A description of the vocabulary.
- Vocabulary Domain: Comma separated values for the domains of this vocabulary. These are used to filter for them in the bank.
- Institution Name, ROR (URL): The Institution this vocabulary belongs to.
- License Name, URL: The license of the vocabulary.
- JSON files of terms: JSON files containing terms that belong in the vocabulary.

The entered Metadata of the vocabularies can be viewed under 'Vocabularies'>'View Metadata'. There a table is shown with vocabulary statistics. The metadata of the vocabulary can be changed by double clicking on the correct row in the table.

The current vocabulary can always be seen and changed in the top right corner of the navigation bar.

## Export
VocPopuli currently offers the export of the top level terms of the vocabulary as a Kadi4Mat Template. All the terms that should be exported need to be approved. In the export a title should be given to the template that starts with the name of the exported term. The Kadi4Mat Host and Access token need to be provided to automatically create the template in Kadi4Mat. If that information is not given the template can be downloaded as a JSON file.

## User
In the user settings the user can enter their ORCID iD.


# Remarks
- VocPopuli is in its early development stages. Bugs are to be expected.
- Currently, the software has been tested mainly on MacOS and Ubuntu. Windows compatibility might be limited.

# Planned features

- Weekly front-end improvements
- Offer ORCID iD as identification option
- Creation of vocabularies from existing Kadi4Mat records or templates
- Vocabulary creation from diverse data sources
- Import of SKOS vocabularies
- Serialization in PROV
- Admin and Domain Expert views with different privileges
- A vocabulary management section for each user to review the vocabularies they have access to
- VocPopuli API
- A "scratchpad" option for quick note taking
- "Mark all terms as read/unread" option
- Provenance visualization
- Notification panel
- Spell checker
- Multiple color schemes
- New login options
- A Feedback panel
- Review entire public vocabularies without a login
- Making Term List actionable by enabling "drag-and-drop" functions
- A "quick add" function for adding terms inline within the hierarchy
- Work with the following ELNs to explore options for integration: OpenBis, Chemotion, RSpace, ELabFTW, Herbie
- Selection of custom namespaces on GitLab which host the vocabulary
- Additional PID providers to PIDA

# Authors

- Ilia Bagov
- Miłosz Meller
- Floriane Bresser
- Nuoyao Ye
- Nick Garabedian
- Fabian Kirchner

# Funding

VocPopuli is funded by the Initiative and Networking Fund of the Helmholtz Association in the framework of the Helmholtz Metadata Collaboration project call.

